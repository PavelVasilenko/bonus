package ru.devroom.krizalbonus.model.objects

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoadedSettings {

    @SerializedName("levels") // уровни бонусной системы
    @Expose
    var levels: ArrayList<LevelInfo>? = null

    @SerializedName("settings") // дополнительные параметры
    @Expose
    var settings: Map<String, String>? = null


}