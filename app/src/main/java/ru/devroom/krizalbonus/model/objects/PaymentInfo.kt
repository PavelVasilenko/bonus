package ru.devroom.krizalbonus.model.objects

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PaymentInfo {

  @SerializedName("p_id") // id  в базе
  @Expose
  var serverId: Int = -1

  @SerializedName("p_date_time") // время
  @Expose
  var dateTime: String = ""

  @SerializedName("p_employee_id") // id оформившего сотрудника
  @Expose
  var employeeId: Int = -1

  @SerializedName("p_client_id") // НОМЕР КАРТЫ клиента-покупателя
  @Expose
  var clientId: Int = 0

  @SerializedName("p_goods_cost") // стоимость товара
  @Expose
  var goodCost: Int = 0

  @SerializedName("p_used_bonuses") // кол-во использованных бонусов
  @Expose
  var usedBonuses: Int = 0

  @SerializedName("p_total_cost") // стоимость со скидкой
  @Expose
  var totalCost: Int = 0

  @SerializedName("c_fio") // фио клиента
  @Expose
  var clientFio: String = ""

  @SerializedName("e_family") // фамилия сотрудника
  @Expose
  var employeeFamily: String = ""

  @SerializedName("p_mark") // 0 или оценка покупателя 1..5
  @Expose
  var mark: Int = 0

  @SerializedName("promises") // сколько бонусов обещано
  @Expose
  var promises: Int = 0

  @SerializedName("p_cash_back") // сколько бонусов обещано
  @Expose
  var cashBack: Int = 0

  @SerializedName("p_comment") // отзыв покупателя
  @Expose
  var comment: String = ""

  @SerializedName("p_admin_comment") // отзыв администратора
  @Expose
  var adminComment: String = ""

  @SerializedName("p_hidden_note") // примечание к покупке
  @Expose
  var note: String = ""

}
