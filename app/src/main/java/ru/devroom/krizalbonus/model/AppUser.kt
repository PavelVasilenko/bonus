package ru.devroom.krizalbonus.model

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.google.gson.Gson
import ru.devroom.krizalbonus.model.objects.EmployeeInfo
import java.util.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.devroom.krizalbonus.model.network.RetrofitInstance


class AppUser {
    private var employeeInfo: EmployeeInfo? = null
    private var usedPassword: String? = null

    @Volatile
    var previousFirmId: Int = -1
    @Volatile
    var previousTemplateId: Int = -1

    private var sharedPreferences: SharedPreferences? = null

    private var mTimer: Timer? = null
    private var mMyTimerTask: MyTimerTask? = null

    companion object {

        private const val RE_AUTH_TIMER_DELAY =
            5L // min задержка при переавторизации при запуске приложения
        private const val RE_AUTH_TIMER_PERIOD = 30L // min период переавторизации

        private var INSTANCE: AppUser? = null
        private const val CACHE_FILE: String = "auth_cache"
        private const val INFO_KEY: String = "infoKey"
        private const val PASSWORD_KEY: String = "passw_key"
        private const val PREVIOUS_FIRM_ID: String = "prevFirmId"
        private const val PREVIOUS_TEMPLATE_ID: String = "prevTemplateId"

        fun init(ctx: Context) {
            if (INSTANCE == null) INSTANCE = AppUser()
            INSTANCE!!.sharedPreferences =
                ctx.getSharedPreferences(CACHE_FILE, Context.MODE_PRIVATE)
            INSTANCE!!.loadUserAuthCache()
            INSTANCE!!.initReAuthTimer()
        }

        fun get(): AppUser {
            return INSTANCE!!
        }

    }


    internal inner class MyTimerTask : TimerTask() {
        override fun run() {
            Log.d("TIMERTEST", "MyTimerTask")
            if (employeeInfo != null && usedPassword != null) {
                GlobalScope.launch {
                    Log.d("TIMERTEST", "запрос к серверу")
                    // запрос к серверу
                    val response = try {
                        // шифруем пароль перед передачей
                        RetrofitInstance.getService.authAsync(employeeInfo!!.login, usedPassword!!)
                            .await()
                    } catch (e: Exception) {
                        Log.d("TIMERTEST", "Exception")
                        null
                    }

                    // ответ сервера
                    Log.d("TIMERTEST", "ответ сервера")
                    if (response?.isSuccessful == true && response.body() != null) {
                        val receivedData = response.body()
                        if (receivedData != null) {
                            Log.d("TIMERTEST", "onLoginSuccess")
                            onLoginSuccess(receivedData, usedPassword!!)
                        }
                    }

                }
            }
        }
    }

    fun initReAuthTimer() {
        Log.d("TIMERTEST", "initReAuthTimer")

        if (mTimer != null) {
            mTimer!!.cancel()
            mTimer = null
        }
        mTimer = Timer()
        mMyTimerTask = MyTimerTask()
        mTimer!!.schedule(
            mMyTimerTask,
            RE_AUTH_TIMER_DELAY * 60 * 1000,
            RE_AUTH_TIMER_PERIOD * 60 * 1000
        )
    }

    fun destroy() {
        Log.d("TIMERTEST", "destroy")
        if (mTimer != null) {
            mTimer!!.cancel()
            mTimer = null
        }

    }

    private fun loadUserAuthCache() {
        try {
            if (sharedPreferences != null) {

                usedPassword = sharedPreferences!!.getString(PASSWORD_KEY, "")

                val gson = Gson()
                val json = sharedPreferences!!.getString(INFO_KEY, "")
                employeeInfo = gson.fromJson(json, EmployeeInfo::class.java)

                previousFirmId = sharedPreferences!!.getInt(PREVIOUS_FIRM_ID, -1)
                previousTemplateId = sharedPreferences!!.getInt(PREVIOUS_TEMPLATE_ID, -1)

            }
        } catch (e: Exception) {
            e.printStackTrace()
            employeeInfo = null
        }
    }

    fun clearUserAuth() {
        val editor = sharedPreferences!!.edit()
        editor.remove(INFO_KEY)
        editor.apply()
        employeeInfo = null
    }
//
//    fun savePreviousFirmId(id:Int) {
//        previousFirmId = id
//        val editor = sharedPreferences!!.edit()
//        editor.putInt(PREVIOUS_FIRM_ID, id)
//        editor.apply()
//    }
//
//    fun savePreviousTemplateId(id:Int) {
//        previousTemplateId = id
//        val editor = sharedPreferences!!.edit()
//        editor.putInt(PREVIOUS_TEMPLATE_ID, id)
//        editor.apply()
//    }

    fun onLoginSuccess(authInfo: EmployeeInfo, usedPassword: String) {
        if (INSTANCE == null) INSTANCE = AppUser()

        this.employeeInfo = authInfo

        val editor = sharedPreferences!!.edit()
        val gson = Gson()
        val json = gson.toJson(authInfo)
        editor.putString(INFO_KEY, json)
        editor.putString(PASSWORD_KEY, usedPassword)
        editor.apply()
    }


    fun getEmployeeInfo() = this.employeeInfo
}