package ru.devroom.krizalbonus.model.objects

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class EmployeeInfo {

  @SerializedName("e_id") // id  в базе
  @Expose
  var serverId: Int = -1

  @SerializedName("e_login") // логин сотрудника
  @Expose
  var login: String = ""

  @SerializedName("e_password") // зашифрованный пароль
  @Expose
  var password: String = ""

  @SerializedName("e_token")
  @Expose
  var token: String = ""

  @SerializedName("e_token_expired")
  @Expose
  var expired: Long = 0

  @SerializedName("e_role") // (0=кассир/1=админ)
  @Expose
  var role: Int = 0

  @SerializedName("e_family") // фамилия сотрудника
  @Expose
  var family: String = ""


  fun isAdminRole() = role == 1
}
