package ru.devroom.krizalbonus.model.objects

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ReportOperationInfo {

  @SerializedName("o_type") // тип операции
  @Expose
  var type: Int = -1

  @SerializedName("sum") // сумма
  @Expose
  var sum: Int = 0


}
