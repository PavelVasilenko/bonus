package ru.devroom.krizalbonus.model.objects

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LevelInfo {

  @SerializedName("l_id") // id  в базе
  @Expose
  var serverId: Int = -1

  @SerializedName("l_total_purchases") // сумма покупок
  @Expose
  var totalPurchases: Int = 0

  @SerializedName("l_cashback") // % возвращающийся в виде бонусов
  @Expose
  var cashBack: Int = 0

  @SerializedName("l_max_discount") // % максимальная скидка с товара
  @Expose
  var maxDiscount: Int = 0

  @SerializedName("l_live_time") // сколько месяцев баллы не сгорают
  @Expose
  var liveTime: Int = 0

  @SerializedName("l_delay_days") // через сколько дней начисляются баллы
  @Expose
  var delayDays: Int = 0

}
