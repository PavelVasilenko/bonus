package ru.devroom.krizalbonus.model.objects

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class OperationInfo {

  @SerializedName("o_id") // id  в базе
  @Expose
  var serverId: Int = -1

  @SerializedName("o_type") // тип операци (1=списание.покупка; 2=начисление.с.покупки; 3=сгорание.бонусов; 4=подарок на др;5=иное.начисление)
  @Expose
  var type: Int = 0

  @SerializedName("o_client_id") // id клиента
  @Expose
  var clientId: Int = -1

  @SerializedName("o_sum") // сумма бонусов
  @Expose
  var summ: Int = 0

  @SerializedName("o_purchase_id") // id документа о покупке
  @Expose
  var purchaseId: Int = 0

  @SerializedName("p_mark") // оценка, если это покупка
  @Expose
  var purchaseMark: Int = 0

  @SerializedName("o_date") // дата создания документа
  @Expose
  var date: String = ""


}
