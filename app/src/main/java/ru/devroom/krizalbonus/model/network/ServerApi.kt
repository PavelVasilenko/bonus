package ru.devroom.krizalbonus.model.network

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*
import retrofit2.http.POST
import ru.devroom.krizalbonus.model.objects.*

interface ServerApi {

    // Авторизация
    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    @FormUrlEncoded
    @POST("employees/auth")
    fun authAsync(
        @Field("login")     login:String,
        @Field("password")  password:String
    ): Deferred<Response<EmployeeInfo>>





    // Уровни
    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    // Получение настроек
    @GET("settings")
    fun getSettingsAsync(
        @Header("Authorization") adminToken:String
    ): Deferred<Response<LoadedSettings>>

    // редактирование настроек бонусов
    @FormUrlEncoded
    @PUT("settings/gifts")
    fun updateGiftsAsync(
        @Field("bon_for_birthday")      sum:Int,
        @Field("bon_for_birthday_days") days:Int,
        @Header("Authorization")    adminToken:String
    ): Deferred<Response<Void>>

    // редактирование настроек организации
    @FormUrlEncoded
    @PUT("settings/orginfo")
    fun updateOrgInfoAsync(
        @Field("phone")         phone:String,
        @Field("whatsapp")      whatsapp:String,
        @Field("address")       address:String,
        @Field("site")          site:String,
        @Field("email")         email:String,
        @Field("insta")         insta:String,
        @Field("about")         about:String,
        @Header("Authorization")    adminToken:String
    ): Deferred<Response<Void>>

    // -----------------------------------------------------------------------------------------------------------------
    // добавление нового уровня
    @FormUrlEncoded
    @POST("levels")
    fun postNewLevelAsync(
        @Field("l_total_purchases") purchases:Int,
        @Field("l_cashback")        cashBack:Int,
        @Field("l_max_discount")    discount: Int,
        @Field("l_live_time")       liveTime: Int,
        @Field("l_delay_days")      delay: Int,
        @Header("Authorization")    adminToken:String
    ): Deferred<Response<Int>>

    // редактирование уровня
    @FormUrlEncoded
    @PUT("levels/{l_id}")
    fun updateLevelAsync(
        @Path("l_id")               levelId:Int,
        @Field("l_total_purchases") purchases:Int,
        @Field("l_cashback")        cashBack:Int,
        @Field("l_max_discount")    discount: Int,
        @Field("l_live_time")       liveTime: Int,
        @Field("l_delay_days")      delay: Int,
        @Header("Authorization")    adminToken:String
    ): Deferred<Response<Int>>

    // удаление данных об уровне
    @DELETE("levels/{l_id}")
    fun deleteLevelAsync(
        @Path("l_id")       levelId: Int,
        @Header("Authorization") adminToken:String
    ): Deferred<Response<Int>>


    // Платежи
    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    // Получение списка платежей
    @GET("purchases/list/{date1}/{date2}")
    fun getPaymentsListAsync(
        @Path("date1")       date1: String,
        @Path("date2")       date2: String,
        @Header("Authorization") adminToken:String
    ): Deferred<Response<ArrayList<PaymentInfo>>>
    // Получение списка платежей
    @GET("purchases/list/{date1}/{date2}/{card_id}")
    fun getPaymentsListByCardAsync(
        @Path("card_id")     cardId: Int,
        @Path("date1")       date1: String,
        @Path("date2")       date2: String,
        @Header("Authorization") adminToken:String
    ): Deferred<Response<ArrayList<PaymentInfo>>>
    // Получение списка платежей
    @GET("purchases/list/{date1}/{date2}/employee/{employeeid}")
    fun getPaymentsListByEmployeeAsync(
        @Path("employeeid")  employeeId: Int,
        @Path("date1")       date1: String,
        @Path("date2")       date2: String,
        @Header("Authorization") adminToken:String
    ): Deferred<Response<ArrayList<PaymentInfo>>>
    // отправка платежа
    // body: Int 	a_active
    // body: String a_text
    // body: String a_caption
    // body: Int 	a_end_time
    // body: Int 	a_start_time
    @FormUrlEncoded
    @POST("purchases")
    fun postPurchaseAsync(
        @Field("client_id")         clientId: Int,
        @Field("goods_cost")        goodsCost:Int,
        @Field("use_bonuses")       useBonuses:Int,
        @Field("bonuses_sum")       bonusesSum:Int,
        @Field("note")              note:String,
        @Header("Authorization")    token:String
    ): Deferred<Response<Void>>
    // удаление платежа
    @DELETE("purchases/{p_id}")
    fun deletePaymentsListAsync(
        @Path("p_id")       purchaseId: Int,
        @Header("Authorization") adminToken:String
    ): Deferred<Response<Void>>



    // Клиенты
    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    // Получение данных клиента
    @GET("clients/{c_cardnum}")
    fun getClientInfoAsync(
        @Path("c_cardnum")       cardNumber: Int,
        @Header("Authorization") adminToken:String
    ): Deferred<Response<ClientInfo>>
    // -----------------------------------------------------------------------------------------------------------------
    // Поиск клиентов
    @FormUrlEncoded
    @POST("clients/list")
    fun findClientsAsync(
        @Field("start_from")    startPos:Int,
        @Field("find_string")   findString:String,
        @Header("Authorization") adminToken:String
    ): Deferred<Response<ArrayList<ClientInfo>>>
    // удаление платежа
    @DELETE("clients/{c_id}")
    fun deleteClientAsync(
        @Path("c_id")       clientCode: Int,
        @Header("Authorization") adminToken:String
    ): Deferred<Response<Void>>

    // отправка push всем
    @FormUrlEncoded
    @POST("clients/push/message/all")
    fun postPushToAllClientsAsync(
        @Field("message")       message:String,
        @Header("Authorization") adminToken:String
    ): Deferred<Response<Void>>

    // отправка push клиенту
    @FormUrlEncoded
    @POST("clients/push/{card_id}")
    fun postPushToClientAsync(
        @Path("card_id")        clientCardId: Int,
        @Field("message")       message:String,
        @Header("Authorization") adminToken:String
    ): Deferred<Response<Void>>

    // отправка ответа на комментарий клиенту
    @FormUrlEncoded
    @POST("purchases/comment/{purchase_id}")
    fun postAnswerForCommentAsync(
        @Path("purchase_id")    purchaseId: Int,
        @Field("message")       message:String,
        @Header("Authorization") adminToken:String
    ): Deferred<Response<Void>>

    // отправка примечания к покупке
    @FormUrlEncoded
    @POST("purchases/note/{purchase_id}")
    fun postNewNoteAsync(
        @Path("purchase_id")    purchaseId: Int,
        @Field("message")       message:String,
        @Header("Authorization") adminToken:String
    ): Deferred<Response<Void>>

    // отправка бонусов клиенту
    @FormUrlEncoded
    @POST("clients/addbon/{card_id}")
    fun postBonusesToClientAsync(
        @Path("card_id")        clientCardId: Int,
        @Field("sum")           bonusesSum:Int,
        @Header("Authorization") adminToken:String
    ): Deferred<Response<Void>>

    // смена пароля клиенту
    @FormUrlEncoded
    @POST("clients/newpassword/{card_id}")
    fun postNewClientPasswordAsync(
        @Path("card_id")        clientCardId: Int,
        @Field("password")      newPassword:String,
        @Header("Authorization") adminToken:String
    ): Deferred<Response<Void>>

    // очистка бонусов у клиента
    @FormUrlEncoded
    @POST("clients/clearbon/all")
    fun postClearClientBalanceAsync(
        @Field("card_id")        clientCardId: Int,
        @Header("Authorization") adminToken:String
    ): Deferred<Response<Void>>

    // регистрация нового клиента
    @FormUrlEncoded
    @POST("clients/registry")
    fun postNewClientAsync(
        @Field("telephone")         telephone:String,
        @Field("password")          password:String,
        @Field("messaging_token")   messagingToken:String,
        @Field("fio")               fio:String,
        @Field("email")             email:String,
        @Field("gender")            clientCardId: Int,
        @Field("birthday")          birthDay:String,
        @Header("Authorization")    adminToken:String
    ): Deferred<Response<ClientInfo>>

    // Получение списка операций
    @GET("clients/operations/list/{card_id}")
    fun getOperationsListAsync(
        @Path("card_id")            cardNumber: Int,
        @Header("Authorization")    token:String
    ): Deferred<Response<ArrayList<OperationInfo>>>


    // Акции
    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    // Получение списка акций
    @GET("actions")
    fun getActionsListAsync(
        @Header("Authorization") adminToken:String
    ): Deferred<Response<ArrayList<ActionInfo>>>
    // удаление данных об акции
    @DELETE("actions/{a_id}")
    fun deleteActionAsync(
        @Path("a_id")       actionId: Int,
        @Header("Authorization") adminToken:String
    ): Deferred<Response<Void>>
    // добавление новой акции
    // body: Int 	a_active
    // body: String a_text
    // body: String a_caption
    // body: Int 	a_end_time
    // body: Int 	a_start_time
    @FormUrlEncoded
    @POST("actions")
    fun postNewActionAsync(
        @Field("a_caption")         caption: String,
        @Field("a_text")            text:String,
        @Field("a_start_time")      startTime:Long,
        @Field("a_end_time")        endTime: Long,
        @Field("a_active")          isActive: Int,
        @Header("Authorization")    adminToken:String
    ): Deferred<Response<Int>>
    // редактирование акции
    // body: Int 	a_active
    // body: String a_text
    // body: String a_caption
    // body: Int 	a_end_time
    // body: Int 	a_start_time
    @FormUrlEncoded
    @PUT("actions/{a_id}")
    fun putActionAsync(
        @Path("a_id")               actionId:Int,
        @Field("a_caption")         caption: String,
        @Field("a_text")            text:String,
        @Field("a_start_time")      startTime:Long,
        @Field("a_end_time")        endTime: Long,
        @Field("a_active")          isActive: Int,
        @Header("Authorization")    adminToken:String
    ): Deferred<Response<Void>>


    // Отчеты
    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    // Получение отчета по бонусам
    @FormUrlEncoded
    @POST("reports/purchases") //@@!!!!
    fun getReportPurchasesAsync(
        @Field("start_date")    startDate:String,
        @Field("end_date")      endDate:String,
        @Header("Authorization") adminToken:String
    ): Deferred<Response<ArrayList<ReportOperationInfo>>>

    // Получение отчета о новых клиентах
    @FormUrlEncoded
    @POST("reports/new_clients") //@@!!!!
    fun getReportNewClientsAsync(
        @Field("start_date")    startDate:String,
        @Field("end_date")      endDate:String,
        @Header("Authorization") adminToken:String
    ): Deferred<Response<ArrayList<ClientInfo>>>


    // Сотрудники
    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------
    // Получение списка сотрудников
    @GET("employees")
    fun getEmployeesListAsync(
        @Header("Authorization") adminToken:String
    ): Deferred<Response<ArrayList<EmployeeInfo>>>

    // удаление данных о сотруднике
    @DELETE("employees/{e_id}")
    fun deleteEmployeeAsync(
        @Path("e_id")       employeeId: Int,
        @Header("Authorization") adminToken:String
    ): Deferred<Response<Int>>

    // редактирование сотрудника
    @FormUrlEncoded
    @PUT("employees/{e_id}")
    fun putEmployeeAsync(
        @Path("e_id")           employeeId:Int,
        @Field("login")         login:String,
        @Field("password")      password:String,
        @Field("family")        family: String,
        @Header("Authorization")    adminToken:String
    ): Deferred<Response<Int>>

    // добавление нового сотрудника
    @FormUrlEncoded
    @POST("employees")
    fun postNewEmployeeAsync(
        @Field("login")         login:String,
        @Field("password")      password:String,
        @Field("family")        family: String,
        @Header("Authorization")    adminToken:String
    ): Deferred<Response<EmployeeInfo>>

}

