package ru.devroom.krizalbonus.model.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Retrofit
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
//import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.converter.gson.GsonConverterFactory
import ru.devroom.krizalbonus.BuildConfig

object RetrofitInstance {

    private var retrofit: Retrofit? = null
    private var service: ServerApi? = null
    const val HOST_URL = "xxxxx"
    const val BASE_URL = "http://xxxx.$HOST_URL/v1/"

    val getService: ServerApi
        get() {
            if (retrofit == null) {

                    // COMMENT BEFORE RELEASE
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

                val client = OkHttpClient.Builder()
                        .addInterceptor(interceptor)
                        .build()

                retrofit = Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(CoroutineCallAdapterFactory())
                        .client(client)
                        .build()
            }

            if(service == null) {
                service = retrofit!!.create(
                    ServerApi::class.java)
            }

            return service!!
        }
}