package ru.devroom.krizalbonus.model

import android.content.Context
import android.net.Uri
import androidx.core.content.FileProvider
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import ru.devroom.krizalbonus.BuildConfig
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.objects.ClientInfo
import ru.devroom.krizalbonus.model.objects.PaymentInfo
import ru.devroom.krizalbonus.model.objects.ReportOperationInfo
import ru.devroom.krizalbonus.ui.reports.bonuses_activity.ReportBonusesGroupAdapter
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class XLSExporter {

    private fun writeXLSFile(ctx: Context, book:XSSFWorkbook):Uri? {
        val filePath = File(ctx.cacheDir, "")
        filePath.mkdirs()
        val file = File(filePath, Date().time.toString() + ".xlsx")

        val fileUrl = FileProvider.getUriForFile(
            ctx,
            BuildConfig.APPLICATION_ID + ".provider",
            file
        )
        val fileOut = FileOutputStream(file)
        book.write(fileOut)
        fileOut.flush()
        fileOut.close()
        return fileUrl
    }

    fun exportPurchases(list: ArrayList<PaymentInfo>, dates: String, ctx: Context): Uri? {
        val book = XSSFWorkbook()
        exportPurchasesGoods(book, list, dates, ctx)
        return writeXLSFile(ctx, book)
    }

    fun exportBonuses(list: ArrayList<ReportOperationInfo>, dates: String, ctx: Context): Uri? {
        val book = XSSFWorkbook()
        exportBonusesToBook(book, list, dates, ctx)
        return writeXLSFile(ctx, book)
    }


    fun exportClients(list: ArrayList<ClientInfo>, dates: String, ctx: Context): Uri? {
        val book = XSSFWorkbook()
        exportClientsToBook(book, list, dates, ctx)
        return writeXLSFile(ctx, book)
    }

    private fun exportBonusesToBook(
        book: XSSFWorkbook,
        list: ArrayList<ReportOperationInfo>,
        dates: String,
        ctx: Context
    ) {
        val sheet = book.createSheet("Report")
        var rowCounter = 0
        var row: Row

        row = sheet.createRow(rowCounter++)
        row.createCell(0).setCellValue(ctx.getString(R.string.report_bonuses_label))

        row = sheet.createRow(rowCounter++)
        row.createCell(0).setCellValue(dates)

        row = sheet.createRow(rowCounter++)

        list.forEach { item ->

            row = sheet.createRow(rowCounter++)
            row.createCell(0).setCellValue(
                when(item.type) {
                    ReportBonusesGroupAdapter.OPERATION_REMOVAL_PURCHASE -> // списывание при покупке
                        ctx.getString(R.string.report_operation_1_rem_purch)

                    ReportBonusesGroupAdapter.OPERATION_ACCRUAL_PURCHASE -> // начисление за покупку
                        ctx.getString(R.string.report_operation_2_acc_purch)

                    ReportBonusesGroupAdapter.OPERATION_FIRE ->			// сгорание бонусов
                        ctx.getString(R.string.report_operation_3_fire)

                    ReportBonusesGroupAdapter.OPERATION_BIRTHDAY_GIFT -> 	// подарок на др
                        ctx.getString(R.string.report_operation_4_bd_gift)

                    ReportBonusesGroupAdapter.OPERATION_ACCRUAL_OTHER -> 	// иное.начисление
                        ctx.getString(R.string.report_operation_5_acc_other)

                    ReportBonusesGroupAdapter.OPERATION_GIFT_FROM_CLIENT -> 	// подарок от клиента
                        ctx.getString(R.string.report_operation_6_client_transm)

                    ReportBonusesGroupAdapter.OPERATION_MOMENTAL -> 	// отправка подарка
                        ctx.getString(R.string.report_operation_0_acc_moment)

                    ReportBonusesGroupAdapter.OPERATION_BONUSES_TOTAL -> 	// Бонусов на счетах клиентов
                        ctx.getString(R.string.report_operation_70_total)

                    ReportBonusesGroupAdapter.OPERATION_BONUSES_IN_PROMISES -> 	// Бонусов обещано клиентам
                        ctx.getString(R.string.report_operation_71_total)

                    else -> ""
                }
            )

            row.createCell(1).setCellValue(item.sum.toString())
        }

    }

    private fun exportPurchasesGoods(
        book: XSSFWorkbook,
        list: List<PaymentInfo>,
        dates: String,
        ctx: Context
    ) {
        val sheet = book.createSheet("Report")
        var rowCounter = 0
        var row: Row

        row = sheet.createRow(rowCounter++)
        row.createCell(0).setCellValue(ctx.getString(R.string.report_purchases_label))

        row = sheet.createRow(rowCounter++)
        row.createCell(0).setCellValue(dates)

        row = sheet.createRow(rowCounter++)

        row = sheet.createRow(rowCounter++)
        row.createCell(0).setCellValue(ctx.getString(R.string.report_header_date))
        row.createCell(1).setCellValue(ctx.getString(R.string.report_header_good_cost))
        row.createCell(2).setCellValue(ctx.getString(R.string.report_header_discount))
        row.createCell(3).setCellValue(ctx.getString(R.string.report_header_total))
        row.createCell(4).setCellValue(ctx.getString(R.string.report_header_client))
        row.createCell(5).setCellValue(ctx.getString(R.string.report_header_cash_back))
        row.createCell(6).setCellValue(ctx.getString(R.string.report_header_mark))
        row.createCell(7).setCellValue(ctx.getString(R.string.report_header_comment))
        row.createCell(8).setCellValue(ctx.getString(R.string.report_header_comment_answer))
        row.createCell(9).setCellValue(ctx.getString(R.string.report_header_note))

        var totalOperations = 0
        var totalGoodCosts = 0
        var totalDiscounts = 0
        var totalTotals = 0

        list.forEach { item ->
            totalOperations++
            totalGoodCosts += item.goodCost
            totalDiscounts += item.usedBonuses
            totalTotals += item.totalCost

            val cashBack = item.cashBack + item.promises
            row = sheet.createRow(rowCounter++)
            row.createCell(0).setCellValue(item.dateTime)
            row.createCell(1).setCellValue(item.goodCost.toString())
            row.createCell(2).setCellValue(item.usedBonuses.toString())
            row.createCell(3).setCellValue(item.totalCost.toString())
            row.createCell(4).setCellValue(item.clientFio)
            row.createCell(5).setCellValue(cashBack.toString())
            row.createCell(6).setCellValue(item.mark.toString())
            row.createCell(7).setCellValue(item.comment)
            row.createCell(8).setCellValue(item.adminComment)
            row.createCell(9).setCellValue(item.note)
        }


        row = sheet.createRow(rowCounter++)

        row = sheet.createRow(rowCounter++)
        row.createCell(0).setCellValue(ctx.getString(R.string.report_bottom_caption))
        row.createCell(1).setCellValue(totalGoodCosts.toString())
        row.createCell(2).setCellValue(totalDiscounts.toString())
        row.createCell(3).setCellValue(totalTotals.toString())

        row = sheet.createRow(rowCounter++)

        row = sheet.createRow(rowCounter++)
        row.createCell(0).setCellValue(ctx.getString(R.string.report_bottom_total_operations))
        row.createCell(1).setCellValue(totalOperations.toString())


    }

    private fun exportClientsToBook(
        book: XSSFWorkbook,
        list: List<ClientInfo>,
        dates: String,
        ctx: Context
    ) {
        val sheet = book.createSheet("Report")
        var rowCounter = 0
        var row: Row

        val oldDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val newDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())


        row = sheet.createRow(rowCounter++)
        row.createCell(0).setCellValue(ctx.getString(R.string.report_new_clients_label))

        row = sheet.createRow(rowCounter++)
        row.createCell(0).setCellValue(dates)

        row = sheet.createRow(rowCounter++)


        //c_id, c_telephone, c_balance, c_gender, c_birthday, c_cardnum, c_blocked, c_fio
        row = sheet.createRow(rowCounter++)
        row.createCell(0).setCellValue(ctx.getString(R.string.report_header_telephone))
        row.createCell(1).setCellValue(ctx.getString(R.string.report_header_card_num))
        row.createCell(2).setCellValue(ctx.getString(R.string.report_header_fio))
        row.createCell(3).setCellValue(ctx.getString(R.string.report_header_balance))
        row.createCell(4).setCellValue(ctx.getString(R.string.report_header_gender))
        row.createCell(5).setCellValue(ctx.getString(R.string.report_header_birthday))

        list.forEach { item ->

            val gender = when(item.gender) {
                1 -> ctx.getString(R.string.male)
                2 -> ctx.getString(R.string.female)
                else -> ""
            }

            val date = try {
                newDateFormat.format(oldDateFormat.parse(item.birthday!!)!!)
            } catch (e:Exception){
                ""
            }

            row = sheet.createRow(rowCounter++)
            row.createCell(0).setCellValue(item.telephone)
            row.createCell(1).setCellValue(item.cardNumber.toString())
            row.createCell(2).setCellValue(item.fio)
            row.createCell(3).setCellValue(item.balance.toString())
            row.createCell(4).setCellValue(gender)
            row.createCell(5).setCellValue(date)
        }


    }

}