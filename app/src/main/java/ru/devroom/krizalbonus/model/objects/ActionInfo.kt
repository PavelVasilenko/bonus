package ru.devroom.krizalbonus.model.objects

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ActionInfo {

  
  @SerializedName("a_id") // id  в базе
  @Expose
  var serverId: Int = -1

  @SerializedName("a_date_time") // время в UNIX формате
  @Expose
  var dateTime: Long = 0

  @SerializedName("a_employee_id") // id оформившего сотрудника
  @Expose
  var employeeId: Int = -1

  @SerializedName("a_active") // активна (0=нет/1=да)
  @Expose
  var active: Int = 0

  @SerializedName("a_text") // текст акции
  @Expose
  var text: String = ""

  @SerializedName("a_caption") // название акции
  @Expose
  var caption: String = ""

  @SerializedName("a_end_time") // время окончания в UNIX формате
  @Expose
  var endTime: Long = 0

}
