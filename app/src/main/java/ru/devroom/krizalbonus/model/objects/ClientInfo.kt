package ru.devroom.krizalbonus.model.objects

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ClientInfo {

  @SerializedName("c_id") // id  в базе
  @Expose
  var serverId: Int = -1

  @SerializedName("c_telephone") // телефон клиента
  @Expose
  var telephone: String = ""

  @SerializedName("c_email") // email клиента
  @Expose
  var email: String? = ""

  @SerializedName("c_password") // зашифрованный пароль
  @Expose
  var password: String = ""

  @SerializedName("c_token") //
  @Expose
  var token: String = ""

  @SerializedName("c_token_expired") //
  @Expose
  var tokenExpired: String = ""

  @SerializedName("c_balance") // остаток бонусов
  @Expose
  var balance: Int = 0

  @SerializedName("c_balance_expired") // время, когда сгорают бонусы
  @Expose
  var balanceExpired: String? = ""

  @SerializedName("c_gender") // пол (1=М/2=Ж)
  @Expose
  var gender: Int = 0

  @SerializedName("c_birthday") // дата рождения
  @Expose
  var birthday: String? = ""

  @SerializedName("c_cardnum") // номер карты пользователя
  @Expose
  var cardNumber: Int = 0

  @SerializedName("c_blocked") // пользователь заблокирован (0=нет/1=да)
  @Expose
  var blocked: Int = 0

  @SerializedName("c_fio") // ФИО
  @Expose
  var fio: String = ""

  @SerializedName("c_purchases_sum") // общая сумма покупок
  @Expose
  var purchasesSum: Int = 0

  @SerializedName("level")	// текущий уровень в бонусной системе
  @Expose
  var levelInfo: LevelInfo? = null

  @SerializedName("promises") // сколько еще не начислено бонусов
  @Expose
  var promises: Int = 0

  @SerializedName("last_purchases") // сколько еще не начислено бонусов
  @Expose
  var lastPurchases: ArrayList<OperationInfo>? = null


}
