package ru.devroom.krizalbonus.ui.reports.bonuses_activity

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.yanzhenjie.permission.AndPermission
import com.yanzhenjie.permission.runtime.Permission
import kotlinx.android.synthetic.main.activity_report_bonuses.*
import kotlinx.android.synthetic.main.dialog_select_dates_range.view.*
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.objects.ReportOperationInfo
import ru.devroom.krizalbonus.ui.reports.purchases_activity.ReportPurchasesAdapter
import java.util.*
import kotlin.collections.ArrayList

class ReportBonusesActivity : MvpAppCompatActivity(), IReportBonusesActivity {

    @InjectPresenter
    lateinit var mPresenter:ReportBonusesActivityPresenter

    private var adapter: ReportBonusesGroupAdapter? = null

    private var selectedDateStart: Calendar? = null
    private var selectedDateEnd: Calendar? = null
    private var daterangeSelect: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_bonuses)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)


        adapter = ReportBonusesGroupAdapter()

        rvPurchasesList?.layoutManager = LinearLayoutManager(this)
        rvPurchasesList.adapter = adapter

        ibSelectDocsDate.setOnClickListener(onDateSelectClickListener)
        ibExport.setOnClickListener { onExportClicked() }

        mPresenter.setTodayDateRange()
        hideProgress()

    }

    private fun onExportClicked() {
        AndPermission.with(this)
            .runtime()
            .permission(Permission.Group.STORAGE)
            .onGranted { _ ->
                mPresenter.export(this@ReportBonusesActivity)
            }
            .onDenied { _ ->
                AlertDialog
                    .Builder(this)
                    .setMessage(R.string.write_storage_denied)
                    .setPositiveButton(android.R.string.ok){ _: DialogInterface, _: Int -> }
                    .show()
            }
            .start()
    }

    override fun openFile(file: Uri) {
        val install = Intent(Intent.ACTION_VIEW)
        install.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
        install.setDataAndType(file, getMimeType(file.path ?: ""))
        install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(install)
    }

    private fun getMimeType(fileUri: String): String {
        var mimeType: String? = null
        val extension = MimeTypeMap.getFileExtensionFromUrl(fileUri)
        if (extension != null) {
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
        }
        return mimeType ?: "*/*"
    }

    override fun insertReportList(data: ArrayList<ReportOperationInfo>) {
        adapter?.setData(data)
    }

    override fun errorLoading() {
        Toast.makeText(this, R.string.error_while_loading, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressBar4.visibility = View.VISIBLE
    }

    override fun onSupportNavigateUp(): Boolean {

        onBackPressed()
        return true
    }

    override fun hideProgress() {
        progressBar4.visibility = View.GONE
    }

    override fun setDates(dates: String) {
        tvDates.text = dates
    }

    override fun errorAuth() {
        Toast.makeText(this, R.string.error_while_auth, Toast.LENGTH_SHORT).show()
    }


    private val onDateSelectClickListener = View.OnClickListener {

        if (daterangeSelect == null) {

            val view = layoutInflater.inflate(R.layout.dialog_select_dates_range, null)
            view.calendar.setCalendarListener(object : DateRangeCalendarView.CalendarListener {
                override fun onDateRangeSelected(startDate: Calendar?, endDate: Calendar?) {
                    selectedDateStart = startDate
                    selectedDateEnd = endDate
                }

                override fun onFirstDateSelected(startDate: Calendar?) {
                    selectedDateStart = startDate
                    selectedDateEnd = startDate
                }
            })
            view.calendar.setWeekOffset(1)

            daterangeSelect = AlertDialog.Builder(this)
                .setView(view)
                .setNeutralButton(R.string.payment_all_dates){ _: DialogInterface, _: Int ->
                    mPresenter.setFullDateRange()
                }
                .setNegativeButton(R.string.payment_today) { _: DialogInterface, _: Int ->
                    mPresenter.setTodayDateRange()
                }
                .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                    if (selectedDateStart != null) {
                        mPresenter.setDateRange(
                            selectedDateStart!!.timeInMillis,
                            selectedDateEnd!!.timeInMillis
                        )
                    }
                }.create()
        }


        val calendar: DateRangeCalendarView? =
            daterangeSelect?.findViewById<DateRangeCalendarView>(R.id.calendar)
        if (calendar != null) {
            if (!mPresenter.isAllTimeSelected()) {
                val startSelectionDate = mPresenter.getStartDateRange()
                val endSelectionDate = mPresenter.getEndDateRange()
                calendar.setSelectedDateRange(startSelectionDate, endSelectionDate)
            } else {
                calendar.setSelectedDateRange(null, null)
            }
        }

        daterangeSelect!!.show()
    }
}
