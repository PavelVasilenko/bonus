package ru.devroom.krizalbonus.ui.payments

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_payment.*
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.objects.PaymentInfo
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.dialog_select_dates_range.view.*
import ru.devroom.krizalbonus.IFragmentCallback
import ru.devroom.krizalbonus.ui.client_card.ClientCardActivity
import ru.devroom.krizalbonus.ui.main_activity.MainActivity
import ru.devroom.krizalbonus.ui.purchase_info.PurchaseInfoActivity
import java.util.*
import kotlin.collections.ArrayList
import java.text.SimpleDateFormat

class PaymentFragment : MvpAppCompatFragment(), IPaymentFragment, PaymentsAdapter.ItemClickListener
//    ,DatePickerDialog.OnDateSetListener
{

    @InjectPresenter
    lateinit var mPresenter: PaymentFragmentPresenter

    companion object {
        const val RESULT_FOR_EDIT_PAYMENT = 74
        const val RESULT_FOR_UPDATE_LIST = 77
    }

    private var listener: IFragmentCallback? = null

    private var selectedDateStart: Calendar? = null
    private var selectedDateEnd: Calendar? = null
    private var daterangeSelect: AlertDialog? = null


    // адаптер списка
    private var adapter: PaymentsAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_payment, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listener?.inflateMenu(null)

        adapter = PaymentsAdapter()
        adapter!!.setClickListener(this)

        rvPurchasesList?.layoutManager = LinearLayoutManager(context)
        rvPurchasesList.adapter = adapter

        ibSelectDocsDate.setOnClickListener(onDateSelectClickListener)
//        {
//
//            // подготавливаем дату для календаря
//            val calendar = Calendar.getInstance()
//            val lastDate = mPresenter.getSelectedDate()
//            val sdf = SimpleDateFormat("y-MM-d", Locale.ENGLISH)
//            val date = sdf.parse(lastDate)
//            if (date != null) calendar.time = date
//
//            val day = calendar.get(Calendar.DAY_OF_MONTH)
//            val month = calendar.get(Calendar.MONTH)
//            val year = calendar.get(Calendar.YEAR)
//            // показываем календарь
//            DatePickerDialog(context!!, this@PaymentFragment, year, month, day).show()
//        }

        ibOpenClientCard.setOnClickListener {
            mPresenter.openClientCard(etClientCode.text.toString())
        }

        ibScanQRCode.setOnClickListener {
            val integrator = IntentIntegrator(context as MainActivity)
            integrator.setOrientationLocked(false)
            integrator.setBeepEnabled(true)
            integrator.initiateScan()
        }


//        mPresenter.updatePaymentsList()
        mPresenter.setTodayDateRange()
        hideProgress()
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is IFragmentCallback) {
            listener = context
        }
    }

    private val onDateSelectClickListener = View.OnClickListener {

        if (daterangeSelect == null) {

            val view = layoutInflater.inflate(R.layout.dialog_select_dates_range, null)
            view.calendar.setCalendarListener(object : DateRangeCalendarView.CalendarListener {
                override fun onDateRangeSelected(startDate: Calendar?, endDate: Calendar?) {
                    selectedDateStart = startDate
                    selectedDateEnd = endDate
                }

                override fun onFirstDateSelected(startDate: Calendar?) {
                    selectedDateStart = startDate
                    selectedDateEnd = startDate
                }
            })
            view.calendar.setWeekOffset(1)

            daterangeSelect = AlertDialog.Builder(context)
                .setView(view)
                .setNeutralButton(R.string.payment_today) { _: DialogInterface, _: Int ->
                    mPresenter.setTodayDateRange()
                }
                .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                    if (selectedDateStart != null) {
                        mPresenter.setDateRange(
                            selectedDateStart!!.timeInMillis,
                            selectedDateEnd!!.timeInMillis
                        )
                    }
                }.create()
        }


        val calendar: DateRangeCalendarView? =
            daterangeSelect?.findViewById<DateRangeCalendarView>(R.id.calendar)
        if (calendar != null) {
            val startSelectionDate = mPresenter.getStartDateRange()
            val endSelectionDate = mPresenter.getEndDateRange()
            calendar.setSelectedDateRange(startSelectionDate, endSelectionDate)
        }

        daterangeSelect!!.show()
    }

    override fun setDates(dates: String) {
        tvDates.text = dates
    }

    override fun onResume() {
        super.onResume()
        mPresenter.updatePaymentsList()
    }

    override fun openClientCard(id: Int, openMode: String) {
        val intent = Intent(context, ClientCardActivity::class.java)
        intent.putExtra(ClientCardActivity.PARAM_CLIENT_CODE, id)
        intent.putExtra(ClientCardActivity.PARAM_CLIENT_OPENMODE, openMode)
//        intent.putExtra(ClientCardActivity.PARAM_CLIENT_OPENMODE, ClientCardActivity.MODE_VIEW)
        startActivityForResult(intent, RESULT_FOR_EDIT_PAYMENT)
    }

    override fun errorClientCode() {
        etClientCode.error = getString(R.string.edit_field_empty)
    }

//    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
    // собираем дату
//        val calendar = Calendar.getInstance()
//        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
//        calendar.set(Calendar.MONTH, month)
//        calendar.set(Calendar.YEAR, year)
//        calendar.set(Calendar.HOUR_OF_DAY, 1)
    // передаем дату в презентер
//        mPresenter.setDate(calendar.timeInMillis)
//    }

    override fun insertPaymentsList(data: ArrayList<PaymentInfo>) {
        if (data.isEmpty()) {
            rvPurchasesList.visibility = View.GONE
//            toolbar.visibility = View.GONE
        } else {
            rvPurchasesList.visibility = View.VISIBLE
//            toolbar.visibility = View.VISIBLE
        }

        adapter?.setData(data)
    }

    override fun errorLoading() {
        Toast.makeText(context, R.string.error_while_loading, Toast.LENGTH_SHORT).show()
    }

    override fun errorAuth() {
        Toast.makeText(context, R.string.error_while_auth, Toast.LENGTH_SHORT).show()
        listener?.onAuthError()
    }


    override fun showProgress() {
        progressBar4.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar4.visibility = View.GONE
    }

    override fun onItemClick(view: View, item: PaymentInfo) {

        val packedItem = Gson().toJson(item, PaymentInfo::class.java)
        val intent = Intent(context, PurchaseInfoActivity::class.java)
        intent.putExtra(PurchaseInfoActivity.SERIALIZED_ITEM, packedItem)
        startActivityForResult(intent, RESULT_FOR_UPDATE_LIST)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

//        when (requestCode) {
//            RESULT_FOR_EDIT -> {
//                if (resultCode == Codes.OK) {
//                    mPresenter.updatePaymentsList()
//                }
//            }
//        }
    }
}