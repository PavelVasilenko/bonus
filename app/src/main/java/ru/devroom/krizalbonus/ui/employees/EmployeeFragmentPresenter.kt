package ru.devroom.krizalbonus.ui.employees

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.devroom.krizalbonus.model.AppUser
import ru.devroom.krizalbonus.model.network.RetrofitInstance

@InjectViewState
class EmployeeFragmentPresenter:MvpPresenter<IEmployeeFragment>() {


    // флаг текущего сеанса отправки/загрузки
    @Volatile var serverBusy = false

    // обновление списка
    fun updateEmployeesList() {
        // если сервер занят - выходим
        if(serverBusy) return

        val token = AppUser.get().getEmployeeInfo()?.token
        if(token == null) {
            viewState.errorAuth()
            return
        }

        // показать анимацию загрузки данных
        viewState.showProgress()

        // в новом потоке - запрашиваем данные с сервера
        GlobalScope.launch {
            // ставим флаг активной загрузки
            serverBusy = true
            // запрос на получение данных
            val response = try {
                RetrofitInstance.getService.getEmployeesListAsync(token).await()
            } catch (e:Exception) {
                null
            }

            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true && response.body() != null) {
                    viewState.insertEmployeesList(response.body()!!)
                } else {
                    when (response?.code()) {
                        401 -> {
                            viewState.errorAuth()
                        }
                        else -> {
                            viewState.errorLoading()
                        }
                    }
                }
                // убираем флаг текущей загрузки
                serverBusy = false
                // прячем анимацию загрузки данных
                viewState.hideProgress()
            }
        }
    }
    //

}