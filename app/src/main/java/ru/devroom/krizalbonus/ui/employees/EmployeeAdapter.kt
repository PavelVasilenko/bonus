package ru.devroom.krizalbonus.ui.employees

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.objects.EmployeeInfo
import kotlin.collections.ArrayList

class EmployeeAdapter: RecyclerView.Adapter<EmployeeAdapter.ViewHolder>(){
    // список документов
    private var data:List<EmployeeInfo> = ArrayList()
    //обработчик нажатия на элемент списка
    private lateinit var mClickListener : ItemClickListener

    // метод для установки нового списка
    fun setData(newData:List<EmployeeInfo>?) {
        // запоминаем список
        if(newData.isNullOrEmpty()) {
            this.data = ArrayList()
        } else {
            this.data = newData
        }
        notifyDataSetChanged() // уведомляем менеджер о том, что список поменялся
    }

    // воздаем вью для элемента списка
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_employee, parent, false)
        )
    }

    // при присоединиении элемента списка к recyclerView
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // оформляем данные внутри элемента списка
        holder.bindData(data[position], mClickListener)
    }

    // возвращаем кол-во элементов списка
    override fun getItemCount(): Int {
        return data.size
    }

    // внутренний класс для хранения и настройки элементов карточки клиента
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private var tvFamily: TextView? = null
        private var tvRole: TextView? = null
        private var tvLogin: TextView? = null
        private var listener: ItemClickListener? = null

        private lateinit var item: EmployeeInfo

        init {
            // находим текстовые поля, в которые будем вставлять даные
            tvFamily = itemView.findViewById(R.id.tvFamily)
            tvRole = itemView.findViewById(R.id.tvRole)
            tvLogin = itemView.findViewById(R.id.tvLogin)

            // применяем обработчик нажатия на элемент списка
            itemView.setOnClickListener(this)
        }

        fun bindData(data: EmployeeInfo, listener: ItemClickListener) {

            val ctx = itemView.context

            tvFamily?.text = data.family
            tvLogin?.text = data.login
            tvRole?.text = if(data.isAdminRole())
                ctx.getString(R.string.adminRole)
            else
                ctx.getString(R.string.operatorRole)

            // запоминаем объект, по которому сделана эта карточка
            this.item = data
            // запоминаем слушатель событий
            this.listener = listener
        }

        // обработчик нажатия на элемент
        override fun onClick(view: View) {
            if (listener != null)
                listener!!.onItemClick(view, item)
        }
    }

    // метод для применения обработчика нажатий на элемент списка
    fun setClickListener(itemClickListener: ItemClickListener) {
        this.mClickListener = itemClickListener
    }

    // интерфейс, описывающий метод обработчика нажатий на элемент
    interface ItemClickListener {
        fun onItemClick(view:View, item: EmployeeInfo)
    }
}