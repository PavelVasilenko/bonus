package ru.devroom.krizalbonus.ui.employees

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.devroom.krizalbonus.model.objects.EmployeeInfo

interface IEmployeeFragment:MvpView {
    @StateStrategyType(SingleStateStrategy::class)
    fun insertEmployeesList(data:ArrayList<EmployeeInfo>)

    @StateStrategyType(SkipStrategy::class)
    fun errorLoading()

    @StateStrategyType(SkipStrategy::class)
    fun errorAuth()

    fun showProgress()

    fun hideProgress()

}