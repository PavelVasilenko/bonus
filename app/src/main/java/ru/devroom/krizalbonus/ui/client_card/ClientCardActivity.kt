package ru.devroom.krizalbonus.ui.client_card

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.activity_client_card.*
import kotlinx.android.synthetic.main.item_level.*
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.Codes
import ru.devroom.krizalbonus.model.objects.ClientInfo
import ru.devroom.krizalbonus.model.objects.PaymentInfo
import ru.devroom.krizalbonus.ui.payments.PaymentsAdapter
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.include_pay_form.*
import ru.devroom.krizalbonus.model.objects.OperationInfo
import ru.devroom.krizalbonus.ui.edit_client.EditClientActivity
import ru.devroom.krizalbonus.ui.reports.purchases_activity.ReportPurchasesActivity
import ru.devroom.krizalbonus.ui.reports.purchases_activity.ReportPurchasesActivity.Companion.PARAM_CLIENT_CARD_CODE


class ClientCardActivity : MvpAppCompatActivity(), IClientCardActivity,
    PaymentsAdapter.ItemClickListener {

    @InjectPresenter
    lateinit var mPresenter: ClientCardActivityPresenter

    // адаптер списка
    private var adapter: OperationsAdapter? = null

    companion object {
        const val PARAM_CLIENT_CODE = "client_code"
        const val PARAM_CLIENT_OPENMODE = "open_mode"
        const val MODE_VIEW = "mode_view"
        const val MODE_PURCHASE = "mode_purchase"

        const val ANSWER_CODE_UPDATE = 77
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_client_card)


        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val mode = intent.getStringExtra(PARAM_CLIENT_OPENMODE)
        val clientCode = intent.getIntExtra(PARAM_CLIENT_CODE, -1)

        if (mode.isNullOrEmpty() || clientCode < 0) finish()

        fabReload.setOnClickListener {
            mPresenter.updateClientInfo()
        }

        if (mode == MODE_VIEW) {
            clPurchasing.visibility = View.GONE
            fabNewPurchase.show()
        } else {
            clPurchasing.visibility = View.VISIBLE
            fabNewPurchase.hide()
        }

        bRepeatPay.setOnClickListener {
            mPresenter.scrollBackToPay()
        }

        fabNewPurchase.setOnClickListener {
            mPresenter.showPurchase()
        }

        swUseBonuses.setOnCheckedChangeListener { _, _ ->
            mPresenter.setUseBonuses(swUseBonuses.isChecked)
            mPresenter.calculate()
        }

        etPayGoodsCost.addTextChangedListener {
            mPresenter.setGoodsCost(etPayGoodsCost.text.toString())
            mPresenter.calculate()
        }

        etUsedBonuses.addTextChangedListener {
            mPresenter.setBonusesSum(etUsedBonuses.text.toString())
            mPresenter.calculate()
        }

        ibUndoCustomBonusSum.setOnClickListener {
            mPresenter.setUseCustomBonusSum(false)
            mPresenter.calculate()
        }
        ibSetCustomBonusSum.setOnClickListener {
            mPresenter.setUseCustomBonusSum(true)
            mPresenter.calculate()
        }

        ibPurchaseCommit.setOnClickListener {
            hideKeyboard()
            mPresenter.onCommitClicked(etNoteText.text.toString())
        }

        adapter = OperationsAdapter()

        rvLastTenOperations?.layoutManager = LinearLayoutManager(this)
        rvLastTenOperations.adapter = adapter

        progressBar2.visibility = View.VISIBLE
        etNoteText.visibility = View.GONE
        bSetNote.visibility = View.VISIBLE

        bSetNote.setOnClickListener {
            etNoteText.visibility = View.VISIBLE
            ibCancelNote.visibility = View.VISIBLE
            bSetNote.visibility = View.GONE
            etNoteText.requestFocus()
        }

        ibCancelNote.setOnClickListener {
            etNoteText.visibility = View.GONE
            ibCancelNote.visibility = View.GONE
            bSetNote.visibility = View.VISIBLE
            etNoteText.setText("")
        }

        mPresenter.initWithClientViews(clientCode, mode!!)

    }

    override fun insertClientOperations(data: ArrayList<OperationInfo>) {
        adapter?.setData(data)
    }

    override fun onStop() {
        super.onStop()
        setResult(Codes.OK)
    }

    override fun onSupportNavigateUp(): Boolean {

        onBackPressed()
        return true
    }

    override fun insertClientInfo(data: ClientInfo) {
        scrollView.visibility = View.VISIBLE
        tvFio.text = data.fio
        tvTelephone.text = data.telephone
        tvCardIdValue.text = data.cardNumber.toString()
        tvPurchasesValue.text = data.purchasesSum.toString()
        tvBirthdayValue.text = data.birthday
        tvBonusesValue.text = data.balance.toString()
        tvPromisesValue.text = data.promises.toString()
        tvEndTimeValue.text = data.balanceExpired

        tvGender.text =
            if (data.gender == 1) getString(R.string.male) else getString(R.string.female)

        adapter?.setData(data.lastPurchases)

        val caption = getString(R.string.client_caption) + " " + data.cardNumber.toString()
        supportActionBar?.title = caption

        if (data.levelInfo != null) {
            tvPurchasesSumValue.text = data.levelInfo!!.totalPurchases.toString()
            tvCashBackValue.text = data.levelInfo!!.cashBack.toString()
            tvMaxDiscountValue.text = data.levelInfo!!.maxDiscount.toString()
            tvLiveTimeValue.text = data.levelInfo!!.liveTime.toString()
            tvAddTimeValue.text = data.levelInfo!!.delayDays.toString()
        } else {
            textView25.visibility = View.GONE
            clientLevel.visibility = View.GONE
        }
    }

    override fun errorEmptyTotalCost() {
        Toast.makeText(this, R.string.client_pay_error_empty, Toast.LENGTH_SHORT).show()
    }

    override fun errorWrongCommitCode() {
        Toast.makeText(this, R.string.client_wrong_commit_code, Toast.LENGTH_SHORT).show()
    }

    override fun clientDeleted() {
        Toast.makeText(this, R.string.client_deleted, Toast.LENGTH_SHORT).show()
        setResult(Codes.OK)
        finish()
    }

    override fun errorWhileDeleting() {
        Toast.makeText(this, R.string.client_error_while_deleting, Toast.LENGTH_SHORT).show()
    }

    private fun hideKeyboard() {
        val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = this.currentFocus
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun showPurchaseLayout(animation: Boolean, completeShowed: Boolean) {
        if (completeShowed) {
            clPayCompleted.translationX = 0f
            clPurchasing.translationX = -clPurchasing.width.toFloat()
        } else {
            clPayCompleted.translationX = 0f
            clPurchasing.translationX = clPurchasing.width.toFloat()
        }


        if (!animation) {
            clPurchasing.visibility = View.VISIBLE
        } else {
            if (clPurchasing.visibility == View.GONE) {
                clPurchasing.alpha = 0f
                clPurchasing.visibility = View.VISIBLE
                clPurchasing.animate()
                    .alpha(1f).duration = 1000
            }
        }
    }

    override fun hidePurchaseLayout() {
        clPurchasing.visibility = View.GONE
    }

    override fun clientNotFound() {
        tvErrorText.visibility = View.VISIBLE
        tvErrorText.text = getString(R.string.client_not_found)
    }

    override fun clearGoodCost() {
        etPayGoodsCost.setText("")
        etNoteText.setText("")
        etNoteText.visibility = View.GONE
        bSetNote.visibility = View.VISIBLE
    }

    override fun errorLoading() {
        tvErrorText.visibility = View.VISIBLE
        tvErrorText.text = getString(R.string.client_error_connection)
    }

    override fun errorAuth() {
        Toast.makeText(this, R.string.error_while_auth, Toast.LENGTH_SHORT).show()
    }

    override fun clientNotFoundToast() {
        Toast.makeText(this, R.string.client_not_found, Toast.LENGTH_SHORT).show()
    }

    override fun clientNotAuthYet() {
        Toast.makeText(this, R.string.client_toast_client_not_auth_yet, Toast.LENGTH_SHORT).show()
    }

    override fun errorDeleting() {
        Toast.makeText(this, R.string.error_while_deleting_purchase, Toast.LENGTH_SHORT).show()
    }

    override fun updateCalculation(discount: Int, cashBack: Int, total: Int) {
        if(etUsedBonuses.text.toString() != discount.toString())
            etUsedBonuses.setText(discount.toString())
        tvPayCashBack.text = cashBack.toString()
        tvPayTotalValue.text = total.toString()

        val useBonuses = mPresenter.useBonuses()
        val customValue = mPresenter.useCustomBonusSum()

        swUseBonuses.isChecked = useBonuses
        if(useBonuses) {
            if (customValue) {
                ibUndoCustomBonusSum.visibility = View.VISIBLE
                ibSetCustomBonusSum.visibility = View.GONE
                etUsedBonuses.isEnabled = true
//                etUsedBonuses.isFocusable = true
            } else {
                ibUndoCustomBonusSum.visibility = View.GONE
                ibSetCustomBonusSum.visibility = View.VISIBLE
                etUsedBonuses.isEnabled = false
//                etUsedBonuses.isFocusable = false
            }
        } else {
            ibUndoCustomBonusSum.visibility = View.GONE
            ibSetCustomBonusSum.visibility = View.GONE
            etUsedBonuses.isEnabled = false
        }
    }

    override fun showProgress() {
        progressBar2.visibility = View.VISIBLE
    }

    override fun showScroll() {
        scrollView.visibility = View.VISIBLE
    }

    override fun hideScroll() {
        scrollView.visibility = View.GONE
    }

    override fun hideProgress() {
        progressBar2.visibility = View.GONE
    }

    override fun showFabPurchase() {
        fabNewPurchase.show()
    }

    override fun hideFabPurchase() {
        fabNewPurchase.hide()
    }

    override fun showFabReload() {
        fabReload.show()
    }

    override fun scrollToPay() {
        clPayForm.translationX = -clPayForm.width.toFloat()
        clPayForm.animate().translationX(0f).duration = 300
        clPayCompleted.animate().translationX(clPayForm.width.toFloat()).duration = 300
    }

    override fun showPayComplete() {
        clPayCompleted.translationX = clPayForm.width.toFloat()
        clPayForm.animate().translationX(-clPayForm.width.toFloat()).duration = 300
        clPayCompleted.animate().translationX(0f).duration = 300
    }

    override fun clientLocked() {
        tvErrorText.visibility = View.VISIBLE
        tvErrorText.text = getString(R.string.error_while_pay_locked)

    }

    override fun errorWhilePay() {
        Toast.makeText(this, R.string.error_while_pay, Toast.LENGTH_SHORT).show()
    }

    override fun hideError() {
        tvErrorText.visibility = View.GONE
    }

    override fun hideFabReload() {
        fabReload.hide()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return if (mPresenter.isAdmin()) {
            menuInflater.inflate(R.menu.client_card_menu, menu)
            true
        } else {
            menuInflater.inflate(R.menu.client_card_menu_kassir, menu)
            true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.menu_send_push -> {
                generateSendPushDialog()
                true
            }
            R.id.menu_bon_add -> {
                generateAddBonusesDialog()
                true
            }
            R.id.menu_client_report -> {
                openClientReport()
                true
            }
            R.id.menu_bon_rem -> {
                generateClearBonusesDialog()
                true
            }
            R.id.menu_change_info -> {
                openEditProfileActivity()
                true
            }
            R.id.menu_change_password -> {
                generateChangePasswordDialog()
                true
            }
            R.id.menu_delete_client -> {
                mPresenter.generateAcceptToDeleteDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun openEditProfileActivity() {
        val clientInfo = mPresenter.getClientProfile()?:return
        val serialized = Gson().toJson(clientInfo)
        val intent = Intent(this, EditClientActivity::class.java)
        intent.putExtra(EditClientActivity.PARAM_EDITED_USER, serialized)
        startActivityForResult(intent, ANSWER_CODE_UPDATE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            ANSWER_CODE_UPDATE -> {
                if(resultCode == Codes.OK) {
                    mPresenter.updateClientInfo(true)
                }
            }
        }
    }

    private fun openClientReport() {
        val cardId = mPresenter.getClientCardId()?:return
        val intent = Intent(this, ReportPurchasesActivity::class.java)
        intent.putExtra(PARAM_CLIENT_CARD_CODE, cardId)
        startActivity(intent)
    }

    private fun generateSendPushDialog() {
        var dialog: AlertDialog? = null
        val dialogView = layoutInflater.inflate(R.layout.dialog_send_push, null)
        val editText = dialogView.findViewById<EditText>(R.id.etMessageText)
        dialogView.findViewById<Button>(R.id.bCancel)?.setOnClickListener {
            dialog?.dismiss()
        }
        dialogView.findViewById<Button>(R.id.bCommit)?.setOnClickListener {
            mPresenter.sendPushToClient(editText.text.toString())
            dialog?.dismiss()
        }
        dialog = AlertDialog.Builder(this@ClientCardActivity).setView(dialogView).show()
    }

    override fun showAnswerToClientDialog(payment: PaymentInfo) {
        var dialog: AlertDialog? = null
        val dialogView = layoutInflater.inflate(R.layout.dialog_answer_for_comment, null)
        val editText = dialogView.findViewById<EditText>(R.id.etMessageText)
        dialogView.findViewById<Button>(R.id.bCancel)?.setOnClickListener {
            dialog?.dismiss()
        }
        dialogView.findViewById<Button>(R.id.bCommit)?.setOnClickListener {
            mPresenter.sendAnswerForComment(payment, editText.text.toString())
            dialog?.dismiss()
        }
        dialog = AlertDialog.Builder(this@ClientCardActivity).setView(dialogView).show()
    }

    private fun generateAddBonusesDialog() {
        var dialog: AlertDialog? = null
        val dialogView = layoutInflater.inflate(R.layout.dialog_bonuses_add, null)
        val editText = dialogView.findViewById<EditText>(R.id.etBonusesSum)
        dialogView.findViewById<Button>(R.id.bCancel)?.setOnClickListener {
            dialog?.dismiss()
        }
        dialogView.findViewById<Button>(R.id.bCommit)?.setOnClickListener {
            mPresenter.sendAddBonusesToClient(editText.text.toString())
            dialog?.dismiss()
        }
        dialog = AlertDialog.Builder(this@ClientCardActivity).setView(dialogView).show()
    }

    private fun generateClearBonusesDialog() {
        var dialog: AlertDialog? = null
        val dialogView = layoutInflater.inflate(R.layout.dialog_bonuses_erase, null)
        dialogView.findViewById<Button>(R.id.bCancel)?.setOnClickListener {
            dialog?.dismiss()
        }
        dialogView.findViewById<Button>(R.id.bCommit)?.setOnClickListener {
            mPresenter.sendEraseClientsBonuses()
            dialog?.dismiss()
        }
        dialog = AlertDialog.Builder(this@ClientCardActivity).setView(dialogView).show()
    }

    private fun generateChangePasswordDialog() {
        var dialog: AlertDialog? = null

        val dialogView = layoutInflater.inflate(R.layout.dialog_change_client_password, null)
        val password1 = dialogView.findViewById(R.id.etNewPassword) as EditText
        val password2 = dialogView.findViewById(R.id.etNewPasswordRepeat) as EditText

        dialogView.findViewById<Button>(R.id.bCancel)?.setOnClickListener {
            dialog?.dismiss()
        }
        dialogView.findViewById<Button>(R.id.bCommit)?.setOnClickListener {
            mPresenter.sendNewClientPassword(password1.text.toString(), password2.text.toString())
            dialog?.dismiss()
        }
        dialog = AlertDialog.Builder(this@ClientCardActivity).setView(dialogView).show()
    }

    override fun showAcceptToDeleteDialog(code: String) {
        var dialog: AlertDialog? = null

        val dialogView = layoutInflater.inflate(R.layout.dialog_accept_to_delete, null)
        dialogView.findViewById<TextView>(R.id.tvCheckCode)?.text = code

        val bCancel = dialogView.findViewById<Button>(R.id.bCancelDeleting)
        val bCommit = dialogView.findViewById<Button>(R.id.bCommit)
        val etTypedCode = dialogView.findViewById<EditText>(R.id.etTypedCode)

        bCommit?.setOnClickListener {
            dialog?.dismiss()
            mPresenter.enteredDeleteCode(etTypedCode.text.toString())
        }


        bCancel?.setOnClickListener {
            dialog?.dismiss()
        }


        dialog = AlertDialog.Builder(this@ClientCardActivity).setView(dialogView).show()
    }

    override fun onItemClick(view: View, item: PaymentInfo) {
        var dialog: AlertDialog? = null

        val dialogView = layoutInflater.inflate(R.layout.dialog_purchase_info, null)
        dialogView.findViewById<TextView>(R.id.tvDocumentDate)?.text = item.dateTime
        dialogView.findViewById<TextView>(R.id.tvCardNumber)?.text = item.clientId.toString()
        dialogView.findViewById<TextView>(R.id.tvClientFio)?.visibility = View.GONE
        dialogView.findViewById<TextView>(R.id.tvGoodCost)?.text = item.goodCost.toString()
        dialogView.findViewById<TextView>(R.id.tvDiscountSize)?.text = item.usedBonuses.toString()
        dialogView.findViewById<TextView>(R.id.tvPayedTotal)?.text = item.totalCost.toString()

        val tvClientMark = dialogView.findViewById<TextView>(R.id.tvClientMark)
        val tvClientComment = dialogView.findViewById<TextView>(R.id.tvClientComment)
        val tvAdminComment = dialogView.findViewById<TextView>(R.id.tvAdminComment)
        val textView35 = dialogView.findViewById<TextView>(R.id.textView35)

        if (item.mark > 0) {
            tvClientMark?.visibility = View.VISIBLE
            tvClientMark.text = getString(R.string.payment_mark, item.mark)
        } else {
            tvClientMark?.visibility = View.GONE
        }

        if (item.comment.isEmpty()) {
            tvClientComment?.visibility = View.GONE
            textView35?.visibility = View.GONE
        } else {
            tvClientComment?.visibility = View.VISIBLE
            textView35?.visibility = View.VISIBLE
            tvClientComment.text = item.comment
        }

        if (item.adminComment.isEmpty()) {
            tvAdminComment?.visibility = View.GONE
        } else {
            tvAdminComment?.visibility = View.VISIBLE
            tvAdminComment.text = item.adminComment
        }

        if(mPresenter.isAdmin()) {
            dialogView.findViewById<ImageButton>(R.id.ibDelete)?.setOnClickListener {
                dialog?.dismiss()
                AlertDialog.Builder(this@ClientCardActivity)
                    .setTitle(R.string.dialog_caption)
                    .setMessage(R.string.dialog_delete_purchase)
                    .setNegativeButton(android.R.string.cancel) { _: DialogInterface, _: Int -> }
                    .setPositiveButton(android.R.string.yes) { _: DialogInterface, _: Int ->
                        mPresenter.deletePurchase(item.serverId)
                    }
                    .show()
            }
            dialogView.findViewById<ImageButton>(R.id.ibMessage)?.setOnClickListener {
                dialog?.dismiss()
                mPresenter.showWriteAnswerToClient(item)
//                dialog?.dismiss()
//                AlertDialog.Builder(this@ClientCardActivity)
//                    .setTitle(R.string.dialog_caption)
//                    .setMessage(R.string.dialog_delete_purchase)
//                    .setNegativeButton(android.R.string.cancel) { _: DialogInterface, _: Int -> }
//                    .setPositiveButton(android.R.string.yes) { _: DialogInterface, _: Int ->
//                        mPresenter.deletePurchase(item.serverId)
//                    }
//                    .show()
            }
        } else {
            dialogView.findViewById<ImageButton>(R.id.ibDelete)?.visibility = View.GONE
            dialogView.findViewById<ImageButton>(R.id.ibMessage)?.visibility = View.GONE
        }

        dialogView.findViewById<ImageButton>(R.id.ibClientCard)?.visibility = View.GONE

        dialogView.findViewById<ImageButton>(R.id.ibClose)?.setOnClickListener {
            dialog?.dismiss()
        }


        dialog = AlertDialog.Builder(this@ClientCardActivity).setView(dialogView).show()
    }

    override fun bonusesWasCleared() {
        Toast.makeText(this, R.string.client_toast_bonuses_was_cleared, Toast.LENGTH_SHORT).show()
    }

    override fun bonusesClearError() {
        Toast.makeText(this, R.string.client_toast_bonuses_clear_error, Toast.LENGTH_SHORT).show()
    }

    override fun answerMessageWasSent() {
        Toast.makeText(this, R.string.answer_toast_was_sent, Toast.LENGTH_SHORT).show()
    }

    override fun answerMessageSentError() {
        Toast.makeText(this, R.string.answer_toast_sent_error, Toast.LENGTH_SHORT).show()
    }

    override fun errorEmptyPushMessage() {
        Toast.makeText(this, R.string.client_toast_empty_push, Toast.LENGTH_SHORT).show()
    }

    override fun pushMessageSent() {
        Toast.makeText(this, R.string.client_toast_push_was_sent, Toast.LENGTH_SHORT).show()
    }

    override fun pushMessageSentError() {
        Toast.makeText(this, R.string.client_toast_push_sent_error, Toast.LENGTH_SHORT).show()
    }

    override fun errorWrongSumToSend() {
        Toast.makeText(this, R.string.client_toast_wrong_sum_to_send, Toast.LENGTH_SHORT).show()
    }

    override fun bonusesWasSent() {
        Toast.makeText(this, R.string.client_toast_bonuses_was_sent, Toast.LENGTH_SHORT).show()
    }

    override fun bonusesSentError() {
        Toast.makeText(this, R.string.client_toast_bonuses_sent_error, Toast.LENGTH_SHORT).show()
    }

    override fun errorNewPasswordIsEmpty() {
        Toast.makeText(this, R.string.client_toast_error_password_empty, Toast.LENGTH_SHORT).show()
    }

    override fun errorNewPasswordsNotEqual() {
        Toast.makeText(this, R.string.client_toast_error_password_not_equal, Toast.LENGTH_SHORT).show()
    }

    override fun clientPasswordChanged() {
        Toast.makeText(this, R.string.client_toast_password_changed, Toast.LENGTH_SHORT).show()
    }

    override fun passwordChangeError() {
        Toast.makeText(this, R.string.client_toast_password_change_error, Toast.LENGTH_SHORT).show()
    }
}
