package ru.devroom.krizalbonus.ui.settings

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.devroom.krizalbonus.model.AppUser
import ru.devroom.krizalbonus.model.network.RetrofitInstance
import ru.devroom.krizalbonus.model.objects.LoadedSettings

@InjectViewState
class SettingsFragmentPresenter:MvpPresenter<ISettingsFragment>() {

    // флаг текущего сеанса отправки/загрузки
    @Volatile var serverBusy = false

    @Volatile private var loadedData: LoadedSettings? = null

    companion object {
        const val SETTINGS_KEY_BITH_DAY_BONUSES  = "bon_for_birthday"
        const val SETTINGS_KEY_BITH_DAY_DAYS     = "bon_for_birthday_days"
        const val SETTINGS_KEY_ORG_PHONE        = "org_phone"
        const val SETTINGS_KEY_ORG_WEB          = "org_web"
        const val SETTINGS_KEY_ORG_EMAIL        = "org_email"
        const val SETTINGS_KEY_ORG_INSTA        = "org_instagramm"
        const val SETTINGS_KEY_ORG_ADDRESS       = "org_address"
        const val SETTINGS_KEY_ORG_ABOUT        = "org_about"
        const val SETTINGS_KEY_ORG_WHATSAPP        = "org_whatsapp"
    }

    fun getPackedSettings():String {
        return Gson().toJson(loadedData, LoadedSettings::class.java)
    }

    fun getLoadedSettings(key:String) = loadedData?.settings?.get(key) ?: ""

    fun isAdmin() = AppUser.get().getEmployeeInfo()?.isAdminRole() ?: false

    // обновление списка
    fun updateSettings() {
        // если сервер занят - выходим
        if(serverBusy) return

        val token = AppUser.get().getEmployeeInfo()?.token
        if(token == null) {
            viewState.errorAuth()
            return
        }

        // показать анимацию загрузки данных
        viewState.showProgress()

        // в новом потоке - запрашиваем данные с сервера
        GlobalScope.launch {
            // ставим флаг активной загрузки
            serverBusy = true
            // запрос на получение данных
            val response = try {
                RetrofitInstance.getService.getSettingsAsync(token).await()
            } catch (e:Exception) {
                null
            }

            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true && response.body() != null) {
                    val answer = response.body()!!
                    loadedData = answer
                    if(answer.levels != null) viewState.insertLevelsList(answer.levels!!)
                    if(answer.settings != null) {
                        viewState.insertGiftSettings()
                        viewState.insertOrgSettings()
                    }
                } else {
                    when (response?.code()) {
                        401 -> {
                            viewState.errorAuth()
                        }
                        else -> {
                            viewState.errorLoading()
                        }
                    }
                }
                // убираем флаг текущей загрузки
                serverBusy = false
                // прячем анимацию загрузки данных
                viewState.hideProgress()
            }
        }
    }
    //
}