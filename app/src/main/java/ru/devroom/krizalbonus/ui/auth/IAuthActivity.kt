package ru.devroom.krizalbonus.ui.auth

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface IAuthActivity:MvpView {
    fun setLastLogin(value:String)

    fun showProgress()
    fun hideProgress()

    @StateStrategyType(SkipStrategy::class)
    fun errorLoginEmpty()

    @StateStrategyType(SkipStrategy::class)
    fun errorPasswordEmpty()

    @StateStrategyType(SkipStrategy::class)
    fun showErrorAuth()

    fun showMainActivity()
}