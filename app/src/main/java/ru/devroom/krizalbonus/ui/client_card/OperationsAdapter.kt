package ru.devroom.krizalbonus.ui.client_card

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.objects.OperationInfo
import kotlin.collections.ArrayList

class OperationsAdapter: RecyclerView.Adapter<OperationsAdapter.ViewHolder>(){
    // список документов
    private var data:List<OperationInfo> = ArrayList()
    //обработчик нажатия на элемент списка
    private var mClickListener : PaymentClickListener? = null

    companion object {
        const val ITEM_RECORD = 1
        const val ITEM_EMPTY_LIST = 0

        const val OPERATION_DEC_BY_PURCHASING = 1 // списание при покупке покупка
        const val OPERATION_INC_CASH_BACK = 2 // кэшбэк . начисление.с.покупки
        const val OPERATION_DEC_FIRE_ALL = 3 // сгорание.бонусов
        const val OPERATION_INC_BIRTHDAY_GIFT = 4 // подарок на др
        const val OPERATION_INC_OTHER = 5 // другое
        const val OPERATION_INC_RECEIVE_GIFT = 6 // получение подарка
        const val OPERATION_DEC_SENDING_GIFT = 7 // подарок другому пользователю
    }

    override fun getItemViewType(position: Int): Int {
        return if(data.isNullOrEmpty()) ITEM_EMPTY_LIST else ITEM_RECORD
    }

    // метод для установки нового списка
    fun setData(newData:List<OperationInfo>?) {
        // запоминаем список
        if(newData.isNullOrEmpty()) {
            this.data = ArrayList()
        } else {
            this.data = newData
        }

        notifyDataSetChanged() // уведомляем менеджер о том, что список поменялся
    }

    // воздаем вью для элемента списка
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                if(viewType == ITEM_RECORD)
                        R.layout.item_operation
                else    R.layout.item_empty_list
                ,parent, false)
        )
    }

    // при присоединиении элемента списка к recyclerView
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // оформляем данные внутри элемента списка
        if(getItemViewType(position) == ITEM_RECORD) {
            val item = data[position]
            holder.bindData(item, mClickListener)
        }
    }

    // возвращаем кол-во элементов списка
    override fun getItemCount(): Int {
        return if(data.isEmpty()) 1 else data.size
    }

    // внутренний класс для хранения и настройки элементов карточки клиента
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private var tvDateTime: TextView? = null
        private var tvOperationName: TextView? = null
        private var tvTotal: TextView? = null

        private var listener: PaymentClickListener? = null

        private lateinit var item: OperationInfo

        init {
            // находим текстовые поля, в которые будем вставлять даные
            tvDateTime = itemView.findViewById(R.id.tvDateTime)
            tvOperationName = itemView.findViewById(R.id.tvOperationName)
            tvTotal = itemView.findViewById(R.id.tvTotal)

            // применяем обработчик нажатия на элемент списка
            itemView.setOnClickListener(this)
        }

        fun bindData(data: OperationInfo, listener: PaymentClickListener?) {

            val ctx = itemView.context

            val total:Int
            val operationName:String

            when(data.type) {
                OPERATION_DEC_BY_PURCHASING -> { // списание при покупке покупка
                    total = -data.summ
                    operationName = ctx.getString(R.string.operation_discount)
                }
                OPERATION_INC_CASH_BACK -> { // кэшбэк . начисление.с.покупки
                    total = data.summ
                    operationName = ctx.getString(R.string.operation_cash_back)
                }
                OPERATION_DEC_FIRE_ALL -> { // сгорание.бонусов
                    total = -data.summ
                    operationName = ctx.getString(R.string.operation_fire)
                }
                OPERATION_INC_BIRTHDAY_GIFT -> { // подарок на др
                    total = data.summ
                    operationName = ctx.getString(R.string.operation_birthday_gift)
                }
                OPERATION_INC_OTHER -> { // другое
                    total = data.summ
                    operationName = ctx.getString(R.string.operation_unknown)
                }
                OPERATION_INC_RECEIVE_GIFT -> { // другое
                    total = data.summ
                    operationName = ctx.getString(R.string.operation_gift)
                }
                OPERATION_DEC_SENDING_GIFT -> { // другое
                    total = -data.summ
                    operationName = ctx.getString(R.string.operation_send_gift)
                }
                else -> {
                    total = data.summ
                    operationName = ctx.getString(R.string.operation_unknown)
                }
            }

            tvDateTime?.text = data.date
            tvOperationName?.text = operationName
            tvTotal?.text = total.toString()

            // запоминаем объект, по которому сделана эта карточка
            this.item = data
            // запоминаем слушатель событий
            this.listener = listener
        }

        // обработчик нажатия на элемент
        override fun onClick(view: View) {
            if (listener != null)
                listener!!.onPaymentItemClick(view, item)
        }
    }

    // метод для применения обработчика нажатий на элемент списка
    fun setClickListener(itemClickListener: PaymentClickListener) {
        this.mClickListener = itemClickListener
    }

    // интерфейс, описывающий метод обработчика нажатий на элемент
    interface PaymentClickListener {
        fun onPaymentItemClick(view:View, item: OperationInfo)
    }
}