package ru.devroom.krizalbonus.ui.reports.bonuses_activity

import android.net.Uri
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.devroom.krizalbonus.model.objects.ReportOperationInfo

interface IReportBonusesActivity:MvpView{
    @StateStrategyType(SingleStateStrategy::class)
    fun insertReportList(data:ArrayList<ReportOperationInfo>)

    @StateStrategyType(SkipStrategy::class)
    fun errorLoading()

    @StateStrategyType(SkipStrategy::class)
    fun errorAuth()

    fun showProgress()

    fun hideProgress()

    fun setDates(dates:String)

    @StateStrategyType(SkipStrategy::class)
    fun openFile(file: Uri)

}