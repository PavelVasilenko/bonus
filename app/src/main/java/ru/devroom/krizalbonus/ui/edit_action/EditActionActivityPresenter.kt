package ru.devroom.krizalbonus.ui.edit_action

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.devroom.krizalbonus.model.AppUser
import ru.devroom.krizalbonus.model.network.RetrofitInstance
import ru.devroom.krizalbonus.model.objects.ActionInfo
import java.util.*

@InjectViewState
class EditActionActivityPresenter:MvpPresenter<IEditActionActivity>() {

    private var editedAction: ActionInfo? = null
    private var selectedStartDate: Long? = null
    private var selectedEndDate: Long? = null

    private val calendar = Calendar.getInstance()
    private var requestedDate = 0
    
    // флаг текущего сеанса отправки/загрузки
    @Volatile
    var serverBusy = false

    fun getEditedItem() = editedAction

    fun lastSelectedStartDate():Long? {
        requestedDate = 1
        return selectedStartDate
    }
    fun lastSelectedEndDate():Long? {
        requestedDate = 2
        return selectedEndDate
    }

    fun dateSelected(value:Long) {
        when(requestedDate) {
            1 -> {
                selectedStartDate = value / 1000
                viewState.insertStartDate(unixTimeToString(selectedStartDate!!))
            }
            2 -> {
                selectedEndDate = value / 1000
                viewState.insertEndDate(unixTimeToString(selectedEndDate!!))
            }
        }
        requestedDate = 0
    }

    private fun unixTimeToString(dateTime:Long):String {
        calendar.timeInMillis = 1000 * dateTime
        val year = calendar.get(Calendar.YEAR)
        val month = 1 + calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        var date = if(day < 10) "0$day" else "$day"
        date += "." + if (month < 10) "0$month" else "$month"
        date += ".$year"
        return date
    }


    fun insertEditedItem(serializedItem: String) {
        editedAction = Gson().fromJson(serializedItem, ActionInfo::class.java)

        if (editedAction != null) {
            viewState.insertValues(
                editedAction!!.caption,
                editedAction!!.text,
                editedAction!!.active == 1
            )

            selectedStartDate = editedAction!!.dateTime
            selectedEndDate = editedAction!!.endTime
            viewState.insertStartDate(unixTimeToString(editedAction!!.dateTime))
            viewState.insertEndDate(unixTimeToString(editedAction!!.endTime))
        }
    }

    fun deleteItem() {
        if(editedAction == null || editedAction!!.serverId < 0) return
        // если сервер занят - выходим
        if(serverBusy) return
        // авторизация пользователя
        val token = AppUser.get().getEmployeeInfo()?.token
        if(token == null) {
            viewState.errorAuth()
            return
        }
        // показать анимацию
        viewState.showProgress()
        // в новом потоке - запрашиваем данные с сервера
        GlobalScope.launch {
            // ставим флаг активной загрузки
            serverBusy = true
            val response = try {
                RetrofitInstance.getService.deleteActionAsync(editedAction!!.serverId, token).await()
            } catch (e: Exception) {
                null
            }
            // в основном потоке - показываем пользователю информацию
            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true) {
                    viewState.finishWithOk()
                } else {
                    viewState.errorDeletingItem()
                }
                // убираем флаг текущей загрузки
                serverBusy = false
                // прячем анимацию загрузки данных
                viewState.hideProgress()
            }
        }
    }

    fun saveItem(caption:String, text:String, enabled:Boolean) {
        // если сервер занят - выходим
        if (serverBusy) return

        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }

        // проверки данных
        if (caption.isEmpty()) {
            viewState.errorEmptyCaption()
            return
        }
        if (text.isEmpty()) {
            viewState.errorEmptyText()
            return
        }
        if (selectedStartDate == null) {
            viewState.errorEmptyStartDate()
            return
        }
        if (selectedEndDate == null) {
            viewState.errorEmptyEndDate()
            return
        }

        // показать анимацию загрузки данных
        viewState.showProgress()
        // в новом потоке - запрашиваем данные с сервера
        GlobalScope.launch {
            // ставим флаг активной загрузки
            serverBusy = true

            val response = try {
                // это новая организация
                if(editedAction == null || editedAction!!.serverId < 0)
                    RetrofitInstance.getService.postNewActionAsync(
                        caption,
                        text,
                        selectedStartDate!!,
                        selectedEndDate!!,
                        if(enabled) 1 else 0,
                        token).await()
                else // это обновление данных
                    RetrofitInstance.getService.putActionAsync(
                        editedAction?.serverId ?: -1,
                        caption,
                        text,
                        selectedStartDate!!,
                        selectedEndDate!!,
                        if(enabled) 1 else 0,
                        token).await()
            } catch (e: Exception) {
                null
            }

            // в основном потоке - показываем пользователю информацию
            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true) {
                    viewState.finishWithOk()
                } else {
                    when(response?.code()) {
                        401 -> viewState.errorAuth()
                        else -> viewState.errorSavingItem()
                    }
                }
                // убираем флаг текущей загрузки
                serverBusy = false
                // прячем анимацию загрузки данных
                viewState.hideProgress()
            }
        }
    }
}