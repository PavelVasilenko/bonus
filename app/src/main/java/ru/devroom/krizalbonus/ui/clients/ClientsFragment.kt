package ru.devroom.krizalbonus.ui.clients

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.fragment_clients.*
import ru.devroom.krizalbonus.IFragmentCallback
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.Codes
import ru.devroom.krizalbonus.model.objects.ClientInfo
import ru.devroom.krizalbonus.ui.client_card.ClientCardActivity



class ClientsFragment : MvpAppCompatFragment(), IClientsFragment, ClientsAdapter.ItemClickListener {

    @InjectPresenter
    lateinit var mPresenter:ClientsFragmentPresenter

    // адаптер списка
    private var adapter: ClientsAdapter? = null

    companion object {
        const val RESULT_FOR_EDIT = 75
    }

    private var listener:IFragmentCallback? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_clients, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        etSearchString.setupClearButtonWithAction()

        listener?.inflateMenu(R.menu.clients_menu)

        adapter = ClientsAdapter()
        adapter!!.setClickListener(this)

        rvFoundedClients?.layoutManager = LinearLayoutManager(context)
        rvFoundedClients.adapter = adapter

        rvFoundedClients.addOnScrollListener(object:RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val manager = rvFoundedClients?.layoutManager ?: return
                val visibleItemCount = manager.childCount
                val totalItemCount = manager.itemCount
                val firstVisibleItems = (manager as LinearLayoutManager).findFirstVisibleItemPosition()

                if ((visibleItemCount + firstVisibleItems + 5) >= totalItemCount) {
                    mPresenter.updateClients()
                }

            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
        })

        ibStartSearch.setOnClickListener {
            mPresenter.clearList()
            mPresenter.updateClients(etSearchString.text.toString(), 0)
        }

        if(mPresenter.haveCache())
            mPresenter.initCache()
        else
            mPresenter.updateClients(etSearchString.text.toString())
        hideProgress()
    }


    override fun insertClientsList(data: ArrayList<ClientInfo>?, replace:Boolean) {
        if(replace)
            adapter?.setData(data)
        else
            adapter?.appendData(data)
    }

    override fun errorLoading() {
        Toast.makeText(context, R.string.error_while_loading, Toast.LENGTH_SHORT).show()
    }

    override fun errorAuth() {
        Toast.makeText(context, R.string.error_while_auth, Toast.LENGTH_SHORT).show()
        listener?.onAuthError()
    }

    override fun showProgress() {
        progressBar4.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar4.visibility = View.GONE
    }

    override fun onItemClick(view: View, item: ClientInfo) {
        val intent = Intent(context, ClientCardActivity::class.java)
        intent.putExtra(ClientCardActivity.PARAM_CLIENT_CODE, item.cardNumber)
        intent.putExtra(ClientCardActivity.PARAM_CLIENT_OPENMODE, ClientCardActivity.MODE_VIEW)
        startActivityForResult(intent, RESULT_FOR_EDIT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode) {
            RESULT_FOR_EDIT -> {
                if(resultCode == Codes.OK){
                    mPresenter.clearList()
                    mPresenter.updateClients(etSearchString.text.toString(), 0)
                }
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is IFragmentCallback) {
            listener = context
        }
    }

    fun updateList() {
        mPresenter.clearList()
        mPresenter.updateClients(etSearchString.text.toString(), 0)
    }

    private fun EditText.setupClearButtonWithAction() {

        addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                val clearIcon = if (editable?.isNotEmpty() == true) R.drawable.round_clear_black_24 else 0
                setCompoundDrawablesWithIntrinsicBounds(0, 0, clearIcon, 0)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
        })

        setOnTouchListener(View.OnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= (this.right - this.compoundPaddingRight)) {
                    this.setText("")
                    mPresenter.clearList()
                    mPresenter.updateClients("", 0)
                    return@OnTouchListener true
                }
            }
            return@OnTouchListener false
        })
    }
}