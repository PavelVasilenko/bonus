package ru.devroom.krizalbonus.ui.payments

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.devroom.krizalbonus.model.objects.PaymentInfo

interface IPaymentFragment:MvpView {

    @StateStrategyType(SingleStateStrategy::class)
    fun insertPaymentsList(data:ArrayList<PaymentInfo>)

    @StateStrategyType(SkipStrategy::class)
    fun errorLoading()

    @StateStrategyType(SkipStrategy::class)
    fun errorAuth()

    fun showProgress()

    fun setDates(dates:String)
    fun hideProgress()

    @StateStrategyType(SkipStrategy::class)
    fun openClientCard(id:Int, openMode:String)

    @StateStrategyType(SkipStrategy::class)
    fun errorClientCode()


}