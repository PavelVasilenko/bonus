package ru.devroom.krizalbonus.ui.purchase_info

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.devroom.krizalbonus.model.AppUser
import ru.devroom.krizalbonus.model.network.RetrofitInstance
import ru.devroom.krizalbonus.model.objects.PaymentInfo
import ru.devroom.krizalbonus.ui.client_card.ClientCardActivity

@InjectViewState
class PurchaseInfoActivityPresenter:MvpPresenter<IPurchaseInfoActivity>() {

    var receivedPaymentInfo:PaymentInfo? = null
    @Volatile private var serverBusyDeleting = false
    @Volatile private var postPushToClient = false
    @Volatile private var sendNoteProgress = false

    fun isAdmin() = AppUser.get().getEmployeeInfo()?.isAdminRole() ?: false

    fun setPackedItem(packedItem:String?) {

        if(packedItem == null) return

        try {
            receivedPaymentInfo = Gson().fromJson(packedItem, PaymentInfo::class.java) ?: return
            viewState.insertInfo(receivedPaymentInfo!!)
        } catch (e:Exception) {

        }
    }


    fun sendAnswerForComment(paymentInfo: PaymentInfo, typedMessage:String) {
        if (postPushToClient) return
        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }



//        if(typedMessage.isEmpty()) {
//            viewState.errorEmptyPushMessage()
//            return
//        }

        viewState.showProgress()
        GlobalScope.launch {
            postPushToClient = true
            val response = try {
                RetrofitInstance.getService.postAnswerForCommentAsync(
                    paymentInfo.serverId,
                    typedMessage,
                    token).await()
            } catch (e: Exception) {
                null
            }
            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true) {
                    viewState.answerMessageWasSent()
                    receivedPaymentInfo?.adminComment = typedMessage
                    viewState.updateAnswer(typedMessage)
                } else {
                    when (response?.code() ?: 0) {
                        401 -> {
                            viewState.errorAuth()
                        }
                        else -> {
                            viewState.answerMessageSentError()
                        }
                    }
                }
                postPushToClient = false
                viewState.hideProgress()
            }
        }
    }

    fun sendNewNote(paymentInfo: PaymentInfo, typedMessage:String) {
        if (sendNoteProgress) return
        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }
//        if(typedMessage.isEmpty()) {
//            viewState.errorEmptyPushMessage()
//            return
//        }

        val messageToSend = if(isAdmin()) {
            typedMessage
        } else {
            if(typedMessage.isNotEmpty())
                paymentInfo.note + "\n" + typedMessage
            else
                paymentInfo.note
        }

        viewState.showProgress()
        GlobalScope.launch {
            sendNoteProgress = true
            val response = try {
                RetrofitInstance.getService.postNewNoteAsync(
                    paymentInfo.serverId,
                    messageToSend,
                    token).await()
            } catch (e: Exception) {
                null
            }
            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true) {
                    viewState.noteMessageWasSent()
                    receivedPaymentInfo?.note = messageToSend
                    viewState.updateNote(messageToSend)
                } else {
                    when (response?.code() ?: 0) {
                        401 -> {
                            viewState.errorAuth()
                        }
                        else -> {
                            viewState.noteSentError()
                        }
                    }
                }
                sendNoteProgress = false
                viewState.hideProgress()
            }
        }
    }

    fun openClientCard(scannedCode: String?, mode:String = ClientCardActivity.MODE_PURCHASE) {
        if (scannedCode.isNullOrEmpty())
            viewState.errorClientCode()
        else
            try {
                viewState.openClientCard(scannedCode.toInt(), mode)
            } catch (e: Exception) {
                viewState.errorClientCode()
            }
    }

    fun showWriteAnswerToClientClicked(item:PaymentInfo) {
        viewState.showAnswerToClientDialog(item)
    }

    fun showEditNoteDialogClicked(item:PaymentInfo) {
        viewState.showEditNoteDialog(item, isAdmin())
    }

    fun deletePurchase(documentServerId:Int) {
        // если сервер занят - выходим
        if (serverBusyDeleting) return

        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }

        // показать анимацию загрузки данных
        viewState.showProgress()

        // в новом потоке - запрашиваем данные с сервера
        GlobalScope.launch {
            // ставим флаг активной загрузки
            serverBusyDeleting = true
            // запрос на получение данных
            val response = try {
                RetrofitInstance.getService.deletePaymentsListAsync(documentServerId, token).await()
            } catch (e: Exception) {
                null
            }

            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true) {
                    viewState.finishWithUpdatePurchasesList()
                } else {
                    when (response?.code()) {
                        401 -> {
                            viewState.errorAuth()
                        }
                        else -> {
                            viewState.errorDeleting()
                        }
                    }
                }
                // убираем флаг текущей загрузки
                serverBusyDeleting = false
                // прячем анимацию загрузки данных
                viewState.hideProgress()
            }
        }
    }

}