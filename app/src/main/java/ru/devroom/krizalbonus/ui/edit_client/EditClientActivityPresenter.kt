package ru.devroom.krizalbonus.ui.edit_client

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import ru.devroom.krizalbonus.model.AppUser
import ru.devroom.krizalbonus.model.network.RetrofitInstance
import ru.devroom.krizalbonus.model.objects.ClientInfo
import java.math.BigInteger
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.SimpleDateFormat
import java.util.*

@InjectViewState
class EditClientActivityPresenter:MvpPresenter<IEditClientActivity>() {

    // потоки
    private val job = SupervisorJob()
    private val scope = CoroutineScope(Dispatchers.Default + job)
    private val uiScope = CoroutineScope(Dispatchers.Main)

    private var selectedDate: String = ""
    private var gender: Int = 0

    private var editedClientInfo:ClientInfo? = null

    private val dataBaseDataFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
    private val humanDataFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH)

    @Volatile var updating = false

    companion object {
        private const val SAULT = "f93F_D."
        // метод для шифрования пароля с помощью SHA-512
        fun encryptPassword(input: String): String {
            try {
                val password = SAULT + input
                val md = MessageDigest.getInstance("SHA-256")
                val messageDigest = md.digest(password.toByteArray())
                val no = BigInteger(1, messageDigest)
                var hashtext = no.toString(16)
                while (hashtext.length < 32) {
                    hashtext = "0$hashtext"
                }
                return hashtext.toUpperCase()
            } catch (e: NoSuchAlgorithmException) {
                throw RuntimeException(e)
            }
        }

    }

    fun setEditedClientData(data:String?) {
        if(data == null) return

        editedClientInfo = Gson().fromJson(data, ClientInfo::class.java) ?: return
        gender = editedClientInfo!!.gender
//        selectedDate = editedClientInfo!!.birthday ?: ""

//        val tmpDate = dataBaseDataFormat.parse(selectedDate)
//        val formatted = if(tmpDate != null) humanDataFormat.format(tmpDate) else ""

        viewState.setEditedClientInfo(
            editedClientInfo!!
        )

    }

    fun lastSelectedDate(): String? {
        return if (selectedDate.isEmpty() || selectedDate == "0000-00-00")
            null
        else
            selectedDate
    }

    fun setSelectedDate(year: Int, month: Int, dayOfMonth: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, dayOfMonth)
        selectedDate = dataBaseDataFormat.format(calendar.time)
        viewState.insertNewDate(formatDate(selectedDate))
    }

    fun setSelectedGender(code:Int) {
        gender = code
        viewState.setGender(code)
    }

    private fun formatDate(dataBaseDate: String): String {
        val tmp = dataBaseDataFormat.parse(dataBaseDate)
        return if (tmp == null) "" else humanDataFormat.format(tmp)
    }

    fun saveItem(telephone:String, fio:String, email:String, password1:String, password2:String) {
// если сервер занят - выходим
        if (updating) return

        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }

        // проверки данных
        if (telephone.length != 18) {
            viewState.errorEmptyTelephone()
            return
        }

        val passwordToSend:String
        if(editedClientInfo == null) {
            if (password1.isEmpty()) {
                viewState.errorEmptyPassword()
                return
            }
            if (password1 != password2) {
                viewState.errorPasswordsNotEqual()
                return
            }
            passwordToSend = encryptPassword(password1)
        } else {
            passwordToSend = ""
        }

        // показать анимацию загрузки данных
        viewState.showProgress()
        // в новом потоке - запрашиваем данные с сервера
        scope.launch {
            // ставим флаг активной загрузки
            updating = true

            val response = try {

                RetrofitInstance.getService.postNewClientAsync(
                    telephone,
                    passwordToSend,
                    "", // messagingToken
                    fio,
                    email,
                    gender,
                    selectedDate,
                    token).await()
            } catch (e: Exception) {
                null
            }

            // в основном потоке - показываем пользователю информацию
            uiScope.launch {
                if (response?.isSuccessful == true && response.body() != null) {
                    if(editedClientInfo == null)
                        viewState.finishRegistered(response.body()!!.cardNumber)
                    else
                        viewState.finishChanged()
                } else {
                    when(response?.code()) {
                        401 -> viewState.errorAuth()
                        409 -> viewState.errorOccupiedTelephone()
                        else -> viewState.errorSavingItem()
                    }
                }
                updating = false
                viewState.hideProgress()
            }
        }

    }
}