package ru.devroom.krizalbonus.ui.main_activity

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.devroom.krizalbonus.model.AppUser
import ru.devroom.krizalbonus.model.network.RetrofitInstance

@InjectViewState
class MainActivityPresenter:MvpPresenter<IMainActivity>() {

    private var currentMenu:Int? = null
    @Volatile private var postPushToClient = false

    fun stopAutoReauth(){
        AppUser.get().destroy()
    }

    fun currentMenu() = this.currentMenu

    fun setCurrentMenuType(value:Int?) {
        this.currentMenu = value
    }

    fun logout(){
        AppUser.get().clearUserAuth()
        viewState.showAuthActivity()
    }

    fun sendPushToAll(typedMessage:String) {
        if (postPushToClient) return
        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }
        if(typedMessage.isEmpty()) {
            viewState.errorEmptyPushMessage()
            return
        }

        GlobalScope.launch {
            postPushToClient = true
            val response = try {
                RetrofitInstance.getService.postPushToAllClientsAsync(
                    typedMessage,
                    token).await()
            } catch (e: Exception) {
                null
            }
            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true) {
                    viewState.pushMessageWasSent()
                } else {
                    when (response?.code()) {
                        401 -> {
                            viewState.errorAuth()
                        }
                        else -> {
                            viewState.pushMessageSentError()
                        }
                    }

                }
                postPushToClient = false
            }
        }
    }


    fun isAdmin() = AppUser.get().getEmployeeInfo()?.isAdminRole() ?: false
}