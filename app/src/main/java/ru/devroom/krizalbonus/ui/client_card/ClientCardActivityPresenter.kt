package ru.devroom.krizalbonus.ui.client_card

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.devroom.krizalbonus.model.AppUser
import ru.devroom.krizalbonus.model.network.RetrofitInstance
import ru.devroom.krizalbonus.model.objects.ClientInfo
import ru.devroom.krizalbonus.model.objects.PaymentInfo
import ru.devroom.krizalbonus.ui.auth.AuthActivityPresenter
import java.text.SimpleDateFormat
import java.util.*

@InjectViewState
class ClientCardActivityPresenter : MvpPresenter<IClientCardActivity>() {

    // флаг текущего сеанса отправки/загрузки
    @Volatile var serverBusy = false
    @Volatile var operationsLoading = false
    @Volatile var inPayProgress = false

    private var clientCardCode: Int? = null
    private var showPurchase: Boolean? = null

    @Volatile private var clientInfo: ClientInfo? = null
    @Volatile private var serverBusyDeleting = false
    @Volatile private var postPushToClient = false
    @Volatile private var postBonusesToClient = false
    @Volatile private var postClearClientBalance = false

    private var answerCode: Int = 0

    private var typedGoodCost: Int = 0
    private var useBonuses: Boolean = true
    private var useCustomBonusSum: Boolean = false
    private var selectedBonusSum: Int = 0

    private var calculatedTotal = 0

    private var completedShowed:Boolean = false

    private var oldDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    private var newDateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())

    private var acceptCodeToDelete:String = ""


    fun useBonuses() = useBonuses
    fun useCustomBonusSum() = useCustomBonusSum
//    fun selectedBonusSum() = selectedBonusSum

    fun generateAcceptToDeleteDialog() {
        acceptCodeToDelete = ""
        for(i:Int in 1..3)
            acceptCodeToDelete += (1 + (9 * Math.random()).toInt()).toString()
        viewState.showAcceptToDeleteDialog(acceptCodeToDelete)
    }

    fun showWriteAnswerToClient(item: PaymentInfo) {
        viewState.showAnswerToClientDialog(item)
    }

    fun enteredDeleteCode(typedCode:String) {
        if(typedCode != acceptCodeToDelete) {
            viewState.errorWrongCommitCode()
            return
        }
        if (clientCardCode == null) {
            return
        }
        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }



        GlobalScope.launch {
            val response = try {
                RetrofitInstance.getService.deleteClientAsync(
                    clientCardCode!!,
                    token
                ).await()
            } catch (e: Exception) {
                null
            }

            withContext(Dispatchers.Main) {
                viewState.hideProgress()
                if (response?.isSuccessful == true) {
                    viewState.clientDeleted()
                } else {
                    answerCode = response?.code() ?: 0
                    when (response?.code()) {
                        401 -> {
                            viewState.errorAuth()
                        }
                        else -> {
                            viewState.errorWhileDeleting()
                        }
                    }
                }
            }
        }



    }

    fun setUseBonuses(value:Boolean) {
        useBonuses = value
    }

    fun setUseCustomBonusSum(value:Boolean) {
        useCustomBonusSum = value
    }

    fun setGoodsCost(typedCost:String) {
        typedGoodCost = try {
            typedCost.toInt()
        } catch (e:Exception) {
            0
        }
    }
    fun setBonusesSum(typedCost:String) {
        selectedBonusSum = try {
            typedCost.toInt()
        } catch (e:Exception) {
            0
        }
    }

    fun sendPushToClient(typedMessage:String) {
        if (postPushToClient) return
        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }
        if(clientCardCode == null) return
        if(typedMessage.isEmpty()) {
            viewState.errorEmptyPushMessage()
            return
        }

        viewState.showProgress()
        GlobalScope.launch {
            postPushToClient = true
            val response = try {
                RetrofitInstance.getService.postPushToClientAsync(
                    clientCardCode!!,
                    typedMessage,
                    token).await()
            } catch (e: Exception) {
                null
            }
            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true) {
                    viewState.pushMessageSent()
                } else {
                    answerCode = response?.code() ?: 0
                    when (response?.code()) {
                        401 -> {
                            viewState.errorAuth()
                        }
                        404 -> {
                            viewState.clientNotFoundToast()
                        }
                        406 -> {
                            viewState.clientNotAuthYet()
                        }
                        else -> {
                            viewState.pushMessageSentError()
                        }
                    }

                }
                postPushToClient = false
                viewState.hideProgress()
            }
        }
    }

    fun sendAnswerForComment(paymentInfo: PaymentInfo, typedMessage:String) {
        if (postPushToClient) return
        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }
        if(typedMessage.isEmpty()) {
            viewState.errorEmptyPushMessage()
            return
        }

        viewState.showProgress()
        GlobalScope.launch {
            postPushToClient = true
            val response = try {
                RetrofitInstance.getService.postAnswerForCommentAsync(
                    paymentInfo.serverId,
                    typedMessage,
                    token).await()
            } catch (e: Exception) {
                null
            }
            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true) {
                    viewState.answerMessageWasSent()
                    updateClientInfo(true)
                } else {
                    when (response?.code() ?: 0) {
                        401 -> {
                            viewState.errorAuth()
                        }
                        else -> {
                            viewState.answerMessageSentError()
                        }
                    }
                }
                postPushToClient = false
                viewState.hideProgress()
            }
        }
    }

    fun sendAddBonusesToClient(typedSum:String) {
        if (postBonusesToClient) return
        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }
        if(clientCardCode == null) return
        val sum = try {
            typedSum.toInt()
        } catch (e:Exception) {
            0
        }
        if(sum == 0) {
            viewState.errorWrongSumToSend()
            return
        }

        viewState.showProgress()
        GlobalScope.launch {
            postBonusesToClient = true
            val response = try {
                RetrofitInstance.getService.postBonusesToClientAsync(
                    clientCardCode!!,
                    sum,
                    token).await()
            } catch (e: Exception) {
                null
            }
            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true) {
                    updateClientInfo(true)
                    viewState.bonusesWasSent()
                } else {
                    viewState.bonusesSentError()
                }
                postBonusesToClient = false
                viewState.hideProgress()
            }
        }
    }


    fun sendNewClientPassword(typedPassword1:String, typedPassword2:String) {
        if (postBonusesToClient) return
        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }
        if(clientCardCode == null) return

        if(typedPassword1.isEmpty()) {
            viewState.errorNewPasswordIsEmpty()
            return
        }
        if(typedPassword1 != typedPassword2) {
            viewState.errorNewPasswordsNotEqual()
            return
        }

        val cryptedPassword = AuthActivityPresenter.encryptPassword(typedPassword1)

        viewState.showProgress()
        GlobalScope.launch {
            postBonusesToClient = true
            val response = try {
                RetrofitInstance.getService.postNewClientPasswordAsync(
                    clientCardCode!!,
                    cryptedPassword,
                    token).await()
            } catch (e: Exception) {
                null
            }
            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true) {
                    viewState.clientPasswordChanged()
                } else {
                    viewState.passwordChangeError()
                }
                postBonusesToClient = false
                viewState.hideProgress()
            }
        }
    }

    fun sendEraseClientsBonuses() {
        if (postClearClientBalance) return
        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }
        if(clientCardCode == null) return

        viewState.showProgress()
        GlobalScope.launch {
            postClearClientBalance = true
            val response = try {
                RetrofitInstance.getService.postClearClientBalanceAsync(
                    clientCardCode!!,
                    token).await()
            } catch (e: Exception) {
                null
            }
            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true) {
                    updateClientInfo(true)
                    viewState.bonusesWasCleared()
                } else {
                    viewState.bonusesClearError()
                }
                postClearClientBalance = false
                viewState.hideProgress()
            }
        }
    }

    private fun clearBonusSwitches() {
        useCustomBonusSum = false
        useBonuses = true
        selectedBonusSum = 0
        calculate()
    }

    fun onCommitClicked(typedNote:String) {
        if (inPayProgress) return
        if (typedGoodCost == 0) {
            viewState.errorEmptyTotalCost()
            return
        }
        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }
        viewState.showProgress()
        // в новом потоке - запрашиваем данные с сервера
        GlobalScope.launch {
            // ставим флаг активной загрузки
            inPayProgress = true
            // запрос на получение данных
            val response = try {
                val useBonuses = if (useBonuses) 1 else 2
                RetrofitInstance.getService.postPurchaseAsync(
                    clientCardCode!!,
                    typedGoodCost,
                    useBonuses,
                    selectedBonusSum,
                    typedNote,
                    token
                ).await()
            } catch (e: Exception) {
                null
            }

            withContext(Dispatchers.Main) {
                viewState.hideProgress()
                if (response?.isSuccessful == true) {
                    completedShowed = true
                    viewState.showPayComplete()
                    // очистка расчетов
                    clearBonusSwitches()
                    viewState.clearGoodCost()
                    // обновление данных клиента
                    updateClientInfo(true)
                } else {
                    answerCode = response?.code() ?: 0
                    when (response?.code()) {
                        401 -> {
                            viewState.errorAuth()
                        }
                        423 -> {
                            viewState.clientLocked()
                        }
                        else -> {
                            viewState.errorWhilePay()
                        }
                    }
                }
                // убираем флаг текущей загрузки
                inPayProgress = false
                // прячем анимацию загрузки данных
                viewState.hideProgress()
            }
        }
    }

    fun deletePurchase(documentServerId:Int) {
        // если сервер занят - выходим
        if (serverBusyDeleting) return

        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }

        // показать анимацию загрузки данных
        viewState.showProgress()

        // в новом потоке - запрашиваем данные с сервера
        GlobalScope.launch {
            // ставим флаг активной загрузки
            serverBusyDeleting = true
            // запрос на получение данных
            val response = try {
                RetrofitInstance.getService.deletePaymentsListAsync(documentServerId, token).await()
            } catch (e: Exception) {
                null
            }

            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true) {
                    updateClientInfo(true)
                } else {
                    when (response?.code()) {
                        401 -> {
                            viewState.errorAuth()
                        }
                        else -> {
                            viewState.errorDeleting()
                        }
                    }
                }
                // убираем флаг текущей загрузки
                serverBusyDeleting = false
                // прячем анимацию загрузки данных
                viewState.hideProgress()
            }
        }
    }

//    private fun calculate() {
//        calculate(typedGoodCost, useBonuses)
//    }

    fun getClientProfile():ClientInfo? = clientInfo

    fun calculate() {
        if (clientInfo == null) return
        val cashBack:Int

        if (clientInfo!!.levelInfo == null) {
            useBonuses = false
            useCustomBonusSum = false
            selectedBonusSum = 0
            calculatedTotal = typedGoodCost
            cashBack = 0
        } else {
            val maxDiscountValue = typedGoodCost * clientInfo!!.levelInfo!!.maxDiscount / 100
            if(useBonuses) {
                if(useCustomBonusSum) {
                    // прользователь указывает конкретную сумму бонусов
                    if(selectedBonusSum > maxDiscountValue) selectedBonusSum = maxDiscountValue
                    calculatedTotal = typedGoodCost - selectedBonusSum
                    cashBack = calculatedTotal * clientInfo!!.levelInfo!!.cashBack / 100
                } else {
                    selectedBonusSum = kotlin.math.min(maxDiscountValue, clientInfo!!.balance)

                    calculatedTotal = typedGoodCost - selectedBonusSum
                    cashBack = calculatedTotal * clientInfo!!.levelInfo!!.cashBack / 100
                }
            } else {
                calculatedTotal = typedGoodCost
                selectedBonusSum = 0
                cashBack = calculatedTotal * clientInfo!!.levelInfo!!.cashBack / 100
            }
        }
        viewState.updateCalculation(selectedBonusSum, cashBack, calculatedTotal)
    }

    fun initWithClientViews(code: Int?, mode: String) {
        if (code == null) return
        this.clientCardCode = code

        if (showPurchase == null)
            showPurchase = when (mode) {
                ClientCardActivity.MODE_VIEW -> {
                    false
                }
                ClientCardActivity.MODE_PURCHASE -> {
                    true
                }
                else -> false
            }

        if (showPurchase!!)
            viewState.hidePurchaseLayout()
        else
            viewState.showPurchaseLayout(false, completedShowed)

//        updateClientOperations()
        updateClientInfo()
    }

    fun showPurchase() {
        completedShowed = false
        showPurchase = true
        viewState.showPurchaseLayout(true, completedShowed)
        viewState.hideFabPurchase()
    }

//    fun getAnswerCode() = answerCode

    fun scrollBackToPay() {
        completedShowed = false
        viewState.scrollToPay()
    }

    fun getClientCardId() = clientCardCode

    fun isAdmin() = AppUser.get().getEmployeeInfo()?.isAdminRole() ?: false

    fun updateClientInfo(afterPurchase:Boolean = false) {

        if (clientInfo != null && !afterPurchase ) {
            GlobalScope.launch(Dispatchers.Main) {
                viewState.showScroll()
                viewState.insertClientInfo(clientInfo!!)
            }
            if (showPurchase == true) {
                viewState.showPurchaseLayout(false, completedShowed)
                viewState.hideFabPurchase()
            } else {
                viewState.hidePurchaseLayout()
                viewState.showFabPurchase()
            }
            viewState.hideFabReload()
            viewState.hideError()
            viewState.hideProgress()

            calculate()
            return
        }

        // если сервер занят - выходим
        if (serverBusy) return

        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            if(!afterPurchase) viewState.errorAuth()
            return
        }

        if (clientCardCode == null) {
            return
        }


        // показать анимацию загрузки данных
        viewState.showProgress()
        if(!afterPurchase) {
            viewState.hideScroll()
            viewState.hideFabPurchase()
            viewState.hideFabReload()
            viewState.hideError()
        }

        // в новом потоке - запрашиваем данные с сервера
        GlobalScope.launch {
            // ставим флаг активной загрузки
            serverBusy = true
            // запрос на получение данных
            val response = try {
                RetrofitInstance.getService.getClientInfoAsync(clientCardCode!!, token).await()
            } catch (e: Exception) {
                null
            }

            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true && response.body() != null) {
                    clientInfo = response.body()!!

                    try{
                        if(!clientInfo!!.birthday.isNullOrEmpty()) {
                            val date = oldDateFormat.parse(clientInfo!!.birthday!!)
                            if (date != null) clientInfo!!.birthday = newDateFormat.format(date)
                        }
                        if(!clientInfo!!.balanceExpired.isNullOrEmpty()) {
                            val date2 = oldDateFormat.parse(clientInfo!!.balanceExpired!!)
                            if (date2 != null) clientInfo!!.balanceExpired = newDateFormat.format(date2)
                        }
                    } catch (e: java.lang.Exception) {}

                    if(!afterPurchase) {
                        calculate()
                        viewState.showScroll()
                    }

                    viewState.insertClientInfo(clientInfo!!)

                    if(!afterPurchase) {
                        if (showPurchase == true) {
                            viewState.showPurchaseLayout(false, completedShowed)
                            viewState.hideFabPurchase()
                        } else {
                            viewState.hidePurchaseLayout()
                            viewState.showFabPurchase()
                        }
                        viewState.hideFabReload()
                    }
                } else {
                    if(!afterPurchase) {
                        answerCode = response?.code() ?: 0
                        when (response?.code()) {
                            401 -> {
                                viewState.errorAuth()
                                viewState.hideFabReload()
                            }
                            404 -> {
                                viewState.clientNotFound()
                                viewState.hideFabReload()
                            }
                            else -> {
                                viewState.errorLoading()
                                viewState.showFabReload()
                            }
                        }
                    }
                }
                // убираем флаг текущей загрузки
                serverBusy = false
                // прячем анимацию загрузки данных
                viewState.hideProgress()
            }
        }
    }

    private fun updateClientOperations() {
        // если сервер занят - выходим
        if (operationsLoading) return

        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }

        if (clientCardCode == null) {
            return
        }

        // в новом потоке - запрашиваем данные с сервера
        GlobalScope.launch {
            // ставим флаг активной загрузки
            operationsLoading = true
            // запрос на получение данных
            val response = try {
                RetrofitInstance.getService.getOperationsListAsync(clientCardCode!!, token).await()
            } catch (e: Exception) {
                null
            }

            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true && response.body() != null) {
                    viewState.insertClientOperations(response.body()!!)
                } else {
                    answerCode = response?.code() ?: 0
                    when (response?.code()) {
                        401 -> {
                            viewState.errorAuth()
                        }
                        404 -> {
                            viewState.clientNotFound()
                        }
                        else -> {
                            viewState.errorLoading()
                        }
                    }
                }
                // убираем флаг текущей загрузки
                operationsLoading = false
                // прячем анимацию загрузки данных
            }
        }
    }
}