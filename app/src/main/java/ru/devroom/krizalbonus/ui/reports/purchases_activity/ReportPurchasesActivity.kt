package ru.devroom.krizalbonus.ui.reports.purchases_activity

import android.Manifest
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.activity_report_purchases.*
import kotlinx.android.synthetic.main.activity_report_purchases.progressBar4
import kotlinx.android.synthetic.main.dialog_select_dates_range.view.*
import kotlinx.android.synthetic.main.report_bottom_purchases.*
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.objects.PaymentInfo
import java.util.*
import kotlin.collections.ArrayList
import com.yanzhenjie.permission.AndPermission
import android.Manifest.permission
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.view.Menu
import android.webkit.MimeTypeMap
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.content.FileProvider
import com.google.gson.Gson
import org.apache.poi.ss.formula.functions.T
import com.yanzhenjie.permission.runtime.Permission
import ru.devroom.krizalbonus.model.Codes
import ru.devroom.krizalbonus.ui.purchase_info.PurchaseInfoActivity


class ReportPurchasesActivity : MvpAppCompatActivity(), IReportPurchasesActivity,
    ReportPurchasesAdapter.ItemClickListener {

    @InjectPresenter
    lateinit var mPresenter: ReportPurchasesActivityPresenter

    // адаптер списка
    private var adapter: ReportPurchasesAdapter? = null

    private var selectedDateStart: Calendar? = null
    private var selectedDateEnd: Calendar? = null
    private var daterangeSelect: AlertDialog? = null

    companion object {
        const val REQUEST_CODE_PERMISSION = 20
        const val RESULT_FOR_UPDATE_LIST = 77
        const val PARAM_CLIENT_CARD_CODE = "card_id"
        const val PARAM_EMPLOYEE_ID = "employee_id"
        const val PARAM_EMPLOYEE_FAMILY = "employee_family"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_purchases)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        tvSelectedEmployee.visibility = View.GONE
        tvSelectedCardId.visibility = View.GONE
        if (intent.hasExtra(PARAM_CLIENT_CARD_CODE)) {
            val cardId = intent.getIntExtra(PARAM_CLIENT_CARD_CODE, -1)
            if (cardId > 0) {
                tvSelectedCardId.text = getString(R.string.report_purchases_label2, cardId)
                tvSelectedCardId.visibility = View.VISIBLE
                mPresenter.setCardId(cardId)
            } else {
                tvSelectedCardId.visibility = View.GONE
            }
            tvSelectedEmployee.visibility = View.GONE
        } else {
            tvSelectedCardId.visibility = View.GONE

            if (intent.hasExtra(PARAM_EMPLOYEE_ID)) {
                val employeeId = intent.getIntExtra(PARAM_EMPLOYEE_ID, -1)
                var employeeFio = intent.getStringExtra(PARAM_EMPLOYEE_FAMILY)
                if (employeeId > 0) {
                    if (employeeFio.isNullOrEmpty()) employeeFio = employeeId.toString()
                    tvSelectedEmployee.text =
                        getString(R.string.report_purchases_label3, employeeFio)
                    tvSelectedEmployee.visibility = View.VISIBLE
                    mPresenter.setEmployeeId(employeeId)
                } else {
                    tvSelectedEmployee.visibility = View.GONE
                }
                tvSelectedEmployee.visibility = View.VISIBLE
            } else {
                tvSelectedCardId.visibility = View.GONE
            }

        }

        if(mPresenter.isAdmin()) {
            ibExport.visibility = View.VISIBLE
        } else {
            ibExport.visibility = View.GONE
        }


        adapter = ReportPurchasesAdapter()
        adapter!!.setClickListener(this)
        rvPurchasesList?.layoutManager = LinearLayoutManager(this)
        rvPurchasesList.adapter = adapter

        ibSelectDocsDate.setOnClickListener(onDateSelectClickListener)

        val bottomSheetBehavior = BottomSheetBehavior.from(report_bottom_sheet)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED

        ibExport.setOnClickListener { onExportClicked() }

        clTotalsCaption.setOnClickListener {
            val bottomSheetBehavior = BottomSheetBehavior.from(report_bottom_sheet)
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        mPresenter.setTodayDateRange()
        hideProgress()
    }


//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.client_card_menu, menu)
//        return true
//    }

    private fun onExportClicked() {
        AndPermission.with(this)
            .runtime()
            .permission(Permission.Group.STORAGE)
            .onGranted { _ ->
                mPresenter.export(this@ReportPurchasesActivity)
            }
            .onDenied { _ ->
                AlertDialog
                    .Builder(this)
                    .setMessage(R.string.write_storage_denied)
                    .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int -> }
                    .show()
            }
            .start()
    }

    override fun insertReportTotals(
        count: String,
        goodCosts: String,
        discountSum: String,
        totalSum: String
    ) {
        tvReportTotal.text = count
        tvReportGoodsSum.text = goodCosts
        tvDiscountSum.text = discountSum
        tvTotalTotal.text = totalSum

    }

    override fun onItemClick(view: View, item: PaymentInfo) {
        val packedItem = Gson().toJson(item, PaymentInfo::class.java)
        val intent = Intent(this@ReportPurchasesActivity, PurchaseInfoActivity::class.java)
        intent.putExtra(PurchaseInfoActivity.SERIALIZED_ITEM, packedItem)
        startActivityForResult(intent, RESULT_FOR_UPDATE_LIST)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            RESULT_FOR_UPDATE_LIST -> {
                if (resultCode == Codes.OK) {
                    mPresenter.updatePaymentsList()
                }
            }
        }
    }

    override fun openFile(file: Uri) {
        val install = Intent(Intent.ACTION_VIEW)
        install.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
        install.setDataAndType(file, getMimeType(file.path ?: ""))
        install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(install)
    }

    private fun getMimeType(fileUri: String): String {
        var mimeType: String? = null
        val extension = MimeTypeMap.getFileExtensionFromUrl(fileUri)
        if (extension != null) {
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
        }
        return mimeType ?: "*/*"
    }

    private val onDateSelectClickListener = View.OnClickListener {

        if (daterangeSelect == null) {

            val view = layoutInflater.inflate(R.layout.dialog_select_dates_range, null)
            view.calendar.setCalendarListener(object : DateRangeCalendarView.CalendarListener {
                override fun onDateRangeSelected(startDate: Calendar?, endDate: Calendar?) {
                    selectedDateStart = startDate
                    selectedDateEnd = endDate
                }

                override fun onFirstDateSelected(startDate: Calendar?) {
                    selectedDateStart = startDate
                    selectedDateEnd = startDate
                }
            })
            view.calendar.setWeekOffset(1)

            daterangeSelect = AlertDialog.Builder(this)
                .setView(view)
                .setNeutralButton(R.string.payment_all_dates){ _: DialogInterface, _: Int ->
                    mPresenter.setFullDateRange()
                }
                .setNegativeButton(R.string.payment_today) { _: DialogInterface, _: Int ->
                    mPresenter.setTodayDateRange()
                }
                .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                    if (selectedDateStart != null) {
                        mPresenter.setDateRange(
                            selectedDateStart!!.timeInMillis,
                            selectedDateEnd!!.timeInMillis
                        )
                    }
                }.create()
        }

        val calendar: DateRangeCalendarView? =
            daterangeSelect?.findViewById<DateRangeCalendarView>(R.id.calendar)
        if (calendar != null) {
            if (!mPresenter.isAllTimeSelected()) {
                val startSelectionDate = mPresenter.getStartDateRange()
                val endSelectionDate = mPresenter.getEndDateRange()
                calendar.setSelectedDateRange(startSelectionDate, endSelectionDate)
            } else {
                calendar.setSelectedDateRange(null, null)
            }
        }

        daterangeSelect!!.show()
    }

    override fun onSupportNavigateUp(): Boolean {

        onBackPressed()
        return true
    }


    override fun errorAuth() {
        Toast.makeText(this, R.string.error_while_auth, Toast.LENGTH_SHORT).show()
    }

    override fun insertReportList(data: ArrayList<PaymentInfo>) {
        adapter?.setData(data)
    }

    override fun errorLoading() {
        Toast.makeText(this, R.string.error_while_loading, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressBar4.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar4.visibility = View.GONE
    }

    override fun setDates(dates: String) {
        tvDates.text = dates
    }
}
