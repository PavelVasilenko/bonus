package ru.devroom.krizalbonus.ui.settings

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_settings.*
import kotlinx.android.synthetic.main.include_org_info.*
import ru.devroom.krizalbonus.IFragmentCallback
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.Codes
import ru.devroom.krizalbonus.model.objects.LevelInfo
import ru.devroom.krizalbonus.ui.edit_gifts.EditGiftsActivity
import ru.devroom.krizalbonus.ui.edit_level.EditLevelActivity
import ru.devroom.krizalbonus.ui.edit_org_settings.EditOrgSettingsActivity
import ru.devroom.krizalbonus.ui.settings.SettingsFragmentPresenter.Companion.SETTINGS_KEY_BITH_DAY_BONUSES
import ru.devroom.krizalbonus.ui.settings.SettingsFragmentPresenter.Companion.SETTINGS_KEY_BITH_DAY_DAYS
import ru.devroom.krizalbonus.ui.settings.SettingsFragmentPresenter.Companion.SETTINGS_KEY_ORG_ABOUT
import ru.devroom.krizalbonus.ui.settings.SettingsFragmentPresenter.Companion.SETTINGS_KEY_ORG_ADDRESS
import ru.devroom.krizalbonus.ui.settings.SettingsFragmentPresenter.Companion.SETTINGS_KEY_ORG_EMAIL
import ru.devroom.krizalbonus.ui.settings.SettingsFragmentPresenter.Companion.SETTINGS_KEY_ORG_INSTA
import ru.devroom.krizalbonus.ui.settings.SettingsFragmentPresenter.Companion.SETTINGS_KEY_ORG_PHONE
import ru.devroom.krizalbonus.ui.settings.SettingsFragmentPresenter.Companion.SETTINGS_KEY_ORG_WEB
import ru.devroom.krizalbonus.ui.settings.SettingsFragmentPresenter.Companion.SETTINGS_KEY_ORG_WHATSAPP


class SettingsFragment : MvpAppCompatFragment(), ISettingsFragment, LevelsAdapter.ItemClickListener {

    @InjectPresenter
    lateinit var mPresenter:SettingsFragmentPresenter

    // адаптер списка
    private var adapter: LevelsAdapter? = null

    private var listener: IFragmentCallback? = null

    companion object {
        const val RESULT_FOR_EDIT_LEVEL = 70
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_settings, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listener?.inflateMenu(null)

        adapter = LevelsAdapter()
        adapter!!.setClickListener(this)

        rvLevelsList?.layoutManager = LinearLayoutManager(context)
        rvLevelsList.adapter = adapter
        rvLevelsList.isNestedScrollingEnabled = false

        if(mPresenter.isAdmin()) {
            ibAddLevel.setOnClickListener {
                val intent = Intent(context, EditLevelActivity::class.java)
                startActivityForResult(intent, RESULT_FOR_EDIT_LEVEL)
            }
        } else {
            ibAddLevel.visibility = View.GONE
        }

        if(mPresenter.isAdmin()) {
            clGiftSettings.setOnClickListener {
                val intent = Intent(context, EditGiftsActivity::class.java)
                intent.putExtra(
                    EditGiftsActivity.VALUE_SUM,
                    mPresenter.getLoadedSettings(SETTINGS_KEY_BITH_DAY_BONUSES)
                )
                intent.putExtra(
                    EditGiftsActivity.VALUE_DAYS,
                    mPresenter.getLoadedSettings(SETTINGS_KEY_BITH_DAY_DAYS)
                )
                startActivityForResult(intent, RESULT_FOR_EDIT_LEVEL)
            }


            clOrgSettings.setOnClickListener {
                val intent = Intent(context, EditOrgSettingsActivity::class.java)
                intent.putExtra(
                    EditOrgSettingsActivity.PARAM_PACKED_SETTINGS,
                    mPresenter.getPackedSettings()
                )
                startActivityForResult(intent, RESULT_FOR_EDIT_LEVEL)
            }
        }

        mPresenter.updateSettings()
        hideProgress()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is IFragmentCallback) {
            listener = context
        }
    }

    override fun onItemClick(view: View, item: LevelInfo) {
        if (mPresenter.isAdmin()) {
            val serialized = Gson().toJson(item, LevelInfo::class.java)

            val intent = Intent(context, EditLevelActivity::class.java)
            intent.putExtra(EditLevelActivity.SERIALIZED_ITEM, serialized)
            startActivityForResult(intent, RESULT_FOR_EDIT_LEVEL)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            RESULT_FOR_EDIT_LEVEL -> {
                if(resultCode == Codes.OK) {
                    mPresenter.updateSettings()
                }
            }

        }
    }

    override fun insertOrgSettings() {
        tvPhoneValue.text = mPresenter.getLoadedSettings(SETTINGS_KEY_ORG_PHONE)
        tvSiteValue.text = mPresenter.getLoadedSettings(SETTINGS_KEY_ORG_WEB)
        tvEmailValue.text = mPresenter.getLoadedSettings(SETTINGS_KEY_ORG_EMAIL)
        tvInstagrammValue.text = mPresenter.getLoadedSettings(SETTINGS_KEY_ORG_INSTA)
        tvAdressValue.text = mPresenter.getLoadedSettings(SETTINGS_KEY_ORG_ADDRESS)
        tvAboutValue.text = mPresenter.getLoadedSettings(SETTINGS_KEY_ORG_ABOUT)
        tvWhatsappValue.text = mPresenter.getLoadedSettings(SETTINGS_KEY_ORG_WHATSAPP)
    }

    override fun insertGiftSettings() {
        tvBirthdayBonuses.text = mPresenter.getLoadedSettings(SETTINGS_KEY_BITH_DAY_BONUSES)
        tvBonusesDays.text = mPresenter.getLoadedSettings(SETTINGS_KEY_BITH_DAY_DAYS)
    }

    override fun insertLevelsList(data: ArrayList<LevelInfo>) {
        adapter?.setData(data)
    }

    override fun errorLoading() {
        Toast.makeText(context, R.string.error_while_loading, Toast.LENGTH_SHORT).show()
    }

    override fun errorAuth() {
        Toast.makeText(context, R.string.error_while_auth, Toast.LENGTH_SHORT).show()
        listener?.onAuthError()
    }

    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar.visibility = View.GONE
    }
}