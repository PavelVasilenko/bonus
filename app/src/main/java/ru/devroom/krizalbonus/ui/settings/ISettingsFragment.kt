package ru.devroom.krizalbonus.ui.settings

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.devroom.krizalbonus.model.objects.LevelInfo

interface ISettingsFragment:MvpView {

    @StateStrategyType(SingleStateStrategy::class)
    fun insertLevelsList(data:ArrayList<LevelInfo>)

    @StateStrategyType(SingleStateStrategy::class)
    fun insertGiftSettings()

    @StateStrategyType(SingleStateStrategy::class)
    fun insertOrgSettings()

    @StateStrategyType(SkipStrategy::class)
    fun errorLoading()

    @StateStrategyType(SkipStrategy::class)
    fun errorAuth()

    fun showProgress()

    fun hideProgress()

}