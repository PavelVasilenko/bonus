package ru.devroom.krizalbonus.ui.settings

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.objects.LevelInfo
import kotlin.collections.ArrayList

class LevelsAdapter: RecyclerView.Adapter<LevelsAdapter.ViewHolder>(){
    // список документов
    private var data:List<LevelInfo> = ArrayList()
    //обработчик нажатия на элемент списка
    private lateinit var mClickListener : ItemClickListener

    // метод для установки нового списка
    fun setData(newData:List<LevelInfo>?) {
        // запоминаем список
        if(newData.isNullOrEmpty()) {
            this.data = ArrayList()
        } else {
            this.data = newData
        }
        notifyDataSetChanged() // уведомляем менеджер о том, что список поменялся
    }

    // воздаем вью для элемента списка
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_level, parent, false)
        )
    }

    // при присоединиении элемента списка к recyclerView
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // оформляем данные внутри элемента списка
        holder.bindData(data[position], mClickListener)
    }

    // возвращаем кол-во элементов списка
    override fun getItemCount(): Int {
        return data.size
    }

    // внутренний класс для хранения и настройки элементов карточки клиента
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private var tvPurchasesSumValue: TextView? = null
        private var tvCashBackValue: TextView? = null
        private var tvMaxDiscountValue: TextView? = null
        private var tvAddTimeValue: TextView? = null
        private var tvLiveTimeValue: TextView? = null
        private var listener: ItemClickListener? = null

        private lateinit var item: LevelInfo

        init {
            // находим текстовые поля, в которые будем вставлять даные
            tvPurchasesSumValue = itemView.findViewById(R.id.tvPurchasesSumValue)
            tvCashBackValue = itemView.findViewById(R.id.tvCashBackValue)
            tvMaxDiscountValue = itemView.findViewById(R.id.tvMaxDiscountValue)
            tvAddTimeValue = itemView.findViewById(R.id.tvAddTimeValue)
            tvLiveTimeValue = itemView.findViewById(R.id.tvLiveTimeValue)

            // применяем обработчик нажатия на элемент списка
            itemView.setOnClickListener(this)
        }

        fun bindData(data: LevelInfo, listener: ItemClickListener) {

            tvPurchasesSumValue?.text = data.totalPurchases.toString()
            tvCashBackValue?.text = data.cashBack.toString()
            tvMaxDiscountValue?.text = data.maxDiscount.toString()
            tvAddTimeValue?.text = data.delayDays.toString()
            tvLiveTimeValue?.text = data.liveTime.toString()

            // запоминаем объект, по которому сделана эта карточка
            this.item = data
            // запоминаем слушатель событий
            this.listener = listener
        }

        // обработчик нажатия на элемент
        override fun onClick(view: View) {
            if (listener != null)
                listener!!.onItemClick(view, item)
        }
    }

    // метод для применения обработчика нажатий на элемент списка
    fun setClickListener(itemClickListener: ItemClickListener) {
        this.mClickListener = itemClickListener
    }

    // интерфейс, описывающий метод обработчика нажатий на элемент
    interface ItemClickListener {
        fun onItemClick(view:View, item: LevelInfo)
    }
}