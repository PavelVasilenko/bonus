package ru.devroom.krizalbonus.ui.edit_org_settings

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.activity_edit_org_settings.*
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.Codes

class EditOrgSettingsActivity : MvpAppCompatActivity(), IEditOrgSettingsActivity {

    @InjectPresenter
    lateinit var mPresenter:EditOrgSettingsActivityPresenter

    companion object {
        const val PARAM_PACKED_SETTINGS = "settings"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_org_settings)


        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val settings = intent.getStringExtra(PARAM_PACKED_SETTINGS)
        if(settings.isNullOrEmpty()) finish()

        fabSave.setOnClickListener { mPresenter.saveItem(
            etOrgPhone.text.toString(),
            etOrgWhatsapp.text.toString(),
            etOrgAddress.text.toString(),
            etOrgSite.text.toString(),
            etOrgEmail.text.toString(),
            etOrgInsta.text.toString(),
            etOrgInfo.text.toString()
        ) }

        mPresenter.insertCurrentOptions(settings!!)

    }

    override fun insertOrgParameters(
        phone:String,
        whatsapp:String,
        address:String,
        site:String,
        email:String,
        instagram:String,
        about:String
    ) {
        etOrgPhone.setText(phone)
        etOrgWhatsapp.setText(whatsapp)
        etOrgAddress.setText(address)
        etOrgSite.setText(site)
        etOrgEmail.setText(email)
        etOrgInsta.setText(instagram)
        etOrgInfo.setText(about)
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
    override fun errorSavingItem() {
        Toast.makeText(this, R.string.error_while_saving, Toast.LENGTH_SHORT).show()
    }

    override fun finishWithOk() {
        setResult(Codes.OK)
        finish()
    }

    override fun errorAuth() {
        Toast.makeText(this, R.string.error_while_auth, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressBar2.visibility = View.VISIBLE
        fabSave.hide()
    }

    override fun hideProgress() {
        progressBar2.visibility = View.GONE
        fabSave.show()
    }
}
