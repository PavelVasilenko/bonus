package ru.devroom.krizalbonus.ui.edit_org_settings

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface IEditOrgSettingsActivity:MvpView {

    fun insertOrgParameters(
        phone:String,
        whatsapp:String,
        address:String,
        site:String,
        email:String,
        instagram:String,
        about:String
    )


    @StateStrategyType(SkipStrategy::class)
    fun errorSavingItem()

    @StateStrategyType(SkipStrategy::class)
    fun finishWithOk()

    @StateStrategyType(SkipStrategy::class)
    fun errorAuth()

    fun showProgress()
    fun hideProgress()
}