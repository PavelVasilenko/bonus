package ru.devroom.krizalbonus.ui.payments

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.devroom.krizalbonus.model.AppUser
import ru.devroom.krizalbonus.model.network.RetrofitInstance
import ru.devroom.krizalbonus.model.objects.PaymentInfo
import ru.devroom.krizalbonus.ui.client_card.ClientCardActivity
import java.text.SimpleDateFormat
import java.util.*

@InjectViewState
class PaymentFragmentPresenter : MvpPresenter<IPaymentFragment>() {

    //private var selectedDate: String

    private var dateFormatServer = SimpleDateFormat("yyyy-MM-dd", Locale.US)
    private var dateFormatApp = SimpleDateFormat("dd.MM.yyyy", Locale.US)

    private var startDate:Long = 0
    private var endDate:Long = 0

    // флаг текущего сеанса отправки/загрузки
    @Volatile var serverBusyReceiving = false

    fun setTodayDateRange() {
        val curDate = Date().time
        setDateRange(curDate, curDate)
    }

    fun getStartDateRange():Calendar? {
        return if(startDate > 0) {
            val startCalendar = GregorianCalendar(Locale.US)
            startCalendar.timeInMillis = startDate
            startCalendar
        } else {
            null
        }
    }


    fun openClientCard(scannedCode: String?, mode:String = ClientCardActivity.MODE_PURCHASE) {
        if (scannedCode.isNullOrEmpty())
            viewState.errorClientCode()
        else
            try {
                viewState.openClientCard(scannedCode.toInt(), mode)
            } catch (e: Exception) {
                viewState.errorClientCode()
            }
    }

    fun getEndDateRange():Calendar? {
        return if(endDate > 0) {
            val endCalendar = GregorianCalendar(Locale.US)
            endCalendar.timeInMillis = endDate
            endCalendar
        } else {
            null
        }
    }

    fun setDateRange(start:Long, end:Long) {
        startDate = start
        endDate = end

        var datesRange = dateFormatApp.format(startDate)
        if(startDate != endDate) datesRange += " - " + dateFormatApp.format(endDate)

        viewState.setDates(datesRange)

        updatePaymentsList()
    }

    private fun unixTimeToString(dateTime: Long): String {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = 1000 * dateTime

        val year = calendar.get(Calendar.YEAR)
        val month = 1 + calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        var date = "$year-"
        date += if (month < 10) "0$month-" else "$month-"
        date += if (day < 10) "0$day" else "$day"

        return date
    }

    fun isAdmin() = AppUser.get().getEmployeeInfo()?.isAdminRole() ?: false

    fun updatePaymentsList() {
        // если сервер занят - выходим
        if (serverBusyReceiving) return

        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }

        // показать анимацию загрузки данных
        viewState.showProgress()

        // в новом потоке - запрашиваем данные с сервера
        GlobalScope.launch {
            // ставим флаг активной загрузки
            serverBusyReceiving = true

            val date1 = dateFormatServer.format(Date(startDate))
            val date2 = dateFormatServer.format(Date(endDate))

            // запрос на получение данных
            val response = try {
                RetrofitInstance.getService.getPaymentsListAsync(date1, date2, token).await()
            } catch (e: Exception) {
                null
            }

            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true && response.body() != null) {
                    viewState.insertPaymentsList(response.body()!!)
                } else {
                    when (response?.code()) {
                        401 -> {
                            viewState.errorAuth()
                        }
                        else -> {
                            viewState.errorLoading()
                        }
                    }
                }
                // убираем флаг текущей загрузки
                serverBusyReceiving = false
                // прячем анимацию загрузки данных
                viewState.hideProgress()
            }
        }
    }
}