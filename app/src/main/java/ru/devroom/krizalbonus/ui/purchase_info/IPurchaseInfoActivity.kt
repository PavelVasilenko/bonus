package ru.devroom.krizalbonus.ui.purchase_info

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.devroom.krizalbonus.model.objects.PaymentInfo

interface IPurchaseInfoActivity:MvpView {

    fun insertInfo(item:PaymentInfo)
    @StateStrategyType(SkipStrategy::class)

    fun errorAuth()

    fun showProgress()

    fun hideProgress()

    @StateStrategyType(SkipStrategy::class)
    fun errorDeleting()

    fun showAnswerToClientDialog(payment:PaymentInfo)
    fun showEditNoteDialog(payment:PaymentInfo, isAdmin:Boolean)

    @StateStrategyType(SkipStrategy::class)
    fun errorClientCode()

    @StateStrategyType(SkipStrategy::class)
    fun openClientCard(id:Int, openMode:String)

    @StateStrategyType(SkipStrategy::class)
    fun answerMessageSentError()

    @StateStrategyType(SkipStrategy::class)
    fun noteSentError()

    @StateStrategyType(SkipStrategy::class)
    fun answerMessageWasSent()

    @StateStrategyType(SkipStrategy::class)
    fun noteMessageWasSent()

    @StateStrategyType(SkipStrategy::class)
    fun errorEmptyPushMessage()

    fun finishWithUpdatePurchasesList()

    fun updateNote(noteText:String)
    fun updateAnswer(answerText:String)

}