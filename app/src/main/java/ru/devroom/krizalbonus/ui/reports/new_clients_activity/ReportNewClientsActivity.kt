package ru.devroom.krizalbonus.ui.reports.new_clients_activity

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.yanzhenjie.permission.AndPermission
import com.yanzhenjie.permission.runtime.Permission
import kotlinx.android.synthetic.main.activity_report_new_clients.*
import kotlinx.android.synthetic.main.dialog_select_dates_range.view.*
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.objects.ClientInfo
import ru.devroom.krizalbonus.ui.client_card.ClientCardActivity
import ru.devroom.krizalbonus.ui.clients.ClientsAdapter
import ru.devroom.krizalbonus.ui.clients.ClientsFragment
import java.util.*

class ReportNewClientsActivity : MvpAppCompatActivity(), IReportNewClientsActivity, ClientsAdapter.ItemClickListener {

    @InjectPresenter
    lateinit var mPresenter:ReportNewClientsActivityPresenter

    // адаптер списка
    private var adapter: ClientsAdapter? = null

    private var selectedDateStart: Calendar? = null
    private var selectedDateEnd: Calendar? = null
    private var daterangeSelect: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_new_clients)


        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)


        adapter = ClientsAdapter()
        adapter?.setClickListener(this)

        rvNewClientsList?.layoutManager = LinearLayoutManager(this)
        rvNewClientsList.adapter = adapter

        ibSelectDocsDate.setOnClickListener(onDateSelectClickListener)
        ibExport.setOnClickListener { onExportClicked() }

        mPresenter.setTodayDateRange()
        hideProgress()
    }


    override fun openFile(file: Uri) {
        val install = Intent(Intent.ACTION_VIEW)
        install.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
        install.setDataAndType(file, getMimeType(file.path ?: ""))
        install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(install)
    }

    private fun getMimeType(fileUri: String): String {
        var mimeType: String? = null
        val extension = MimeTypeMap.getFileExtensionFromUrl(fileUri)
        if (extension != null) {
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
        }
        return mimeType ?: "*/*"
    }

    private fun onExportClicked() {
        AndPermission.with(this)
            .runtime()
            .permission(Permission.Group.STORAGE)
            .onGranted { _ ->
                mPresenter.export(this@ReportNewClientsActivity)
            }
            .onDenied { _ ->
                AlertDialog
                    .Builder(this)
                    .setMessage(R.string.write_storage_denied)
                    .setPositiveButton(android.R.string.ok){ _: DialogInterface, _: Int -> }
                    .show()
            }
            .start()
    }

    private val onDateSelectClickListener = View.OnClickListener {

        if (daterangeSelect == null) {

            val view = layoutInflater.inflate(R.layout.dialog_select_dates_range, null)
            view.calendar.setCalendarListener(object : DateRangeCalendarView.CalendarListener {
                override fun onDateRangeSelected(startDate: Calendar?, endDate: Calendar?) {
                    selectedDateStart = startDate
                    selectedDateEnd = endDate
                }

                override fun onFirstDateSelected(startDate: Calendar?) {
                    selectedDateStart = startDate
                    selectedDateEnd = startDate
                }
            })
            view.calendar.setWeekOffset(1)

            daterangeSelect = android.app.AlertDialog.Builder(this)
                .setView(view)
                .setNeutralButton(R.string.payment_all_dates){ _: DialogInterface, _: Int ->
                    mPresenter.setFullDateRange()
                }
                .setNegativeButton(R.string.payment_today) { _: DialogInterface, _: Int ->
                    mPresenter.setTodayDateRange()
                }
                .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                    if (selectedDateStart != null) {
                        mPresenter.setDateRange(
                            selectedDateStart!!.timeInMillis,
                            selectedDateEnd!!.timeInMillis
                        )
                    }
                }.create()
        }


        val calendar: DateRangeCalendarView? =
            daterangeSelect?.findViewById<DateRangeCalendarView>(R.id.calendar)
        if (calendar != null) {
            if (!mPresenter.isAllTimeSelected()) {
                val startSelectionDate = mPresenter.getStartDateRange()
                val endSelectionDate = mPresenter.getEndDateRange()
                calendar.setSelectedDateRange(startSelectionDate, endSelectionDate)
            } else {
                calendar.setSelectedDateRange(null, null)
            }
        }

        daterangeSelect!!.show()
    }

    override fun onItemClick(view: View, item: ClientInfo) {
        val intent = Intent(this, ClientCardActivity::class.java)
        intent.putExtra(ClientCardActivity.PARAM_CLIENT_CODE, item.cardNumber)
        intent.putExtra(ClientCardActivity.PARAM_CLIENT_OPENMODE, ClientCardActivity.MODE_VIEW)
        startActivityForResult(intent, ClientsFragment.RESULT_FOR_EDIT)
    }

    override fun onSupportNavigateUp(): Boolean {

        onBackPressed()
        return true
    }


    override fun errorAuth() {
        Toast.makeText(this, R.string.error_while_auth, Toast.LENGTH_SHORT).show()
    }

    override fun insertClientsList(data: ArrayList<ClientInfo>) {
        adapter?.setData(data)
    }

    override fun errorLoading() {
        Toast.makeText(this, R.string.error_while_loading, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressBar4.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar4.visibility = View.GONE
    }

    override fun setDates(dates: String) {
        tvDates.text = dates
    }
}
