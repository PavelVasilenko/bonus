package ru.devroom.krizalbonus.ui.edit_client

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.DatePicker
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.redmadrobot.inputmask.MaskedTextChangedListener
import kotlinx.android.synthetic.main.activity_new_client.*
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.Codes
import ru.devroom.krizalbonus.model.objects.ClientInfo
import java.text.SimpleDateFormat
import java.util.*

class EditClientActivity : MvpAppCompatActivity(), IEditClientActivity,
    DatePickerDialog.OnDateSetListener, View.OnClickListener {

    @InjectPresenter
    lateinit var mPresenter: EditClientActivityPresenter

    companion object {
        const val PARAM_EDITED_USER = "edited_user"
    }

    private lateinit var gendersList:Array<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_client)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        fabSave.setOnClickListener (this)

        if(intent.hasExtra(PARAM_EDITED_USER)) {
            // редактировнаие данных
            mPresenter.setEditedClientData(
                intent.getStringExtra(PARAM_EDITED_USER)
            )
        } else {
            // новый клиента
        }

        val listener = MaskedTextChangedListener("+7 ([000]) [000]-[00]-[00]", etTelephone)

        etTelephone.addTextChangedListener(listener)
        etTelephone.onFocusChangeListener = listener

        etBirthDay.setOnClickListener (this)

        etGender.setOnClickListener (this)

        gendersList = arrayOf(
            "",
            getString(R.string.personal_gender_male),
            getString(R.string.personal_gender_female)
        )

        hideProgress()
    }

    override fun onClick(p0: View?) {
        when(p0?.id) {
            R.id.etGender -> {
                showGenderDialog()
            }

            R.id.fabSave -> {
                mPresenter.saveItem(
                    etTelephone.text.toString(),
                    etFamily.text.toString(),
                    etEmail.text.toString(),
                    etPassword.text.toString(),
                    etPasswordReply.text.toString()
                )
            }

            R.id.etBirthDay -> {
                // подготавливаем дату для календаря
                val calendar = Calendar.getInstance()
                val df = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
                val lastSelectedDate = mPresenter.lastSelectedDate()
                if (lastSelectedDate != null) {
                    var result = df.parse(lastSelectedDate)
                    if (result == null) result = Date()
                    calendar.timeInMillis = result.time
                }

                val day = calendar.get(Calendar.DAY_OF_MONTH)
                val month = calendar.get(Calendar.MONTH)
                val year = calendar.get(Calendar.YEAR)
                // показываем календарь
                DatePickerDialog(
                    this@EditClientActivity,
                    this@EditClientActivity,
                    year,
                    month,
                    day
                ).show()
            }
        }
    }

    override fun onDateSet(p0: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        mPresenter.setSelectedDate(year, month, dayOfMonth)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun showGenderDialog() {
        AlertDialog.Builder(this).setItems(gendersList) { dialogInterface, i ->
            mPresenter.setSelectedGender(i)
        }.show()
    }

    override fun setEditedClientInfo(data: ClientInfo) {
        setGender(data.gender)
        insertNewDate(data.birthday ?: "")
        tvPasswordLabel2.visibility = View.GONE
        tvPasswordLabel.visibility = View.GONE
        etPassword.visibility = View.GONE
        etPasswordReply.visibility = View.GONE

        etTelephone.isEnabled = false
        etTelephone.setText(data.telephone)
        etFamily.setText(data.fio)
        etEmail.setText(data.email)
    }

    override fun insertNewDate(value: String) {
        etBirthDay.setText(value)
    }

    override fun setGender(value: Int) {
        if(value in 0..2)
            etGender.setText(gendersList[value])
        else etGender.setText("")
    }

    override fun errorEmptyTelephone() {
        etTelephone.error = getString(R.string.error_empty_field)
    }

    override fun errorEmptyPassword() {
        etPassword.error = getString(R.string.error_empty_field)
    }

    override fun errorPasswordsNotEqual() {
        etPasswordReply.error = getString(R.string.error_passwords_not_equal)
    }

    override fun errorOccupiedTelephone() {
        Toast.makeText(this, R.string.error_telephone_occupied, Toast.LENGTH_SHORT).show()
    }

    override fun errorSavingItem() {
        Toast.makeText(this, R.string.error_while_saving, Toast.LENGTH_SHORT).show()
    }

    override fun finishRegistered(clientCardNumber:Int) {
        AlertDialog.Builder(this).setMessage(
            getString(R.string.client_add_dialog_number, clientCardNumber)
        ).setPositiveButton(android.R.string.ok){ _: DialogInterface, _: Int ->
            setResult(Codes.OK)
            finish()
        }.show()
    }

    override fun finishChanged() {
        AlertDialog.Builder(this).setMessage(
            getString(R.string.client_add_changed)
        ).setPositiveButton(android.R.string.ok){ _: DialogInterface, _: Int ->
            setResult(Codes.OK)
            finish()
        }.show()
    }

    override fun errorAuth() {
        Toast.makeText(this, R.string.error_while_auth, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        fabSave.hide()
        progressBar2.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        fabSave.show()
        progressBar2.visibility = View.GONE
    }
}
