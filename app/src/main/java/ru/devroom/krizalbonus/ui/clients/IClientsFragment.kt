package ru.devroom.krizalbonus.ui.clients

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.devroom.krizalbonus.model.objects.ClientInfo

interface IClientsFragment:MvpView {

    @StateStrategyType(SingleStateStrategy::class)
    fun insertClientsList(data:ArrayList<ClientInfo>?, replace:Boolean)

    @StateStrategyType(SkipStrategy::class)
    fun errorLoading()

    @StateStrategyType(SkipStrategy::class)
    fun errorAuth()

    fun showProgress()

    fun hideProgress()
}