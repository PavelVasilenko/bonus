package ru.devroom.krizalbonus.ui.reports.bonuses_activity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.objects.ReportOperationInfo
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ReportBonusesGroupAdapter: RecyclerView.Adapter<ReportBonusesGroupAdapter.ViewHolder>(){
    // список документов
    private var data:List<ReportOperationInfo> = ArrayList()
    //обработчик нажатия на элемент списка
    private var mClickListener : ItemClickListener? = null

    companion object {
        
        const val OPERATION_MOMENTAL = 0    // моментальное начисление
        const val OPERATION_REMOVAL_PURCHASE = 1 // списывание при покупке
        const val OPERATION_ACCRUAL_PURCHASE = 2 // начисление за покупку
        const val OPERATION_FIRE = 3			// сгорание бонусов
        const val OPERATION_BIRTHDAY_GIFT = 4 	// подарок на др
        const val OPERATION_ACCRUAL_OTHER = 5 	// иное.начисление
        const val OPERATION_GIFT_FROM_CLIENT = 6 	// подарок от клиента
        const val OPERATION_GIFT_TO_CLIENT = 7 	// отправка подарка
        const val OPERATION_BONUSES_TOTAL = 70 	// бонусов на счетах
        const val OPERATION_BONUSES_IN_PROMISES = 71 	// бонусов в обещаниях

    }
    
    // метод для установки нового списка
    fun setData(newData:List<ReportOperationInfo>?) {
        // запоминаем список
        if(newData.isNullOrEmpty()) {
            this.data = ArrayList()
        } else {
            this.data = newData
        }
        notifyDataSetChanged() // уведомляем менеджер о том, что список поменялся
    }

    // воздаем вью для элемента списка
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                        R.layout.item_report_bonus
                ,parent, false)
        )
    }

    // при присоединиении элемента списка к recyclerView
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]

        val ctx = holder.itemView.context
        val textType = when(item.type) {
            OPERATION_REMOVAL_PURCHASE -> // списывание при покупке
                ctx.getString(R.string.report_operation_1_rem_purch)

            OPERATION_ACCRUAL_PURCHASE  -> // начисление за покупку
                ctx.getString(R.string.report_operation_2_acc_purch)

            OPERATION_FIRE  ->			// сгорание бонусов
                ctx.getString(R.string.report_operation_3_fire)

            OPERATION_BIRTHDAY_GIFT  -> 	// подарок на др
                ctx.getString(R.string.report_operation_4_bd_gift)

            OPERATION_ACCRUAL_OTHER  -> 	// иное.начисление
                ctx.getString(R.string.report_operation_5_acc_other)

            OPERATION_GIFT_FROM_CLIENT  -> 	// подарок от клиента
                ctx.getString(R.string.report_operation_6_client_transm)

            OPERATION_MOMENTAL  -> 	// отправка подарка
                ctx.getString(R.string.report_operation_0_acc_moment)

            OPERATION_BONUSES_TOTAL  -> 	// Бонусов на счетах клиентов
                ctx.getString(R.string.report_operation_70_total)


            OPERATION_BONUSES_IN_PROMISES  -> 	// Бонусов обещано клиентам
                ctx.getString(R.string.report_operation_71_total)

            else -> ""

        }

        holder.bindData(item, textType, mClickListener)
    }

    // возвращаем кол-во элементов списка
    override fun getItemCount(): Int {
        return data.size
    }

    // внутренний класс для хранения и настройки элементов карточки клиента
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private var tvGroupType: TextView? = null
        private var tvGroupValue: TextView? = null
        private var listener: ItemClickListener? = null

        private lateinit var item: ReportOperationInfo

        init {
            // находим текстовые поля, в которые будем вставлять даные
            tvGroupType = itemView.findViewById(R.id.tvGroupType)
            tvGroupValue = itemView.findViewById(R.id.tvGroupValue)

            // применяем обработчик нажатия на элемент списка
            itemView.setOnClickListener(this)
        }

        fun bindData(data: ReportOperationInfo, textType:String, listener: ItemClickListener?) {

            tvGroupType?.text = textType
            tvGroupValue?.text = data.sum.toString()

            this.item = data
            this.listener = listener
        }

        // обработчик нажатия на элемент
        override fun onClick(view: View) {
            listener?.onItemClick(view, item)
        }
    }

    // метод для применения обработчика нажатий на элемент списка
    fun setClickListener(itemClickListener: ItemClickListener) {
        this.mClickListener = itemClickListener
    }

    // интерфейс, описывающий метод обработчика нажатий на элемент
    interface ItemClickListener {
        fun onItemClick(view:View, item: ReportOperationInfo)
    }
}