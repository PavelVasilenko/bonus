package ru.devroom.krizalbonus.ui.actions


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_actions.*
import ru.devroom.krizalbonus.IFragmentCallback

import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.Codes
import ru.devroom.krizalbonus.model.objects.ActionInfo
import ru.devroom.krizalbonus.ui.edit_action.EditActionActivity

class ActionsFragment : MvpAppCompatFragment(), IActionsFragment, ActionsAdapter.ItemClickListener {

    @InjectPresenter
    lateinit var mPresenter: ActionsFragmentPresenter

    companion object {
        const val RESULT_FOR_EDIT_ACTION = 72
    }

    private var listener: IFragmentCallback? = null

    // адаптер списка
    private var adapter: ActionsAdapter? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_actions, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listener?.inflateMenu(null)

        adapter = ActionsAdapter()
        adapter!!.setClickListener(this)

        rvActionsList?.layoutManager = LinearLayoutManager(context)
        rvActionsList.adapter = adapter

        fabAdd.setOnClickListener {
            val intent = Intent(context, EditActionActivity::class.java)
            startActivityForResult(intent, RESULT_FOR_EDIT_ACTION)
        }

        if (mPresenter.isAdmin()) {
            rvActionsList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                }

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    if (dy > 0) {
                        fabAdd.hide()
                    } else {
                        fabAdd.show()
                    }
                }
            })
        } else {
            fabAdd.hide()
        }

        mPresenter.updateActionsList()
        hideProgress()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is IFragmentCallback) {
            listener = context
        }
    }

    override fun insertActionsList(data: ArrayList<ActionInfo>) {
        adapter?.setData(data)
    }

    override fun errorLoading() {
        Toast.makeText(context, R.string.error_while_loading, Toast.LENGTH_SHORT).show()
    }

    override fun errorAuth() {
        Toast.makeText(context, R.string.error_while_auth, Toast.LENGTH_SHORT).show()
        listener?.onAuthError()
    }

    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar.visibility = View.GONE
    }

    override fun onItemClick(view: View, item: ActionInfo) {
        if (mPresenter.isAdmin()) {
            val serialized = Gson().toJson(item, ActionInfo::class.java)

            val intent = Intent(context, EditActionActivity::class.java)
            intent.putExtra(EditActionActivity.SERIALIZED_ITEM, serialized)
            startActivityForResult(intent, RESULT_FOR_EDIT_ACTION)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            RESULT_FOR_EDIT_ACTION -> {
                if (resultCode == Codes.OK) {
                    mPresenter.updateActionsList()
                }
            }
        }
    }
}
