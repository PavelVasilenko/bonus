package ru.devroom.krizalbonus.ui.edit_gifts

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.devroom.krizalbonus.model.AppUser
import ru.devroom.krizalbonus.model.network.RetrofitInstance

@InjectViewState
class EditGiftsActivityPresenter:MvpPresenter<IEditGiftsActivity>() {


    // флаг текущего сеанса отправки/загрузки
    @Volatile
    var serverBusy = false

    fun insertEditedData(sum:Int, days:Int) {
        viewState.insertValues(sum, days)
    }

    fun saveItem(
        sum:String, days:String
    ) {
        // если сервер занят - выходим
        if (serverBusy) return

        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }

        // проверки данных
        val checkedSum:Int = try {
            sum.toInt()
        }catch (e:java.lang.Exception) {
            -1
        }
        if (checkedSum < 0) {
            viewState.errorEmptySum()
            return
        }

        val checkedDays:Int = try {
            days.toInt()
        }catch (e:java.lang.Exception) {
            -1
        }
        if (checkedDays < 0) {
            viewState.errorEmptyDays()
            return
        }

        // показать анимацию загрузки данных
        viewState.showProgress()
        // в новом потоке - запрашиваем данные с сервера
        GlobalScope.launch {
            // ставим флаг активной загрузки
            serverBusy = true

            val response = try {
                RetrofitInstance.getService.updateGiftsAsync(
                    checkedSum,
                    checkedDays,
                    token).await()
            } catch (e: Exception) {
                null
            }

            // в основном потоке - показываем пользователю информацию
            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true) {
                    viewState.finishWithOk()
                } else {
                    when(response?.code()) {
                        401 -> viewState.errorAuth()
                        else -> viewState.errorSavingItem()
                    }
                }
                // убираем флаг текущей загрузки
                serverBusy = false
                // прячем анимацию загрузки данных
                viewState.hideProgress()
            }
        }
    }


}