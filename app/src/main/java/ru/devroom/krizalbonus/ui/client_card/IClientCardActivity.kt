package ru.devroom.krizalbonus.ui.client_card

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.devroom.krizalbonus.model.objects.ClientInfo
import ru.devroom.krizalbonus.model.objects.OperationInfo
import ru.devroom.krizalbonus.model.objects.PaymentInfo

interface IClientCardActivity:MvpView {
    @StateStrategyType(SingleStateStrategy::class)
    fun insertClientInfo(data:ClientInfo)

    @StateStrategyType(SkipStrategy::class)
    fun errorLoading()

    @StateStrategyType(SkipStrategy::class)
    fun clientNotFound()

    @StateStrategyType(SkipStrategy::class)
    fun errorAuth()

    @StateStrategyType(SkipStrategy::class)
    fun errorDeleting()

    @StateStrategyType(SingleStateStrategy::class)
    fun insertClientOperations(data:ArrayList<OperationInfo>)

    @StateStrategyType(SingleStateStrategy::class)
    fun showPurchaseLayout(animation:Boolean, completeShowed:Boolean)

    @StateStrategyType(SkipStrategy::class)
    fun hidePurchaseLayout()

    @StateStrategyType(SkipStrategy::class)
    fun showAcceptToDeleteDialog(code:String)

    @StateStrategyType(SkipStrategy::class)
    fun errorWrongCommitCode()

    @StateStrategyType(SkipStrategy::class)
    fun clientDeleted()

    @StateStrategyType(SkipStrategy::class)
    fun clientNotFoundToast()

    @StateStrategyType(SkipStrategy::class)
    fun clientNotAuthYet()

    @StateStrategyType(SkipStrategy::class)
    fun errorWhileDeleting()

    @StateStrategyType(SkipStrategy::class)
    fun errorNewPasswordIsEmpty()

    @StateStrategyType(SkipStrategy::class)
    fun errorNewPasswordsNotEqual()

    @StateStrategyType(SkipStrategy::class)
    fun clientPasswordChanged()

    @StateStrategyType(SkipStrategy::class)
    fun passwordChangeError()

    @StateStrategyType(SkipStrategy::class)
    fun bonusesWasCleared()

    @StateStrategyType(SkipStrategy::class)
    fun bonusesClearError()

    @StateStrategyType(SkipStrategy::class)
    fun errorEmptyPushMessage()

    @StateStrategyType(SkipStrategy::class)
    fun pushMessageSent()

    @StateStrategyType(SkipStrategy::class)
    fun answerMessageWasSent()

    @StateStrategyType(SkipStrategy::class)
    fun answerMessageSentError()

    @StateStrategyType(SkipStrategy::class)
    fun pushMessageSentError()

    @StateStrategyType(SkipStrategy::class)
    fun errorWrongSumToSend()

    @StateStrategyType(SkipStrategy::class)
    fun bonusesWasSent()

    @StateStrategyType(SkipStrategy::class)
    fun bonusesSentError()

    fun showProgress()
    fun hideScroll()
    fun showScroll()
    fun hideProgress()

    fun showFabPurchase()
    fun hideFabPurchase()
    fun showFabReload()
    fun hideFabReload()
    fun hideError()

    fun showPayComplete()
    fun clientLocked()
    fun errorWhilePay()
    fun scrollToPay()

    fun showAnswerToClientDialog(payment: PaymentInfo)

    @StateStrategyType(SkipStrategy::class)
    fun clearGoodCost()

    @StateStrategyType(SingleStateStrategy::class)
    fun updateCalculation(discount:Int, cashBack:Int, total:Int)

    fun errorEmptyTotalCost()
}