package ru.devroom.krizalbonus.ui.edit_action

import android.app.DatePickerDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.DatePicker
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.activity_edit_action.*
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.Codes
import java.util.*

class EditActionActivity : MvpAppCompatActivity(), IEditActionActivity,
    DatePickerDialog.OnDateSetListener {

    @InjectPresenter
    lateinit var mPresenter: EditActionActivityPresenter

    companion object {
        const val SERIALIZED_ITEM = "edited_item"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_action)


        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        fabSave.setOnClickListener {
            mPresenter.saveItem(
                etCaption.text.toString(),
                etActionText.text.toString(),
                swActionEnabled.isChecked
            )
        }

        val data: String? = intent.getStringExtra(SERIALIZED_ITEM)
        if (data != null) {
            fabDelete.show()
            fabDelete.setOnClickListener { onDeleteClicked() }
            mPresenter.insertEditedItem(data)
        } else {
            fabDelete.hide()
        }

        ibSelectDocsDate.setOnClickListener {
            // подготавливаем дату для календаря
            val lastDate = mPresenter.lastSelectedStartDate()
            val calendar = Calendar.getInstance()
            if (lastDate != null) calendar.timeInMillis = lastDate * 1000
            val day = calendar.get(Calendar.DAY_OF_MONTH)
            val month = calendar.get(Calendar.MONTH)
            val year = calendar.get(Calendar.YEAR)
            // показываем календарь
            DatePickerDialog(this, this, year, month, day).show()
        }

        ibSelectEndDate.setOnClickListener {
            // подготавливаем дату для календаря
            val lastDate = mPresenter.lastSelectedEndDate()
            val calendar = Calendar.getInstance()
            if (lastDate != null) calendar.timeInMillis = lastDate * 1000
            val day = calendar.get(Calendar.DAY_OF_MONTH)
            val month = calendar.get(Calendar.MONTH)
            val year = calendar.get(Calendar.YEAR)
            // показываем календарь
            DatePickerDialog(this, this, year, month, day).show()
        }

        hideProgress()
    }

    // событие при выборе даты
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        // собираем дату
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.HOUR_OF_DAY, 1)
        // передаем дату в презентер
        mPresenter.dateSelected(calendar.timeInMillis)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun onDeleteClicked() {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_caption)
            .setMessage(R.string.dialog_delete_action)
            .setNegativeButton(android.R.string.no) { _: DialogInterface, _: Int -> }
            .setPositiveButton(android.R.string.yes) { _: DialogInterface, _: Int ->
                mPresenter.deleteItem()
            }.show()
    }

    override fun insertValues(caption: String, text: String, enabled: Boolean) {
        etCaption.setText(caption)
        etActionText.setText(text)
        swActionEnabled.isChecked = enabled
    }

    override fun insertStartDate(date: String) {
        etActionStart.setText(date)
    }

    override fun insertEndDate(date: String) {
        etActionEnd.setText(date)
    }

    override fun errorEmptyCaption() {
        etCaption.error = getString(R.string.edit_field_empty)
    }

    override fun errorEmptyText() {
        etActionText.error = getString(R.string.edit_field_empty)
    }

    override fun errorEmptyStartDate() {
        etActionStart.error = getString(R.string.edit_field_empty)
    }

    override fun errorEmptyEndDate() {
        etActionEnd.error = getString(R.string.edit_field_empty)
    }

    override fun errorSavingItem() {
        Toast.makeText(this, R.string.error_while_saving, Toast.LENGTH_SHORT).show()
    }

    override fun finishWithOk() {
        setResult(Codes.OK)
        finish()
    }

    override fun errorDeletingItem() {
        Toast.makeText(this, R.string.error_while_deleting, Toast.LENGTH_SHORT).show()
    }

    override fun errorAuth() {
        Toast.makeText(this, R.string.error_while_auth, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressBar2.visibility = View.VISIBLE
        fabDelete.hide()
        fabSave.hide()
    }

    override fun hideProgress() {
        progressBar2.visibility = View.GONE
        fabSave.show()
        if (mPresenter.getEditedItem() == null)
            fabDelete.hide()
        else
            fabDelete.show()
    }
}
