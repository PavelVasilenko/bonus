package ru.devroom.krizalbonus.ui.edit_level

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import ru.devroom.krizalbonus.R
import kotlinx.android.synthetic.main.activity_edit_level.*
import ru.devroom.krizalbonus.model.Codes


class EditLevelActivity : MvpAppCompatActivity(), IEditLevelActivity {

    @InjectPresenter
    lateinit var mPresenter:EditLevelActivityPresenter

    companion object {
        const val SERIALIZED_ITEM = "edited_item"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_level)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        fabSave.setOnClickListener { mPresenter.saveItem(
            etPurchases.text.toString(),
            etCashBack.text.toString(),
            etDiscount.text.toString(),
            etLiveTime.text.toString(),
            etDelay.text.toString()
        ) }

        val data:String? = intent.getStringExtra(SERIALIZED_ITEM)
        if(data != null) {
            fabDelete.show()
            fabDelete.setOnClickListener { onDeleteClicked() }
            mPresenter.insertEditedItem(data)
        } else {
            fabDelete.hide()
        }

        hideProgress()
    }

    private fun onDeleteClicked() {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_caption)
            .setMessage(R.string.dialog_delete_item)
            .setNegativeButton(android.R.string.no){ _: DialogInterface, _: Int -> }
            .setPositiveButton(android.R.string.yes){ _: DialogInterface, _: Int ->
                mPresenter.deleteItem()
            }.show()
    }

    override fun insertValues(
        purchases: Int,
        cashBack: Int,
        discount: Int,
        liveTime: Int,
        delay: Int
    ) {
        etPurchases.setText(purchases.toString())
        etCashBack.setText(cashBack.toString())
        etDiscount.setText(discount.toString())
        etLiveTime.setText(liveTime.toString())
        etDelay.setText(delay.toString())
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun errorEmptyPurchases() {
        etPurchases.error = getString(R.string.edit_field_empty)
    }

    override fun errorEmptyCashBack() {
        etCashBack.error = getString(R.string.edit_field_empty)
    }

    override fun errorEmptyDiscount() {
        etDiscount.error = getString(R.string.edit_field_empty)
    }

    override fun errorEmptyLiveTime() {
        etLiveTime.error = getString(R.string.edit_field_empty)
    }

    override fun errorEmptyDelay() {
        etDelay.error = getString(R.string.edit_field_empty)
    }

    override fun errorOccupiedPurchases() {
        Toast.makeText(this, R.string.edit_purchase_sum_occupied, Toast.LENGTH_SHORT).show()
    }

    override fun errorSavingItem() {
        Toast.makeText(this, R.string.error_while_saving, Toast.LENGTH_SHORT).show()
    }

    override fun finishWithOk() {
        setResult(Codes.OK)
        finish()
    }

    override fun errorDeletingItem() {
        Toast.makeText(this, R.string.error_while_deleting, Toast.LENGTH_SHORT).show()
    }

    override fun errorAuth() {
        Toast.makeText(this, R.string.error_while_auth, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressBar2.visibility = View.VISIBLE
        fabDelete.hide()
        fabSave.hide()
    }

    override fun hideProgress() {
        progressBar2.visibility = View.GONE
        fabSave.show()
        if(mPresenter.getEditedItem() == null)
            fabDelete.hide()
        else
            fabDelete.show()
    }

}
