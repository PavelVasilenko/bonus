package ru.devroom.krizalbonus.ui.reports.new_clients_activity

import android.net.Uri
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.devroom.krizalbonus.model.objects.ClientInfo

interface IReportNewClientsActivity:MvpView {

    @StateStrategyType(SingleStateStrategy::class)
    fun insertClientsList(data:ArrayList<ClientInfo>)

    @StateStrategyType(SkipStrategy::class)
    fun errorLoading()

    @StateStrategyType(SkipStrategy::class)
    fun errorAuth()

    fun showProgress()

    fun hideProgress()

    fun setDates(dates:String)


    @StateStrategyType(SkipStrategy::class)
    fun openFile(file: Uri)
}