package ru.devroom.krizalbonus.ui.reports


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.fragment_reports.*

import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.ui.reports.bonuses_activity.ReportBonusesActivity
import ru.devroom.krizalbonus.ui.reports.new_clients_activity.ReportNewClientsActivity
import ru.devroom.krizalbonus.ui.reports.purchases_activity.ReportPurchasesActivity

/**
 * A simple [Fragment] subclass.
 */
class ReportsFragment : MvpAppCompatFragment(), IReportsFragment {

    @InjectPresenter
    lateinit var mPresenter:ReportsFragmentPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reports, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        clReportPurchases.setOnClickListener {
            startActivity(Intent(context, ReportPurchasesActivity::class.java))
        }

        clReportBonuses.setOnClickListener {
            startActivity(Intent(context, ReportBonusesActivity::class.java))
        }

        clReportNewClients.setOnClickListener {
            startActivity(Intent(context, ReportNewClientsActivity::class.java))
        }
    }
}
