package ru.devroom.krizalbonus.ui.edit_gifts

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface IEditGiftsActivity:MvpView {

    fun insertValues(sum:Int, days:Int)

    @StateStrategyType(SkipStrategy::class)
    fun errorEmptySum()

    @StateStrategyType(SkipStrategy::class)
    fun errorEmptyDays()

    @StateStrategyType(SkipStrategy::class)
    fun errorSavingItem()

    @StateStrategyType(SkipStrategy::class)
    fun finishWithOk()

    @StateStrategyType(SkipStrategy::class)
    fun errorAuth()

    fun showProgress()
    fun hideProgress()
}