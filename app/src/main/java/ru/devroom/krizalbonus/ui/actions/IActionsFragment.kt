package ru.devroom.krizalbonus.ui.actions

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.devroom.krizalbonus.model.objects.ActionInfo

interface IActionsFragment:MvpView {
    @StateStrategyType(SingleStateStrategy::class)
    fun insertActionsList(data:ArrayList<ActionInfo>)

    @StateStrategyType(SkipStrategy::class)
    fun errorLoading()

    @StateStrategyType(SkipStrategy::class)
    fun errorAuth()

    fun showProgress()

    fun hideProgress()
}