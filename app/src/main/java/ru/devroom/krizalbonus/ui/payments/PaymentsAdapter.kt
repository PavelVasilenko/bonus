package ru.devroom.krizalbonus.ui.payments

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.objects.PaymentInfo
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class PaymentsAdapter: RecyclerView.Adapter<PaymentsAdapter.ViewHolder>(){
    // список документов
    private var data:List<PaymentInfo> = ArrayList()
    //обработчик нажатия на элемент списка
    private lateinit var mClickListener : ItemClickListener

    private var oldDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    private var newDateFormat = SimpleDateFormat("HH:mm dd.MM.yyyy", Locale.getDefault())

    companion object {
        const val ITEM_RECORD = 1
        const val ITEM_EMPTY_LIST = 0
    }

    override fun getItemViewType(position: Int): Int {
        return if(data.isNullOrEmpty()) ITEM_EMPTY_LIST else ITEM_RECORD
    }

    // метод для установки нового списка
    fun setData(newData:List<PaymentInfo>?) {
        // запоминаем список
        if(newData.isNullOrEmpty()) {
            this.data = ArrayList()
        } else {
            this.data = newData
        }

        this.data.forEach {
            try {
                val date = oldDateFormat.parse(it.dateTime)
                if (date != null) it.dateTime = newDateFormat.format(date)
            } catch (e:Exception) {}
        }
        notifyDataSetChanged() // уведомляем менеджер о том, что список поменялся
    }

    // воздаем вью для элемента списка
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                if(viewType == ITEM_RECORD)
                        R.layout.item_purchase
                else    R.layout.item_empty_list
                ,parent, false)
        )
    }

    // при присоединиении элемента списка к recyclerView
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // оформляем данные внутри элемента списка
        if(getItemViewType(position) == ITEM_RECORD) {
            val item = data[position]
            holder.bindData(item, mClickListener)
        }
    }

    // возвращаем кол-во элементов списка
    override fun getItemCount(): Int {
        return if(data.isEmpty()) 1 else data.size
    }

    // внутренний класс для хранения и настройки элементов карточки клиента
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private var tvDateTime: TextView? = null
        private var tvClientFio: TextView? = null
        private var tvMarkComment: TextView? = null
        private var tvPayed: TextView? = null
        private var tvHasComment: TextView? = null
        private var listener: ItemClickListener? = null

        private lateinit var item: PaymentInfo

        init {
            // находим текстовые поля, в которые будем вставлять даные
            tvDateTime = itemView.findViewById(R.id.tvDateTime)
            tvClientFio = itemView.findViewById(R.id.tvClientFio)
            tvMarkComment = itemView.findViewById(R.id.tvMarkComment)
            tvHasComment = itemView.findViewById(R.id.tvHasComment)
            tvPayed = itemView.findViewById(R.id.tvPayed)

            // применяем обработчик нажатия на элемент списка
            itemView.setOnClickListener(this)
        }

        fun bindData(data: PaymentInfo, listener: ItemClickListener) {

            val total = "= " + data.totalCost.toString()

            val ctx = itemView.context

            val text = if(data.clientFio.isNullOrEmpty()) { // !!! isNullOrEmpty
                itemView.context.getString(R.string.client_item_used_bonuses, data.usedBonuses)
            } else {
                data.clientFio
            }


            val ratingByClient = if(data.mark > 0)
                ctx.getString(R.string.payment_has_mark, data.mark)
            else
                ""
            if(ratingByClient.isNotEmpty()) {
                tvMarkComment?.text = ratingByClient
                tvMarkComment?.visibility = View.VISIBLE
            } else {
                tvMarkComment?.visibility = View.GONE
            }


            if(data.comment.isNullOrEmpty()) {
                tvHasComment?.visibility = View.GONE
            } else {
                tvHasComment?.visibility = View.VISIBLE
                //tvHasComment?.text = data.comment
            }

            if(data.comment.isNotEmpty()) {
                if(data.adminComment.isEmpty()) {
                    tvHasComment?.setTextColor(ContextCompat.getColor(ctx, android.R.color.holo_red_dark))
                } else {
                    tvHasComment?.setTextColor(ContextCompat.getColor(ctx, R.color.colorTextDarkGray))
                }
            }

            tvDateTime?.text = data.dateTime
            tvClientFio?.text = text

            tvPayed?.text = total

            // запоминаем объект, по которому сделана эта карточка
            this.item = data
            // запоминаем слушатель событий
            this.listener = listener
        }

        // обработчик нажатия на элемент
        override fun onClick(view: View) {
            if (listener != null)
                listener!!.onItemClick(view, item)
        }
    }

    // метод для применения обработчика нажатий на элемент списка
    fun setClickListener(itemClickListener: ItemClickListener) {
        this.mClickListener = itemClickListener
    }

    // интерфейс, описывающий метод обработчика нажатий на элемент
    interface ItemClickListener {
        fun onItemClick(view:View, item: PaymentInfo)
    }
}