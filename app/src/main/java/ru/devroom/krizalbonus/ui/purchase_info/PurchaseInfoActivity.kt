package ru.devroom.krizalbonus.ui.purchase_info

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.widget.Toolbar
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.activity_purchase_info.*
import kotlinx.android.synthetic.main.activity_purchase_info.tvAdminComment
import kotlinx.android.synthetic.main.activity_purchase_info.tvCardNumber
import kotlinx.android.synthetic.main.activity_purchase_info.tvClientComment
import kotlinx.android.synthetic.main.activity_purchase_info.tvClientFio
import kotlinx.android.synthetic.main.activity_purchase_info.tvClientMark
import kotlinx.android.synthetic.main.activity_purchase_info.tvDiscountSize
import kotlinx.android.synthetic.main.activity_purchase_info.tvDocumentDate
import kotlinx.android.synthetic.main.activity_purchase_info.tvGoodCost
import kotlinx.android.synthetic.main.activity_purchase_info.tvPayedTotal
import org.w3c.dom.Text
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.Codes
import ru.devroom.krizalbonus.model.objects.PaymentInfo
import ru.devroom.krizalbonus.ui.client_card.ClientCardActivity
import ru.devroom.krizalbonus.ui.payments.PaymentFragment

class PurchaseInfoActivity : MvpAppCompatActivity(), IPurchaseInfoActivity {

    @InjectPresenter
    lateinit var mPresenter:PurchaseInfoActivityPresenter

    companion object {
        const val SERIALIZED_ITEM = "item"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_purchase_info)


        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        progressBar2.visibility = View.INVISIBLE

        mPresenter.setPackedItem(intent.getStringExtra(SERIALIZED_ITEM))
    }

    override fun insertInfo(item: PaymentInfo) {
        tvDocumentDate.text = item.dateTime
        tvCardNumber.text = item.clientId.toString()
        tvClientFio.text = item.clientFio
        tvGoodCost.text = item.goodCost.toString()
        tvDiscountSize.text = item.usedBonuses.toString()
        tvPayedTotal.text = item.totalCost.toString()


        if (item.mark > 0) {
            tvClientMark?.visibility = View.VISIBLE
            tvClientMark.text = getString(R.string.payment_mark, item.mark)
        } else {
            tvClientMark?.visibility = View.GONE
        }

        if (item.adminComment.isEmpty()) {
//            textView48?.visibility = View.GONE
            tvAdminComment?.visibility = View.GONE
        } else {
//            textView48?.visibility = View.VISIBLE
            tvAdminComment?.visibility = View.VISIBLE
            tvAdminComment.text = item.adminComment
        }

        if (item.comment.isEmpty()) {
            tvClientComment?.visibility = View.GONE
//            textView35?.visibility = View.GONE
        } else {
            tvClientComment?.visibility = View.VISIBLE
//            textView35?.visibility = View.VISIBLE
            tvClientComment.text = item.comment

        }

        if (item.note.isEmpty()) {
            tvNote?.visibility = View.GONE
            // textView49?.visibility = View.GONE
        } else {
            tvNote?.visibility = View.VISIBLE
            //textView49?.visibility = View.VISIBLE
            tvNote.text = item.note

        }

        bMessage.setOnClickListener {
            mPresenter.showWriteAnswerToClientClicked(item)
        }

        if (mPresenter.isAdmin()) {
            bDelete.setOnClickListener {
                AlertDialog.Builder(this@PurchaseInfoActivity)
                    .setTitle(R.string.dialog_caption)
                    .setMessage(R.string.dialog_delete_purchase)
                    .setNegativeButton(android.R.string.cancel) { _: DialogInterface, _: Int -> }
                    .setPositiveButton(android.R.string.yes) { _: DialogInterface, _: Int ->
                        mPresenter.deletePurchase(item.serverId)
                    }
                    .show()
            }
                    } else {
            bDelete.visibility = View.GONE
            bMessage.visibility = View.GONE
        }


        bClientCard.setOnClickListener {
            mPresenter.openClientCard(item.clientId.toString(), ClientCardActivity.MODE_VIEW)
        }

        bNote.setOnClickListener {
            mPresenter.showEditNoteDialogClicked(item)
        }


    }

    override fun updateAnswer(answerText: String) {
        tvAdminComment?.visibility = View.VISIBLE
        tvAdminComment.text = answerText
    }

    override fun updateNote(noteText: String) {
        tvNote?.visibility = View.VISIBLE
        tvNote.text = noteText
    }

    override fun errorClientCode() {
        finish()
    }

    override fun openClientCard(id: Int, openMode: String) {
        val intent = Intent(this, ClientCardActivity::class.java)
        intent.putExtra(ClientCardActivity.PARAM_CLIENT_CODE, id)
        intent.putExtra(ClientCardActivity.PARAM_CLIENT_OPENMODE, openMode)
//        intent.putExtra(ClientCardActivity.PARAM_CLIENT_OPENMODE, ClientCardActivity.MODE_VIEW)
        startActivityForResult(intent, PaymentFragment.RESULT_FOR_EDIT_PAYMENT)
    }

    override fun answerMessageWasSent() {
        Toast.makeText(this, R.string.answer_toast_was_sent, Toast.LENGTH_SHORT).show()
    }

    override fun answerMessageSentError() {
        Toast.makeText(this, R.string.answer_toast_sent_error, Toast.LENGTH_SHORT).show()
    }

    override fun noteSentError() {
        Toast.makeText(this, R.string.note_toast_sent_error, Toast.LENGTH_SHORT).show()
    }

    override fun noteMessageWasSent() {
        Toast.makeText(this, R.string.note_toast_was_sent, Toast.LENGTH_SHORT).show()
    }

    override fun showEditNoteDialog(payment: PaymentInfo, isAdmin:Boolean) {
        var dialog: AlertDialog? = null
        val dialogView = layoutInflater.inflate(R.layout.dialog_edit_note, null)
        val editText = dialogView.findViewById<EditText>(R.id.etMessageText)
        val tvOldText = dialogView.findViewById<TextView>(R.id.tvOldText)

        if(isAdmin) {
            tvOldText?.visibility = View.GONE
            editText?.setText(payment.note)
        } else {
            tvOldText?.visibility = View.VISIBLE
            tvOldText?.text = payment.note
            editText?.setText("")
        }

        dialogView.findViewById<Button>(R.id.bCancel)?.setOnClickListener {
            dialog?.dismiss()
        }
        dialogView.findViewById<Button>(R.id.bCommit)?.setOnClickListener {
            mPresenter.sendNewNote(payment, editText.text.toString())
            dialog?.dismiss()
        }
        dialog = AlertDialog.Builder(this@PurchaseInfoActivity).setView(dialogView).show()
    }

    override fun showAnswerToClientDialog(payment:PaymentInfo) {
        var dialog: AlertDialog? = null
        val dialogView = layoutInflater.inflate(R.layout.dialog_answer_for_comment, null)
        val editText = dialogView.findViewById<EditText>(R.id.etMessageText)

        dialogView.findViewById<Button>(R.id.bCancel)?.setOnClickListener {
            dialog?.dismiss()
        }
        dialogView.findViewById<Button>(R.id.bCommit)?.setOnClickListener {
            mPresenter.sendAnswerForComment(payment, editText.text.toString())
            dialog?.dismiss()
        }
        dialog = AlertDialog.Builder(this@PurchaseInfoActivity).setView(dialogView).show()
    }


    override fun errorEmptyPushMessage() {
        Toast.makeText(this, R.string.client_toast_empty_push, Toast.LENGTH_SHORT).show()
    }

    override fun errorDeleting() {
        Toast.makeText(this, R.string.error_while_deleting_purchase, Toast.LENGTH_SHORT).show()
    }

    override fun errorAuth() {
        Toast.makeText(this, R.string.error_while_auth, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progressBar2.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar2.visibility = View.INVISIBLE
    }

    override fun finishWithUpdatePurchasesList() {
        setResult(Codes.OK)
        finish()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
