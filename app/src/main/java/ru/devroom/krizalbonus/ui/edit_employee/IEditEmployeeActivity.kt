package ru.devroom.krizalbonus.ui.edit_employee

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface IEditEmployeeActivity:MvpView {

    fun insertValues(login:String, family:String)

    @StateStrategyType(SkipStrategy::class)
    fun errorEmptyLogin()

    @StateStrategyType(SkipStrategy::class)
    fun errorEmptyPassword()

    @StateStrategyType(SkipStrategy::class)
    fun errorEmptyFamily()

    @StateStrategyType(SkipStrategy::class)
    fun errorOccupiedLogin()

    @StateStrategyType(SkipStrategy::class)
    fun errorSavingItem()

    @StateStrategyType(SkipStrategy::class)
    fun finishWithOk()

    @StateStrategyType(SkipStrategy::class)
    fun errorDeletingItem()

    @StateStrategyType(SkipStrategy::class)
    fun errorAuth()

    fun showChangePassword()

    fun showProgress()
    fun hideProgress()
}