package ru.devroom.krizalbonus.ui.main_activity

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface IMainActivity:MvpView {
    fun showAuthActivity()


    @StateStrategyType(SkipStrategy::class)
    fun errorAuth()

    @StateStrategyType(SkipStrategy::class)
    fun errorEmptyPushMessage()

    @StateStrategyType(SkipStrategy::class)
    fun pushMessageWasSent()

    @StateStrategyType(SkipStrategy::class)
    fun pushMessageSentError()

}