package ru.devroom.krizalbonus.ui.reports.purchases_activity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_report_purchase.view.*
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.objects.PaymentInfo
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ReportPurchasesAdapter: RecyclerView.Adapter<ReportPurchasesAdapter.ViewHolder>(){
    // список документов
    private var data:List<PaymentInfo> = ArrayList()
    //обработчик нажатия на элемент списка
    private var mClickListener : ItemClickListener? = null

    private var oldDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    private var newDateFormat = SimpleDateFormat("HH:mm dd.MM.yyyy", Locale.getDefault())

    companion object {
        const val ITEM_RECORD = 1
        const val ITEM_HEADER = 0
    }

    override fun getItemViewType(position: Int): Int {
        return if(position == 0) ITEM_HEADER else ITEM_RECORD
    }

    // метод для установки нового списка
    fun setData(newData:List<PaymentInfo>?) {
        // запоминаем список
        if(newData.isNullOrEmpty()) {
            this.data = ArrayList()
        } else {
            this.data = newData
        }

        this.data.forEach {
            try {
                val date = oldDateFormat.parse(it.dateTime)
                if (date != null) it.dateTime = newDateFormat.format(date)
            } catch (e:Exception) {}
        }
        notifyDataSetChanged() // уведомляем менеджер о том, что список поменялся
    }

    // воздаем вью для элемента списка
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                if(viewType == ITEM_RECORD)
                        R.layout.item_report_purchase
                else    R.layout.item_report_purchase_header
                ,parent, false)
        )
    }

    // при присоединиении элемента списка к recyclerView
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // оформляем данные внутри элемента списка
        if(getItemViewType(position) == ITEM_RECORD) {
            val item = data[position - 1]
            holder.bindData(item, mClickListener)
        }
    }

    // возвращаем кол-во элементов списка
    override fun getItemCount(): Int {
        return 1 + data.size
    }

    // внутренний класс для хранения и настройки элементов карточки клиента
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private var tvDate: TextView? = null
        private var tvCost: TextView? = null
        private var tvDiscount: TextView? = null
        private var tvTotal: TextView? = null
        private var tvCashBack: TextView? = null
        private var tvHaveComment: TextView? = null
        private var tvHaveCommentAnswer: TextView? = null
        private var tvHaveNote: TextView? = null
        private var listener: ItemClickListener? = null

        private lateinit var item: PaymentInfo

        init {
            // находим текстовые поля, в которые будем вставлять даные
            tvDate = itemView.findViewById(R.id.tvDate)
            tvCost = itemView.findViewById(R.id.tvCost)
            tvDiscount = itemView.findViewById(R.id.tvDiscount)
            tvTotal = itemView.findViewById(R.id.tvTotal)
            tvCashBack = itemView.findViewById(R.id.tvCashBack)
            tvHaveComment = itemView.findViewById(R.id.tvHaveComment)
            tvHaveCommentAnswer = itemView.findViewById(R.id.tvHaveCommentAnswer)
            tvHaveNote = itemView.findViewById(R.id.tvHaveNote)

            // применяем обработчик нажатия на элемент списка
            itemView.setOnClickListener(this)
        }

        fun bindData(data: PaymentInfo, listener: ItemClickListener?) {

            tvHaveComment?.visibility = if(data.comment.isNotEmpty()) View.VISIBLE else View.GONE
            tvHaveCommentAnswer?.visibility = if(data.adminComment.isNotEmpty()) View.VISIBLE else View.GONE
            tvHaveNote?.visibility = if(data.note.isNotEmpty()) View.VISIBLE else View.GONE
            tvHaveNote?.text = data.note

            val totalCashBack = data.cashBack + data.promises
            tvCashBack?.text = totalCashBack.toString()

            tvDate?.text = data.dateTime
            tvCost?.text = data.goodCost.toString()
            tvDiscount?.text = data.usedBonuses.toString()
            tvTotal?.text = data.totalCost.toString()

            this.item = data
            this.listener = listener
        }

        // обработчик нажатия на элемент
        override fun onClick(view: View) {
            listener?.onItemClick(view, item)
        }
    }

    // метод для применения обработчика нажатий на элемент списка
    fun setClickListener(itemClickListener: ItemClickListener) {
        this.mClickListener = itemClickListener
    }

    // интерфейс, описывающий метод обработчика нажатий на элемент
    interface ItemClickListener {
        fun onItemClick(view:View, item: PaymentInfo)
    }
}