package ru.devroom.krizalbonus.ui.auth

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.activity_auth.*
import ru.devroom.krizalbonus.ui.main_activity.MainActivity
import ru.devroom.krizalbonus.R

// активити авторизации пользователя
class AuthActivity :MvpAppCompatActivity(), IAuthActivity {

    @InjectPresenter
    lateinit var mPresenter: AuthActivityPresenter

    companion object {
        const val REQUEST_CODE_PERMISSION = 77
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        // при нажатии кнопки авторизации
        bLogin.setOnClickListener {
            hideKeyboard(this@AuthActivity)
            mPresenter.auth(
                etLogin.text.toString(),
                etPassword.text.toString()
            )
        }
        hideProgress()


        val permissionStatus =
            ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
        if (permissionStatus != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this, arrayOf(
                    Manifest.permission.INTERNET,
                    Manifest.permission.CAMERA
                    ),
                REQUEST_CODE_PERMISSION
            )
        } else {
            // инициализация начальных данных
            mPresenter.init()
        }

    }

    override fun setLastLogin(value: String) {
        etLogin.setText(value)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            REQUEST_CODE_PERMISSION -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    AlertDialog
                        .Builder(this)
                        .setMessage(R.string.internet_denied)
                        .setPositiveButton(android.R.string.ok){ _: DialogInterface, _: Int -> }
                        .show()
                } else {
                    mPresenter.init()
                }
                return
            }
        }

    }


    // показать прогресс
    override fun showProgress() {
        progress.visibility = View.VISIBLE
    }
    // спрятать прогресс
    override fun hideProgress() {
        progress.visibility = View.GONE
    }
    // сообщение об ошибке
    override fun errorLoginEmpty() {
        etLogin.error = getString(R.string.auth_login_empty)
    }
    // сообщение об ошибке
    override fun errorPasswordEmpty() {
        etPassword.error = getString(R.string.auth_password_empty)
    }
    // сообщение об ошибке
    override fun showErrorAuth() {
        Toast.makeText(this, R.string.auth_error, Toast.LENGTH_SHORT).show()
    }
    // открытие рабочей активити
    override fun showMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
    // метод для скрытия экранной клавиатуры
    private fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = activity.currentFocus
        if (view == null) view = View(activity)
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}
