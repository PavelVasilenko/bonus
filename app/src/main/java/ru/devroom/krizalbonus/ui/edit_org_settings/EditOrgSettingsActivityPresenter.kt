package ru.devroom.krizalbonus.ui.edit_org_settings

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.devroom.krizalbonus.model.AppUser
import ru.devroom.krizalbonus.model.network.RetrofitInstance
import ru.devroom.krizalbonus.model.objects.LoadedSettings
import ru.devroom.krizalbonus.ui.settings.SettingsFragmentPresenter.Companion.SETTINGS_KEY_ORG_ABOUT
import ru.devroom.krizalbonus.ui.settings.SettingsFragmentPresenter.Companion.SETTINGS_KEY_ORG_ADDRESS
import ru.devroom.krizalbonus.ui.settings.SettingsFragmentPresenter.Companion.SETTINGS_KEY_ORG_EMAIL
import ru.devroom.krizalbonus.ui.settings.SettingsFragmentPresenter.Companion.SETTINGS_KEY_ORG_INSTA
import ru.devroom.krizalbonus.ui.settings.SettingsFragmentPresenter.Companion.SETTINGS_KEY_ORG_PHONE
import ru.devroom.krizalbonus.ui.settings.SettingsFragmentPresenter.Companion.SETTINGS_KEY_ORG_WEB
import ru.devroom.krizalbonus.ui.settings.SettingsFragmentPresenter.Companion.SETTINGS_KEY_ORG_WHATSAPP

@InjectViewState
class EditOrgSettingsActivityPresenter:MvpPresenter<IEditOrgSettingsActivity>() {

    @Volatile
    var serverBusy = false

    private var originalSettings:LoadedSettings? = null

    fun insertCurrentOptions(data:String) {
        originalSettings = Gson().fromJson(data, LoadedSettings::class.java)
        if(originalSettings == null) return

        viewState.insertOrgParameters(
            originalSettings!!.settings?.get(SETTINGS_KEY_ORG_PHONE) ?: "",
            originalSettings!!.settings?.get(SETTINGS_KEY_ORG_WHATSAPP) ?: "",
            originalSettings!!.settings?.get(SETTINGS_KEY_ORG_ADDRESS) ?: "",
            originalSettings!!.settings?.get(SETTINGS_KEY_ORG_WEB) ?: "",
            originalSettings!!.settings?.get(SETTINGS_KEY_ORG_EMAIL) ?: "",
            originalSettings!!.settings?.get(SETTINGS_KEY_ORG_INSTA) ?: "",
            originalSettings!!.settings?.get(SETTINGS_KEY_ORG_ABOUT) ?: ""
        )
    }

    fun saveItem(
        phone:String,
        whatsapp:String,
        address:String,
        site:String,
        email:String,
        insta:String,
        about:String
    ) {

        // если сервер занят - выходим
        if (serverBusy) return

        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }

        // показать анимацию загрузки данных
        viewState.showProgress()
        // в новом потоке - запрашиваем данные с сервера
        GlobalScope.launch {
            // ставим флаг активной загрузки
            serverBusy = true

            val response = try {
                RetrofitInstance.getService.updateOrgInfoAsync(
                    phone,
                    whatsapp,
                    address,
                    site,
                    email,
                    insta,
                    about,
                    token).await()
            } catch (e: Exception) {
                null
            }

            // в основном потоке - показываем пользователю информацию
            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true) {
                    viewState.finishWithOk()
                } else {
                    when(response?.code()) {
                        401 -> viewState.errorAuth()
                        else -> viewState.errorSavingItem()
                    }
                }
                // убираем флаг текущей загрузки
                serverBusy = false
                // прячем анимацию загрузки данных
                viewState.hideProgress()
            }
        }
    }

}