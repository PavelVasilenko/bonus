package ru.devroom.krizalbonus.ui.main_activity

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.google.zxing.integration.android.IntentIntegrator
import com.google.zxing.integration.android.IntentResult
import ru.devroom.krizalbonus.IFragmentCallback
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.ui.auth.AuthActivity
import ru.devroom.krizalbonus.ui.client_card.ClientCardActivity
import ru.devroom.krizalbonus.ui.clients.ClientsFragment
import ru.devroom.krizalbonus.ui.edit_client.EditClientActivity
import ru.devroom.krizalbonus.ui.payments.PaymentFragment
import java.lang.Exception

class MainActivity : MvpAppCompatActivity(), IMainActivity, IFragmentCallback {

    @InjectPresenter
    lateinit var mPresenter: MainActivityPresenter

    private lateinit var appBarConfiguration: AppBarConfiguration

    companion object {
        const val REQUEST_SCANNED_BARCODE_CODE = 49374
        const val ADD_NEW_CLIENT = 77
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_payment
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)

        if(mPresenter.isAdmin()) {
            navView.menu.clear()
            navView.inflateMenu(R.menu.activity_main_drawer)
        } else {
            navView.menu.clear()
            navView.inflateMenu(R.menu.activity_main_drawer_kassir)
        }

        navView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_Exit -> {
                    mPresenter.logout()
                }
                else -> {
                    navController.navigate(it.itemId)
                    drawerLayout.closeDrawer(GravityCompat.START)
                }
            }

            true
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuId = mPresenter.currentMenu()
        return if(menuId == null) {
            true
        } else {
            menuInflater.inflate(menuId, menu)
            true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_send_push_to_all -> {
                showDialogPushToAll()
                true
            }
            R.id.menu_new_client -> {
                val intent = Intent(this, EditClientActivity::class.java)
                startActivityForResult(intent, ADD_NEW_CLIENT)
                true
            }
            else ->
                super.onOptionsItemSelected(item)
        }
    }

    private fun showDialogPushToAll() {
        var dialog: AlertDialog? = null
        val dialogView = layoutInflater.inflate(R.layout.dialog_send_push, null)

        dialogView.findViewById<TextView>(R.id.tvHint)?.text = getString(R.string.client_push_dialog_to_all_hint)
        val editText = dialogView.findViewById<EditText>(R.id.etMessageText)
        dialogView.findViewById<Button>(R.id.bCancel)?.setOnClickListener {
            dialog?.dismiss()
        }
        dialogView.findViewById<Button>(R.id.bCommit)?.setOnClickListener {
            mPresenter.sendPushToAll(editText.text.toString())
            dialog?.dismiss()
        }
        dialog = AlertDialog.Builder(this).setView(dialogView).show()
    }

    override fun inflateMenu(menuId: Int?) {
        mPresenter.setCurrentMenuType(menuId)
        invalidateOptionsMenu()
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun errorAuth() {
        Toast.makeText(this, R.string.error_while_auth, Toast.LENGTH_SHORT).show()
    }

    override fun errorEmptyPushMessage() {
        Toast.makeText(this, R.string.client_toast_empty_push, Toast.LENGTH_SHORT).show()
    }

    override fun pushMessageWasSent() {
        Toast.makeText(this, R.string.client_toast_push_was_sent, Toast.LENGTH_SHORT).show()
    }

    override fun pushMessageSentError() {
        Toast.makeText(this, R.string.client_toast_push_sent_error, Toast.LENGTH_SHORT).show()
    }

    override fun showAuthActivity() {
        val intent = Intent(this, AuthActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onAuthError() {
        mPresenter.logout()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            ADD_NEW_CLIENT -> {
                val fragments = supportFragmentManager.primaryNavigationFragment?.childFragmentManager
                if(fragments != null && fragments.fragments.size > 0) {
                    val fragment = fragments.fragments[0]
                    if(fragment is ClientsFragment) {
                        fragment.updateList()
                    }
                }
            }
            REQUEST_SCANNED_BARCODE_CODE -> {
                val result: IntentResult? =
                    IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
                if (result != null) {
                    if (result.contents != null) {

                        if (supportFragmentManager.fragments.isNotEmpty()) {
                            val fr0 = supportFragmentManager.fragments[0].childFragmentManager
                            if (fr0.fragments.isNotEmpty()) {
                                val frPayment = fr0.fragments[0]
                                if (frPayment is PaymentFragment) {

                                    try {
                                        frPayment.openClientCard(
                                            result.contents.toInt(),
                                            ClientCardActivity.MODE_PURCHASE
                                        )
                                    } catch (e: Exception) {
                                        frPayment.errorClientCode()
                                    }
                                }
                            }
                        }

                    }
                }

            }
        }
    }
}
