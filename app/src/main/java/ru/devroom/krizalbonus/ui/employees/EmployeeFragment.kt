package ru.devroom.krizalbonus.ui.employees

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_employees.*
import ru.devroom.krizalbonus.IFragmentCallback
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.Codes
import ru.devroom.krizalbonus.model.objects.EmployeeInfo
import ru.devroom.krizalbonus.ui.edit_employee.EditEmployeeActivity

class EmployeeFragment : MvpAppCompatFragment(), IEmployeeFragment, EmployeeAdapter.ItemClickListener {

    @InjectPresenter
    lateinit var mPresenter: EmployeeFragmentPresenter

    companion object {
        const val RESULT_FOR_EDIT_EMPLOYEE = 71
    }

    private var listener: IFragmentCallback? = null

    // адаптер списка
    private var adapter: EmployeeAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_employees, container, false)
        return root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listener?.inflateMenu(null)

        adapter = EmployeeAdapter()
        adapter!!.setClickListener(this)

        rvEmployeesList?.layoutManager = LinearLayoutManager(context)
        rvEmployeesList.adapter = adapter

        fabAdd.setOnClickListener {
            val intent = Intent(context, EditEmployeeActivity::class.java)
            startActivityForResult(intent, RESULT_FOR_EDIT_EMPLOYEE)
        }

        rvEmployeesList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    fabAdd.hide()
                } else {
                    fabAdd.show()
                }
            }
        })

        mPresenter.updateEmployeesList()
        hideProgress()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is IFragmentCallback) {
            listener = context
        }
    }

    override fun insertEmployeesList(data: ArrayList<EmployeeInfo>) {
        adapter?.setData(data)
    }

    override fun errorLoading() {
        Toast.makeText(context, R.string.error_while_loading, Toast.LENGTH_SHORT).show()
    }

    override fun errorAuth() {
        Toast.makeText(context, R.string.error_while_auth, Toast.LENGTH_SHORT).show()
        listener?.onAuthError()
    }

    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar.visibility = View.GONE
    }

    override fun onItemClick(view: View, item: EmployeeInfo) {
        val serialized = Gson().toJson(item, EmployeeInfo::class.java)

        val intent = Intent(context, EditEmployeeActivity::class.java)
        intent.putExtra(EditEmployeeActivity.SERIALIZED_ITEM, serialized)
        startActivityForResult(intent, RESULT_FOR_EDIT_EMPLOYEE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            RESULT_FOR_EDIT_EMPLOYEE -> {
                if(resultCode == Codes.OK) {
                    mPresenter.updateEmployeesList()
                }
            }
        }
    }
}