package ru.devroom.krizalbonus.ui.clients

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.objects.ClientInfo
import kotlin.collections.ArrayList

class ClientsAdapter: RecyclerView.Adapter<ClientsAdapter.ViewHolder>(){
    // список документов
    private var data = ArrayList<ClientInfo>()
    //обработчик нажатия на элемент списка
    private lateinit var mClickListener : ItemClickListener

    companion object {
        const val ITEM_RECORD = 1
        const val ITEM_EMPTY_LIST = 0
    }

    override fun getItemViewType(position: Int): Int {
        return if(data.isNullOrEmpty()) ITEM_EMPTY_LIST else ITEM_RECORD
    }

    // метод для установки нового списка
    fun setData(newData:List<ClientInfo>?) {
        // запоминаем список
        if(newData.isNullOrEmpty()) {
            this.data = ArrayList()
        } else {
            this.data = newData as ArrayList<ClientInfo>
        }
        notifyDataSetChanged() // уведомляем менеджер о том, что список поменялся
    }

    // метод для добавления данных
    fun appendData(newData:List<ClientInfo>?) {
        // запоминаем список
        if(!newData.isNullOrEmpty()) {
            val cnt = this.data.size
            this.data.addAll(newData)
            notifyItemRangeChanged(cnt, this.data.size)
        }
    }

    // воздаем вью для элемента списка
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                if(viewType == ITEM_RECORD)
                    R.layout.item_client
                else    R.layout.item_empty_list
                ,parent, false)
        )
    }

    // при присоединиении элемента списка к recyclerView
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // оформляем данные внутри элемента списка
        if(getItemViewType(position) == ITEM_RECORD) {
            val item = data[position]
            holder.bindData(item, mClickListener)
        }
    }

    // возвращаем кол-во элементов списка
    override fun getItemCount(): Int {
        return if(data.isEmpty()) 1 else data.size
    }

    // внутренний класс для хранения и настройки элементов карточки клиента
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private var tvFamily: TextView? = null
        private var tvTelephone: TextView? = null
        private var listener: ItemClickListener? = null

        private lateinit var item: ClientInfo

        init {
            // находим текстовые поля, в которые будем вставлять даные
            tvFamily = itemView.findViewById(R.id.tvFamily)
            tvTelephone = itemView.findViewById(R.id.tvTelephone)

            // применяем обработчик нажатия на элемент списка
            itemView.setOnClickListener(this)
        }

        fun bindData(data: ClientInfo, listener: ItemClickListener) {

            tvFamily?.text = if(data.fio.isNotEmpty()) data.fio else data.telephone
            tvTelephone?.text = data.telephone

            // запоминаем объект, по которому сделана эта карточка
            this.item = data
            // запоминаем слушатель событий
            this.listener = listener
        }

        // обработчик нажатия на элемент
        override fun onClick(view: View) {
            if (listener != null)
                listener!!.onItemClick(view, item)
        }
    }

    // метод для применения обработчика нажатий на элемент списка
    fun setClickListener(itemClickListener: ItemClickListener) {
        this.mClickListener = itemClickListener
    }

    // интерфейс, описывающий метод обработчика нажатий на элемент
    interface ItemClickListener {
        fun onItemClick(view:View, item: ClientInfo)
    }
}