package ru.devroom.krizalbonus.ui.reports.purchases_activity

import android.content.Context
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.devroom.krizalbonus.model.AppUser
import ru.devroom.krizalbonus.model.network.RetrofitInstance
import ru.devroom.krizalbonus.model.objects.PaymentInfo
import ru.devroom.krizalbonus.model.XLSExporter
import java.text.SimpleDateFormat
import java.util.*

@InjectViewState
class ReportPurchasesActivityPresenter:MvpPresenter<IReportPurchasesActivity>() {


    private var dateFormatServer = SimpleDateFormat("yyyy-MM-dd", Locale.US)
    private var dateFormatApp = SimpleDateFormat("dd.MM.yyyy", Locale.US)

    private var startDate:Long = 0
    private var endDate:Long = 0

    @Volatile private var totalOperations:Int = 0
    @Volatile private var totalGoodCosts:Int = 0
    @Volatile private var totalDiscounts:Int = 0
    @Volatile private var totalTotals:Int = 0
    @Volatile private var lastLoadedData:ArrayList<PaymentInfo>? = null

    private var selectedCardId:Int? = null
    private var selectedEmployeeId:Int? = null
    // флаг текущего сеанса отправки/загрузки
    @Volatile var serverBusyReceiving = false

    companion object {
        const val MAX_FOR_ALL_TIME = 3474002297039
        const val MIN_FOR_ALL_TIME = 318242297039
    }

    fun setCardId(cardId:Int?) {
        selectedCardId = cardId
    }

    fun setEmployeeId(employeeId:Int?) {
        selectedEmployeeId = employeeId
    }

    fun isAdmin() = AppUser.get().getEmployeeInfo()?.isAdminRole() ?: false

    fun getStartDateRange():Calendar? {
        return if(startDate > 0) {
            val startCalendar = GregorianCalendar(Locale.US)
            startCalendar.timeInMillis = startDate
            startCalendar
        } else {
            null
        }
    }

    fun getEndDateRange():Calendar? {
        return if(endDate > 0) {
            val endCalendar = GregorianCalendar(Locale.US)
            endCalendar.timeInMillis = endDate
            endCalendar
        } else {
            null
        }
    }

    fun setTodayDateRange() {
        val curDate = Date().time
        setDateRange(curDate, curDate)
    }

    fun setFullDateRange() {
        setDateRange(MIN_FOR_ALL_TIME, MAX_FOR_ALL_TIME)
    }

    fun isAllTimeSelected() = (startDate == MIN_FOR_ALL_TIME && endDate == MAX_FOR_ALL_TIME)

    fun setDateRange(start:Long, end:Long) {
        startDate = start
        endDate = end

        if(start == MIN_FOR_ALL_TIME && end == MAX_FOR_ALL_TIME) {
            viewState.setDates("")
        } else {
            var datesRange = dateFormatApp.format(startDate)
            if(startDate != endDate) datesRange += " - " + dateFormatApp.format(endDate)
            viewState.setDates(datesRange)
        }
        updatePaymentsList()
    }

    fun export(ctx: Context) {
        if (lastLoadedData != null) {
            viewState.showProgress()
            GlobalScope.launch {
                var datesRange = dateFormatApp.format(startDate)
                if (startDate != endDate) datesRange += " - " + dateFormatApp.format(endDate)
                val file = XLSExporter().exportPurchases(lastLoadedData!!, datesRange, ctx)
                withContext(Dispatchers.Main) {
                    if(file != null) {
                        viewState.openFile(file)
                    }
                    viewState.hideProgress()
                }

            }
        }
    }

    fun updatePaymentsList() {
        if (serverBusyReceiving) return

        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }
        viewState.showProgress()
        GlobalScope.launch {
            // ставим флаг активной загрузки
            serverBusyReceiving = true

            val date1 = dateFormatServer.format(Date(startDate))
            val date2 = dateFormatServer.format(Date(endDate))

            // запрос на получение данных
            val response = try {
                if(selectedCardId ?: -1 > 0) {
                    RetrofitInstance.getService.getPaymentsListByCardAsync(
                        selectedCardId ?: -1,
                        date1,
                        date2,
                        token
                    ).await()
                } else if(selectedEmployeeId ?: -1 > 0) {
                    RetrofitInstance.getService.getPaymentsListByEmployeeAsync(
                        selectedEmployeeId ?: -1,
                        date1,
                        date2,
                        token
                    ).await()
                } else {
                    RetrofitInstance.getService.getPaymentsListAsync(
                        date1,
                        date2,
                        token
                    ).await()
                }
            } catch (e: Exception) {
                null
            }

            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true && response.body() != null) {

                    totalOperations = 0
                    totalGoodCosts = 0
                    totalDiscounts = 0
                    totalTotals = 0

                    response.body()!!.forEach {
                        totalOperations++
                        totalGoodCosts += it.goodCost
                        totalDiscounts += it.usedBonuses
                        totalTotals += it.totalCost
                    }

                    viewState.insertReportList(response.body()!!)
                    lastLoadedData = response.body()

                    viewState.insertReportTotals(
                        totalOperations.toString(),
                        totalGoodCosts.toString(),
                        totalDiscounts.toString(),
                        totalTotals.toString()
                    )
                } else {
                    when (response?.code()) {
                        401 -> {
                            viewState.errorAuth()
                        }
                        else -> {
                            viewState.errorLoading()
                        }
                    }
                }
                // убираем флаг текущей загрузки
                serverBusyReceiving = false
                // прячем анимацию загрузки данных
                viewState.hideProgress()
            }
        }
    }
}