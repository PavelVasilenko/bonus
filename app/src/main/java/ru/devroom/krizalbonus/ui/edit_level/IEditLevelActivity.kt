package ru.devroom.krizalbonus.ui.edit_level

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface IEditLevelActivity:MvpView {

    fun insertValues(purchases:Int, cashBack:Int, discount:Int, liveTime:Int, delay:Int)


    @StateStrategyType(SkipStrategy::class)
    fun errorEmptyPurchases()

    @StateStrategyType(SkipStrategy::class)
    fun errorEmptyCashBack()

    @StateStrategyType(SkipStrategy::class)
    fun errorEmptyDiscount()

    @StateStrategyType(SkipStrategy::class)
    fun errorEmptyLiveTime()

    @StateStrategyType(SkipStrategy::class)
    fun errorEmptyDelay()

    @StateStrategyType(SkipStrategy::class)
    fun errorOccupiedPurchases()

    @StateStrategyType(SkipStrategy::class)
    fun errorSavingItem()

    @StateStrategyType(SkipStrategy::class)
    fun finishWithOk()

    @StateStrategyType(SkipStrategy::class)
    fun errorDeletingItem()

    @StateStrategyType(SkipStrategy::class)
    fun errorAuth()

    fun showProgress()
    fun hideProgress()
}