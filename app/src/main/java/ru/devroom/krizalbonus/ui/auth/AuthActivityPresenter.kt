package ru.devroom.krizalbonus.ui.auth

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.devroom.krizalbonus.model.AppUser
import ru.devroom.krizalbonus.model.network.RetrofitInstance
import java.math.BigInteger
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*

@InjectViewState
class AuthActivityPresenter: MvpPresenter<IAuthActivity>() {
    // флаг неоконченного соединения
    @Volatile var inLoginProcess = false

    companion object {
        private const val SAULT = "xxxxxx"

        fun encryptPassword(input: String): String {
            try {
                val password = SAULT + input
                val md = MessageDigest.getInstance("SHA-256")
                val messageDigest = md.digest(password.toByteArray())
                val no = BigInteger(1, messageDigest)
                var hashtext = no.toString(16)
                while (hashtext.length < 32) {
                    hashtext = "0$hashtext"
                }
                return hashtext.toUpperCase()
            } catch (e: NoSuchAlgorithmException) {
                throw RuntimeException(e)
            }
        }

    }

    // инициализация данных
    fun init() {
        // если раньше были авторизованы запускаем пользователькое активити
        startUserInterface()
    }

    // запуск пользовательского активити
    // если токен не просрочен
    private fun startUserInterface():Boolean {
        val userInfo = AppUser.get().getEmployeeInfo()

        return if (userInfo != null) {
            if(Date().time * 0.001 < userInfo.expired) {
                viewState.showMainActivity()
                true
            } else {
                viewState.setLastLogin(AppUser.get().getEmployeeInfo()?.login ?: "")
                false
            }
        } else
            false
    }

    //авторизация по логину и паролю
    fun auth(login:String, password:String) {
        // если предыдущий запрос на авторизацию еще не завершен
        if (inLoginProcess) return
        // проверка входных данных
        if (login.isEmpty()) {
            viewState.errorLoginEmpty()
            return
        }
        if (password.isEmpty()) {
            viewState.errorPasswordEmpty()
            return
        }
        // показываем прогресс
        viewState.showProgress()



        // в новом потоке - запрашиваем данные с сервера
        GlobalScope.launch {

            var shaPassword = ""

            // ставим флаг активной загрузки
            inLoginProcess = true
            // запрос к серверу
            val response = try {
                // шифруем пароль перед передачей
                shaPassword = encryptPassword(password)
                RetrofitInstance.getService.authAsync(login, shaPassword).await()
            } catch (e: Exception) {
                null
            } catch (e: NoSuchAlgorithmException) {
                e.printStackTrace()
                throw RuntimeException("No Such Algorithm")
            }

            withContext(Dispatchers.Main) {
                // убираем флаг текущей загрузки
                inLoginProcess = false
                // ответ сервера
                if (response?.isSuccessful == true && response.body() != null) {
                    val receivedData = response.body()
                    if (receivedData != null)
                        AppUser.get().onLoginSuccess(receivedData, shaPassword)
                } else {
                    viewState.showErrorAuth()
                }

                // прячем прогресс
                viewState.hideProgress()
                // если авторизваны, запускаем пользовательское активити
                startUserInterface()
            }
        }
    }
}