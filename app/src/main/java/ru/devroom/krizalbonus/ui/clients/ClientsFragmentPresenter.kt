package ru.devroom.krizalbonus.ui.clients

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import kotlinx.coroutines.*
import ru.devroom.krizalbonus.model.AppUser
import ru.devroom.krizalbonus.model.network.RetrofitInstance
import ru.devroom.krizalbonus.model.objects.ClientInfo

@InjectViewState
class ClientsFragmentPresenter:MvpPresenter<IClientsFragment>() {

    private val job = SupervisorJob()
    private val findScope = CoroutineScope(Dispatchers.Default + job)
    private val uiScope = CoroutineScope(Dispatchers.Main)


    // флаг текущего сеанса отправки/загрузки
    @Volatile var serverBusyReceiving = false

    @Volatile var haveToRepeatUpdateString:Boolean = false

    private var lastLoadedPosition:Int = 0
    private var lastSearchString = ""

    private var loadedCache:ArrayList<ClientInfo>? = null

    fun haveCache() = !loadedCache.isNullOrEmpty()

    fun initCache() {
        if (loadedCache != null) {
            viewState.insertClientsList(loadedCache!!, true)
            lastLoadedPosition = loadedCache!!.size
        }
    }

    fun clearList() {
        loadedCache?.clear()
        viewState.insertClientsList(null, true)
    }

    fun updateClients(typedString:String? = null, startPos:Int? = null) {
        Log.d("DEBBBUG", "----------------------------------------------------")
        Log.d("DEBBBUG", "updateClients ($typedString, $startPos)")

        // если сервер занят - выходим
        if (serverBusyReceiving) {
            Log.d("DEBBBUG", "сервер занят, просим повторить позже")
            haveToRepeatUpdateString = true
            return
        }

        val findString:String = typedString ?: lastSearchString
        lastSearchString = findString

        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }

        // показать анимацию загрузки данных
        viewState.showProgress()

        // в новом потоке - запрашиваем данные с сервера
        findScope.launch {
            Log.d("DEBBBUG", "============GlobalScope.launch")
            // ставим флаг активной загрузки
            serverBusyReceiving = true

            if(startPos != null) lastLoadedPosition = startPos
            Log.d("DEBBBUG", "startPos=$startPos")

            do {
                Log.d("DEBBBUG", "Начало цикла")


                haveToRepeatUpdateString = false
                Log.d("DEBBBUG", "haveToRepeatUpdateString = false")

                // запрос на получение данных
                val response = try {
                    Log.d("DEBBBUG", "запрос($lastLoadedPosition, $findString)")
                    RetrofitInstance.getService.findClientsAsync(lastLoadedPosition, findString, token)
                        .await()
                } catch (e: Exception) {
                    Log.d("DEBBBUG", "ошибка")
                    null
                }

                val answerList = if (response?.isSuccessful == true && response.body() != null) {
                    Log.d("DEBBBUG", "lastLoadedPosition += response.body()!!.size")
                    lastLoadedPosition += response.body()!!.size
                    response.body()!!
                } else {
                    null
                }


                uiScope.launch {
                    Log.d("DEBBBUG", "===== withContext(Dispatchers.Main)=======")
                    if (response?.isSuccessful == true && answerList != null) {
                        Log.d("DEBBBUG", "ответ кол-во="+answerList.size)

                        if(lastLoadedPosition == 0 || loadedCache == null) {
                            loadedCache = answerList
                        } else {
                            if(answerList.size > 0)
                                loadedCache!!.addAll(answerList)
                        }

                        if(answerList.size > 0)
                            viewState.insertClientsList(answerList, lastLoadedPosition == 0)

                        Log.d("DEBBBUG", "теперь lastLoadedPosition=$lastLoadedPosition")

                    } else {
                        when (response?.code()) {
                            401 -> {
                                viewState.errorAuth()
                            }
                            else -> {
                                viewState.errorLoading()
                            }
                        }
                    }

                    // прячем анимацию загрузки данных
                    viewState.hideProgress()
                }
            } while (haveToRepeatUpdateString)

            // убираем флаг текущей загрузки
            serverBusyReceiving = false

        }
    }

}