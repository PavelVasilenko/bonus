package ru.devroom.krizalbonus.ui.edit_employee

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.devroom.krizalbonus.model.AppUser
import ru.devroom.krizalbonus.model.network.RetrofitInstance
import ru.devroom.krizalbonus.model.objects.EmployeeInfo
import ru.devroom.krizalbonus.ui.auth.AuthActivityPresenter

@InjectViewState
class EditEmployeeActivityPresenter:MvpPresenter<IEditEmployeeActivity>() {

    private var editedEmployee: EmployeeInfo? = null

    // флаг текущего сеанса отправки/загрузки
    @Volatile
    var serverBusy = false

    fun getEditedItem() = editedEmployee

    fun insertEditedItem(serializedItem: String) {
        editedEmployee = Gson().fromJson(serializedItem, EmployeeInfo::class.java)

        if (editedEmployee != null) {
            viewState.insertValues(
                editedEmployee!!.login,
                editedEmployee!!.family
            )
        }
    }
    fun onEditPasswordClicked() {
        viewState.showChangePassword()

    }

    fun saveItem(login:String, family:String, typedPassword:String) {
        // если сервер занят - выходим
        if (serverBusy) return

        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }

        // проверки данных
        if (login.isEmpty()) {
            viewState.errorEmptyLogin()
            return
        }
        if (family.isEmpty()) {
            viewState.errorEmptyFamily()
            return
        }
        if (editedEmployee == null && typedPassword.isEmpty()) {
            viewState.errorEmptyPassword()
            return
        }

        // показать анимацию загрузки данных
        viewState.showProgress()
        // в новом потоке - запрашиваем данные с сервера
        GlobalScope.launch {
            // ставим флаг активной загрузки
            serverBusy = true

            val shaPassword = if(typedPassword.isEmpty())
                ""
            else
                AuthActivityPresenter.encryptPassword(typedPassword)

            val response = try {
                // это новая организация
                if(editedEmployee == null || editedEmployee!!.serverId < 0)
                    RetrofitInstance.getService.postNewEmployeeAsync(
                        login,
                        shaPassword,
                        family,
                        token).await()
                else // это обновление данных
                    RetrofitInstance.getService.putEmployeeAsync(
                        editedEmployee?.serverId ?: -1,
                        login,
                        shaPassword,
                        family,
                        token).await()
            } catch (e: Exception) {
                null
            }

            // в основном потоке - показываем пользователю информацию
            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true) {
                    viewState.finishWithOk()
                } else {
                    when(response?.code()) {
                        401 -> viewState.errorAuth()
                        409 -> viewState.errorOccupiedLogin()
                        else -> viewState.errorSavingItem()
                    }
                }
                // убираем флаг текущей загрузки
                serverBusy = false
                // прячем анимацию загрузки данных
                viewState.hideProgress()
            }
        }
    }

    fun deleteItem() {
        if(editedEmployee == null || editedEmployee!!.serverId < 0) return
        // если сервер занят - выходим
        if(serverBusy) return
        // авторизация пользователя
        val token = AppUser.get().getEmployeeInfo()?.token
        if(token == null) {
            viewState.errorAuth()
            return
        }
        // показать анимацию
        viewState.showProgress()
        // в новом потоке - запрашиваем данные с сервера
        GlobalScope.launch {
            // ставим флаг активной загрузки
            serverBusy = true
            val response = try {
                RetrofitInstance.getService.deleteEmployeeAsync(editedEmployee!!.serverId, token).await()
            } catch (e: Exception) {
                null
            }
            // в основном потоке - показываем пользователю информацию
            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true) {
                    viewState.finishWithOk()
                } else {
                    viewState.errorDeletingItem()
                }
                // убираем флаг текущей загрузки
                serverBusy = false
                // прячем анимацию загрузки данных
                viewState.hideProgress()
            }
        }
    }
}