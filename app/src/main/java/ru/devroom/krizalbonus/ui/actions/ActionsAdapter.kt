package ru.devroom.krizalbonus.ui.actions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.objects.ActionInfo
import java.util.*
import kotlin.collections.ArrayList

class ActionsAdapter : RecyclerView.Adapter<ActionsAdapter.ViewHolder>() {
    // список документов
    private var data: List<ActionInfo> = ArrayList()
    //обработчик нажатия на элемент списка
    private lateinit var mClickListener: ItemClickListener
    private val calendar = Calendar.getInstance()


    // метод для установки нового списка
    fun setData(newData: List<ActionInfo>?) {
        // запоминаем список
        if (newData.isNullOrEmpty()) {
            this.data = ArrayList()
        } else {
            this.data = newData
        }
        notifyDataSetChanged() // уведомляем менеджер о том, что список поменялся
    }

    // воздаем вью для элемента списка
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_action, parent, false)
        )
    }

    // при присоединиении элемента списка к recyclerView
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // оформляем данные внутри элемента списка
        val item = data[position]

        calendar.timeInMillis = 1000 * item.dateTime
        val year = calendar.get(Calendar.YEAR)
        val month = 1 + calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        var date1 = if (day < 10) "0$day" else "$day"
        date1 += "." + if (month < 10) "0$month" else "$month"
        date1 += ".$year"

        calendar.timeInMillis = 1000 * item.endTime
        val year2 = calendar.get(Calendar.YEAR)
        val month2 = 1 + calendar.get(Calendar.MONTH)
        val day2 = calendar.get(Calendar.DAY_OF_MONTH)
        var date2 = if (day2 < 10) "0$day2" else "$day2"
        date2 += "." + if (month2 < 10) "0$month2" else "$month2"
        date2 += ".$year2"

        holder.bindData(item, date1, date2, mClickListener)
    }

    // возвращаем кол-во элементов списка
    override fun getItemCount(): Int {
        return data.size
    }

    // внутренний класс для хранения и настройки элементов карточки клиента
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private var tvCaption: TextView? = null
        private var tvActionText: TextView? = null
        private var tvStartAction: TextView? = null
        private var tvEndAction: TextView? = null
        private var tvDisabled: TextView? = null
        private var listener: ItemClickListener? = null

        private lateinit var item: ActionInfo

        init {
            // находим текстовые поля, в которые будем вставлять даные
            tvCaption = itemView.findViewById(R.id.tvCaption)
            tvActionText = itemView.findViewById(R.id.tvActionText)
            tvStartAction = itemView.findViewById(R.id.tvStartAction)
            tvEndAction = itemView.findViewById(R.id.tvEndAction)
            tvDisabled = itemView.findViewById(R.id.tvDisabled)

            // применяем обработчик нажатия на элемент списка
            itemView.setOnClickListener(this)
        }

        fun bindData(data: ActionInfo, start: String, end: String, listener: ItemClickListener) {

            tvCaption?.text = data.caption
            tvActionText?.text = data.text
            tvStartAction?.text = start
            tvEndAction?.text = end

            tvDisabled?.visibility = if (data.active == 1) View.GONE else View.VISIBLE

            // запоминаем объект, по которому сделана эта карточка
            this.item = data
            // запоминаем слушатель событий
            this.listener = listener
        }

        // обработчик нажатия на элемент
        override fun onClick(view: View) {
            if (listener != null)
                listener!!.onItemClick(view, item)
        }
    }

    // метод для применения обработчика нажатий на элемент списка
    fun setClickListener(itemClickListener: ItemClickListener) {
        this.mClickListener = itemClickListener
    }

    // интерфейс, описывающий метод обработчика нажатий на элемент
    interface ItemClickListener {
        fun onItemClick(view: View, item: ActionInfo)
    }
}