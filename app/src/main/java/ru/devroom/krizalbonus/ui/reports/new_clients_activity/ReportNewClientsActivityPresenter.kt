package ru.devroom.krizalbonus.ui.reports.new_clients_activity

import android.content.Context
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.devroom.krizalbonus.model.AppUser
import ru.devroom.krizalbonus.model.XLSExporter
import ru.devroom.krizalbonus.model.network.RetrofitInstance
import ru.devroom.krizalbonus.model.objects.ClientInfo
import java.text.SimpleDateFormat
import java.util.*

@InjectViewState
class ReportNewClientsActivityPresenter:MvpPresenter<IReportNewClientsActivity>() {


    private var dateFormatServer = SimpleDateFormat("yyyy-MM-dd", Locale.US)
    private var dateFormatApp = SimpleDateFormat("dd.MM.yyyy", Locale.US)

    private var startDate:Long = 0
    private var endDate:Long = 0

    companion object {
        const val MAX_FOR_ALL_TIME = 3474002297039
        const val MIN_FOR_ALL_TIME = 318242297039
    }
    @Volatile private var lastLoadedData:ArrayList<ClientInfo>? = null

    // флаг текущего сеанса отправки/загрузки
    @Volatile var serverBusyReceiving = false


    fun getStartDateRange(): Calendar? {
        return if(startDate > 0) {
            val startCalendar = GregorianCalendar(Locale.US)
            startCalendar.timeInMillis = startDate
            startCalendar
        } else {
            null
        }
    }

    fun export(ctx:Context) {
        if (lastLoadedData != null) {
            viewState.showProgress()
            GlobalScope.launch {
                var datesRange = dateFormatApp.format(startDate)
                if (startDate != endDate) datesRange += " - " + dateFormatApp.format(endDate)
                val file = XLSExporter().exportClients(lastLoadedData!!, datesRange, ctx)
                withContext(Dispatchers.Main) {
                    if(file != null) {
                        viewState.openFile(file)
                    }
                    viewState.hideProgress()
                }

            }
        }
    }

    fun getEndDateRange(): Calendar? {
        return if(endDate > 0) {
            val endCalendar = GregorianCalendar(Locale.US)
            endCalendar.timeInMillis = endDate
            endCalendar
        } else {
            null
        }
    }

    fun setTodayDateRange() {
        val curDate = Date().time
        setDateRange(curDate, curDate)
    }

    fun setFullDateRange() {
        setDateRange(MIN_FOR_ALL_TIME, MAX_FOR_ALL_TIME)
    }

    fun isAllTimeSelected() = (startDate == MIN_FOR_ALL_TIME && endDate == MAX_FOR_ALL_TIME)

    fun setDateRange(start:Long, end:Long) {
        startDate = start
        endDate = end

        if(start == MIN_FOR_ALL_TIME && end == MAX_FOR_ALL_TIME) {
            viewState.setDates("")
        } else {
            var datesRange = dateFormatApp.format(startDate)
            if(startDate != endDate) datesRange += " - " + dateFormatApp.format(endDate)
            viewState.setDates(datesRange)
        }


        updatePaymentsList()
    }

    fun updatePaymentsList() {
        if (serverBusyReceiving) return

        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }
        viewState.showProgress()
        GlobalScope.launch {
            // ставим флаг активной загрузки
            serverBusyReceiving = true

            val date1 = dateFormatServer.format(Date(startDate))
            val date2 = dateFormatServer.format(Date(endDate))

            // запрос на получение данных
            val response = try {
                RetrofitInstance.getService.getReportNewClientsAsync(date1, date2, token).await()
            } catch (e: Exception) {
                null
            }

            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true && response.body() != null) {

                    viewState.insertClientsList(response.body()!!)
                    lastLoadedData = response.body()!!
                } else {
                    when (response?.code()) {
                        401 -> {
                            viewState.errorAuth()
                        }
                        else -> {
                            viewState.errorLoading()
                        }
                    }
                }
                // убираем флаг текущей загрузки
                serverBusyReceiving = false
                // прячем анимацию загрузки данных
                viewState.hideProgress()
            }
        }
    }
}