package ru.devroom.krizalbonus.ui.edit_level

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.devroom.krizalbonus.model.AppUser
import ru.devroom.krizalbonus.model.network.RetrofitInstance
import ru.devroom.krizalbonus.model.objects.LevelInfo

@InjectViewState
class EditLevelActivityPresenter : MvpPresenter<IEditLevelActivity>() {

    private var editedLevel: LevelInfo? = null

    // флаг текущего сеанса отправки/загрузки
    @Volatile
    var serverBusy = false

    fun getEditedItem() = editedLevel

    fun insertEditedItem(serializedItem: String) {
        editedLevel = Gson().fromJson(serializedItem, LevelInfo::class.java)

        if (editedLevel != null) {
            viewState.insertValues(
                editedLevel!!.totalPurchases,
                editedLevel!!.cashBack,
                editedLevel!!.maxDiscount,
                editedLevel!!.liveTime,
                editedLevel!!.delayDays
            )
        }
    }

    fun saveItem(
        totalPurchases: String,
        cashBack: String,
        maxDiscount: String,
        liveTime: String,
        delayDays: String

    ) {
        // если сервер занят - выходим
        if (serverBusy) return

        val token = AppUser.get().getEmployeeInfo()?.token
        if (token == null) {
            viewState.errorAuth()
            return
        }

        // проверки данных
        if (totalPurchases.isEmpty()) {
            viewState.errorEmptyPurchases()
            return
        }
        if (cashBack.isEmpty()) {
            viewState.errorEmptyCashBack()
            return
        }
        if (maxDiscount.isEmpty()) {
            viewState.errorEmptyDiscount()
            return
        }
        if (liveTime.isEmpty()) {
            viewState.errorEmptyLiveTime()
            return
        }
        if (delayDays.isEmpty()) {
            viewState.errorEmptyDelay()
            return
        }

        // показать анимацию загрузки данных
        viewState.showProgress()
        // в новом потоке - запрашиваем данные с сервера
        GlobalScope.launch {
            // ставим флаг активной загрузки
            serverBusy = true

            val response = try {
                // это новая организация
                if(editedLevel == null || editedLevel!!.serverId < 0)
                    RetrofitInstance.getService.postNewLevelAsync(
                        totalPurchases.toIntOrNull() ?: 0,
                        cashBack.toIntOrNull() ?: 0,
                        maxDiscount.toIntOrNull() ?: 0,
                        liveTime.toIntOrNull() ?: 0,
                        delayDays.toIntOrNull() ?: 0,
                        token).await()
                else // это обновление данных
                    RetrofitInstance.getService.updateLevelAsync(
                        editedLevel?.serverId ?: -1,
                        totalPurchases.toIntOrNull() ?: 0,
                        cashBack.toIntOrNull() ?: 0,
                        maxDiscount.toIntOrNull() ?: 0,
                        liveTime.toIntOrNull() ?: 0,
                        delayDays.toIntOrNull() ?: 0,
                        token).await()
            } catch (e: Exception) {
                null
            }

            // в основном потоке - показываем пользователю информацию
            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true) {
                    viewState.finishWithOk()
                } else {
                    when(response?.code()) {
                        401 -> viewState.errorAuth()
                        409 -> viewState.errorOccupiedPurchases()
                        else -> viewState.errorSavingItem()
                    }
                }
                // убираем флаг текущей загрузки
                serverBusy = false
                // прячем анимацию загрузки данных
                viewState.hideProgress()
            }
        }
    }

    fun deleteItem() {
        if(editedLevel == null || editedLevel!!.serverId < 0) return
        // если сервер занят - выходим
        if(serverBusy) return
        // авторизация пользователя
        val token = AppUser.get().getEmployeeInfo()?.token
        if(token == null) {
            viewState.errorAuth()
            return
        }
        // показать анимацию
        viewState.showProgress()
        // в новом потоке - запрашиваем данные с сервера
        GlobalScope.launch {
            // ставим флаг активной загрузки
            serverBusy = true
            val response = try {
                RetrofitInstance.getService.deleteLevelAsync(editedLevel!!.serverId, token).await()
            } catch (e: Exception) {
                null
            }
            // в основном потоке - показываем пользователю информацию
            withContext(Dispatchers.Main) {
                if (response?.isSuccessful == true) {
                    viewState.finishWithOk()
                } else {
                    viewState.errorDeletingItem()
                }
                // убираем флаг текущей загрузки
                serverBusy = false
                // прячем анимацию загрузки данных
                viewState.hideProgress()
            }
        }
    }


}