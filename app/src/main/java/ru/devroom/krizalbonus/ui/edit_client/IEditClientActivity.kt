package ru.devroom.krizalbonus.ui.edit_client

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.devroom.krizalbonus.model.objects.ClientInfo

interface IEditClientActivity:MvpView {

    @StateStrategyType(SkipStrategy::class)
    fun errorEmptyTelephone()

    @StateStrategyType(SkipStrategy::class)
    fun errorEmptyPassword()

    @StateStrategyType(SkipStrategy::class)
    fun errorPasswordsNotEqual()

    @StateStrategyType(SkipStrategy::class)
    fun errorOccupiedTelephone()

    @StateStrategyType(SkipStrategy::class)
    fun errorSavingItem()

    @StateStrategyType(SkipStrategy::class)
    fun finishRegistered(clientCardNumber:Int)

    @StateStrategyType(SkipStrategy::class)
    fun finishChanged()

    @StateStrategyType(SkipStrategy::class)
    fun errorAuth()

    fun setEditedClientInfo(data:ClientInfo)

    fun showProgress()
    fun hideProgress()


    fun insertNewDate(value:String)
    fun setGender(value:Int)


}