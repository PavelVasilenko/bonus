package ru.devroom.krizalbonus.ui.edit_action

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface IEditActionActivity:MvpView {

    fun insertValues(caption:String, text:String, enabled:Boolean)
    fun insertStartDate(date:String)
    fun insertEndDate(date:String)


    @StateStrategyType(SkipStrategy::class)
    fun errorEmptyCaption()

    @StateStrategyType(SkipStrategy::class)
    fun errorEmptyText()

    @StateStrategyType(SkipStrategy::class)
    fun errorEmptyStartDate()

    @StateStrategyType(SkipStrategy::class)
    fun errorEmptyEndDate()

    @StateStrategyType(SkipStrategy::class)
    fun errorSavingItem()

    @StateStrategyType(SkipStrategy::class)
    fun finishWithOk()

    @StateStrategyType(SkipStrategy::class)
    fun errorDeletingItem()

    @StateStrategyType(SkipStrategy::class)
    fun errorAuth()

    fun showProgress()
    fun hideProgress()
}