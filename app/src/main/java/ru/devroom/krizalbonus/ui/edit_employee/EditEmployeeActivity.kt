package ru.devroom.krizalbonus.ui.edit_employee

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.activity_edit_employee.*
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.Codes
import ru.devroom.krizalbonus.ui.reports.purchases_activity.ReportPurchasesActivity
import ru.devroom.krizalbonus.ui.reports.purchases_activity.ReportPurchasesActivity.Companion.PARAM_EMPLOYEE_FAMILY
import ru.devroom.krizalbonus.ui.reports.purchases_activity.ReportPurchasesActivity.Companion.PARAM_EMPLOYEE_ID

class EditEmployeeActivity : MvpAppCompatActivity(), IEditEmployeeActivity {

    @InjectPresenter
    lateinit var mPresenter: EditEmployeeActivityPresenter

    companion object {
        const val SERIALIZED_ITEM = "edited_item"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_employee)


        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)


        bChangePassword.setOnClickListener {
            mPresenter.onEditPasswordClicked()
        }

        fabSave.setOnClickListener {
            val typedPassword = if (etPassword.visibility == View.GONE)
                ""
            else
                etPassword.text.toString()

            mPresenter.saveItem(
                etLogin.text.toString(),
                etFamily.text.toString(),
                typedPassword
            )
        }

        val data: String? = intent.getStringExtra(SERIALIZED_ITEM)
        if (data != null) {
            fabDelete.show()
            fabDelete.setOnClickListener { onDeleteClicked() }
            mPresenter.insertEditedItem(data)

            tvPasswordLabel.visibility = View.GONE
            etPassword.visibility = View.GONE

            bChangePassword.visibility = View.VISIBLE

            etLogin.isEnabled = false
        } else {
            fabDelete.hide()
            etLogin.isEnabled = true
            bChangePassword.visibility = View.GONE

        }

        hideProgress()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.employee_card, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.menu_employee_report -> {
                openEmployeeReport()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun openEmployeeReport() {
        val employee = mPresenter.getEditedItem() ?: return

        val intent = Intent(this, ReportPurchasesActivity::class.java)
        intent.putExtra(PARAM_EMPLOYEE_ID, employee.serverId)
        intent.putExtra(PARAM_EMPLOYEE_FAMILY, employee.family)
        startActivity(intent)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun onDeleteClicked() {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_caption)
            .setMessage(R.string.dialog_delete_employee)
            .setNegativeButton(android.R.string.no){ _: DialogInterface, _: Int -> }
            .setPositiveButton(android.R.string.yes){ _: DialogInterface, _: Int ->
                mPresenter.deleteItem()
            }.show()
    }
    override fun insertValues(login: String, family: String) {
        etFamily.setText(family)
        etLogin.setText(login)
    }

    override fun errorEmptyLogin() {
        etLogin.error = getString(R.string.edit_field_empty)
    }

    override fun errorEmptyFamily() {
        etFamily.error = getString(R.string.edit_field_empty)
    }

    override fun errorEmptyPassword() {
        etPassword.error = getString(R.string.edit_field_empty)
    }

    override fun errorOccupiedLogin() {
        Toast.makeText(this, R.string.edit_employee_login_occupied, Toast.LENGTH_SHORT).show()
    }

    override fun errorSavingItem() {
        Toast.makeText(this, R.string.error_while_saving, Toast.LENGTH_SHORT).show()
    }

    override fun finishWithOk() {
        setResult(Codes.OK)
        finish()
    }

    override fun errorDeletingItem() {
        Toast.makeText(this, R.string.error_while_deleting, Toast.LENGTH_SHORT).show()
    }

    override fun errorAuth() {
        Toast.makeText(this, R.string.error_while_auth, Toast.LENGTH_SHORT).show()
    }

    override fun showChangePassword() {
        if (tvPasswordLabel.visibility == View.VISIBLE) {
            tvPasswordLabel.visibility = View.GONE
            etPassword.visibility = View.GONE
            bChangePassword.text = getString(R.string.but_change_password)
        } else {
            tvPasswordLabel.visibility = View.VISIBLE
            etPassword.visibility = View.VISIBLE
            bChangePassword.text = getString(R.string.but_cancel)
        }


    }

    override fun showProgress() {
        fabDelete.hide()
        fabSave.hide()
        progressBar2.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        if(mPresenter.getEditedItem() == null)
            fabDelete.hide()
        else
            fabDelete.show()
        fabSave.show()
        progressBar2.visibility = View.GONE
    }
}
