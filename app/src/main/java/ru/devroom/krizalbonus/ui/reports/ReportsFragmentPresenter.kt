package ru.devroom.krizalbonus.ui.reports

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

@InjectViewState
class ReportsFragmentPresenter:MvpPresenter<IReportsFragment>() {
}