package ru.devroom.krizalbonus.ui.edit_gifts

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.activity_edit_gifts.*
import ru.devroom.krizalbonus.R
import ru.devroom.krizalbonus.model.Codes

class EditGiftsActivity : MvpAppCompatActivity(), IEditGiftsActivity {

    @InjectPresenter
    lateinit var mPresenter: EditGiftsActivityPresenter

    companion object {
        const val VALUE_SUM = "value_sum"
        const val VALUE_DAYS = "value_days"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_gifts)


        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        fabSave.setOnClickListener { mPresenter.saveItem(
            etBirthDayGiftSum.text.toString(),
            etBirthDayGiftDays.text.toString()
        ) }

        val sum = intent.getIntExtra(VALUE_SUM, 0)
        val days = intent.getIntExtra(VALUE_DAYS, 0)
        mPresenter.insertEditedData(sum, days)

        hideProgress()
    }

    override fun insertValues(sum: Int, days: Int) {
        etBirthDayGiftSum.setText(sum.toString())
        etBirthDayGiftDays.setText(days.toString())
    }

    override fun errorEmptySum() {
        etBirthDayGiftSum.error = getString(R.string.edit_field_empty)
    }

    override fun errorEmptyDays() {
        etBirthDayGiftDays.error = getString(R.string.edit_field_empty)
    }

    override fun errorSavingItem() {
        Toast.makeText(this, R.string.error_while_saving, Toast.LENGTH_SHORT).show()
    }

    override fun finishWithOk() {
        setResult(Codes.OK)
        finish()
    }

    override fun errorAuth() {
        Toast.makeText(this, R.string.error_while_auth, Toast.LENGTH_SHORT).show()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun showProgress() {
        progressBar2.visibility = View.VISIBLE
        fabSave.hide()
    }

    override fun hideProgress() {
        progressBar2.visibility = View.GONE
        fabSave.show()
    }
}
