package ru.devroom.krizalbonus

import android.app.Application
import ru.devroom.krizalbonus.model.AppUser


class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        // загрузка пользовательских данных
        AppUser.init(applicationContext)
    }

}