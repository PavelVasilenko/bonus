package ru.devroom.krizalbonus

interface IFragmentCallback {
    fun inflateMenu(menuId:Int?)
    fun onAuthError()
}